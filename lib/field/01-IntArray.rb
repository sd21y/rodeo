# -*- coding: utf-8 -*-

#
# 整数配列型
#

class RFIntArray < RFBase
  def self.name(); "整数配列" end
  def self.type(); "int[]" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # データのクリア
  def clear()
    @value = []
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name])
      @value = instance_eval(db[db_name()].sub(/^\{/,"[").sub(/\}$/,"]").untaint)
    else
      @value = []
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    if(@value)
      "'{"+@value.collect{|n| n.to_s}.join(",")+"}'"
    else
      "'{}'"
    end
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] != "")
        @value = @table.rodeo[cgi_name("hidden")].split(/,/).collect{|n| n.to_i }
      else
        yield() if(block_given?)
      end
		}
  end
  # CSVからの取り込み
  def from_csv(data)
    if(data)
      @value = instance_eval((data).untaint)
    end
  end
  
  def to_csv()
    %Q("[#{@value.join(",")}]")
  end
  #
  def hidden()
    @table.rodeo.html_hidden(cgi_name("hidden"),@value.join(","))
  end
  #
  def val()
    @value = nil
    where = "where not (\"#{@name}\" is null or \"#{@name}\" = '{}')"
    if(@table.where_string()) then where = " where #{@table.where_string()} and not (\"#{@name}\" is null or \"#{@name}\" = '{}')"; end
    @table.rodeo.sql.exec(%Q(select count("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      @value = res[0]['count'].to_i
    }
    @value
  end
  def nval()
    @value = nil
    where = "where \"#{@name}\" is null or \"#{@name}\" = '{}'"
    if(@table.where_string()) then where = " where #{@table.where_string()} and (\"#{@name}\" is null or \"#{@name}\" = '{}')"; end
    @table.rodeo.sql.exec(%Q(select count("#{@name}") from "rd_#{@table.name}"#{where})) {|res|
      @value = res[0]['count'].to_i
    }
    @value
  end
end

__END__

