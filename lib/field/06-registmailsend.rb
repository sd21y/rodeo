# -*- coding: utf-8 -*-

#
# メール送信型
#

require "#{File.dirname(__FILE__)}/../email.rb"

class RFregistmailsend < RFPlugin
  def self.name(); "登録完了メール送信" end
  # 実行用アイコン
  def icon()
    nil
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['mail_title'] = @table.rodeo["#{@fid}.mail_title"]
    @conf['from_address'] = @table.rodeo["#{@fid}.from_address"]
    @conf['to_field'] = @table.rodeo["#{@fid}.to_field"]
    @conf['pass_field'] = @table.rodeo["#{@fid}.pass_field"]
    @conf['info_text'] = @table.rodeo["#{@fid}.info_text"]
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super()
    @table.rodeo.output()
  end
  # 
  def regmailsend()
    mail = Email.new()
    mail.smtpServer = "smtp.sgmail.jp"
    mail['To'] = (@table[@conf['to_field']].value).untaint
    mail['From'] = (@conf['from_address']).untaint
    mail['Subject'] = @conf['mail_title'].mime_encode
    mail['Content-Type'] = "text/plain; charset=ISO-2022-JP"
    @conf['info_text'].each_line {|line|
      line.chomp!
      line.gsub!(/\r/,"")
      line.gsub!(/(%[^%]*%)/) {
        key = $1[1..-2]
        if(@table[key])
          text = @table[key].value.to_s
        else
          text = "?"
        end
        text
      }
      line.gsub(/㈱/,"(株)").gsub(/㈲/,"(有)")
      mail << line.tojis
    }
    mail.send()
  end
  # 登録完了メール送信タグ
  def display(list)
    Rodeo.print %Q(<a href="javascript:app_exec('#{@name}',#{@table.roid});">登録メール送信</a>)
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super()
    if(@table[@conf['to_field']].value and @table[@conf['to_field']].value.strip != "")
      if(@table['仮パスワード'].value != nil and @table['仮パスワード'].value.strip != "")
        @table.rodeo.eruby(__FILE__,"regmail_start",binding)
      else
        @table.rodeo.eruby(__FILE__,"regmail_nopasswd",binding)
      end
    else
      @table.rodeo.eruby(__FILE__,"regmail_nomailaddr",binding)
    end
    @table.rodeo.output()
  end
  # アプリケーション画面の表示
  def action_regmail_exec()
    regmailsend()
    @table.rodeo.eruby(__FILE__,"regmail_done",binding)
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信タイトル","メールのタイトルを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.mail_title",@conf['mail_title'],:size=>60)%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信元アドレス","送信元のアドレスを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.from_address",@conf['from_address'],:size=>60)%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信先フィールド","宛先のFromアドレスとして使用するフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.to_field",@conf['to_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFstring")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("パスワードフィールド","生パスワードとして使用するフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.pass_field",@conf['pass_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFstring")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("案内文","案内文を設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_textarea("#{@fid}.info_text",@conf['info_text'],60,10,{"style"=>"width:100%"})%></td>
</tr>

#### regmail_start

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>登録完了メール</title>
  </head>
  <body>
    [[ 登録完了メール送信 ]]
    <hr>
    下記の登録完了メールを<%=@table[@conf['to_field']].value%>宛に送信します。<br>
    よろしいですか？<br>
    <hr>
    <tt>
    Subject: <%=@conf['mail_title']%><br>
    To: <%=@table[@conf['to_field']].value%><br>
    From: <%=@conf['from_address']%><br>
    <br>
    <%
      @conf['info_text'].each_line {|line|
        line.chomp!
        line.gsub!(/\r/,"")
        line.gsub!(/(%[^%]*%)/) {
          key = $1[1..-2]
          if(@table[key])
            text = @table[key].value.to_s
          else
            text = "?"
          end
          text
        }
        line.gsub(/㈱/,"(株)").gsub(/㈲/,"(有)")
        Rodeo.print line
        Rodeo.puts "<br>"
      }
    %>
    </tt>
    <hr>
    <%@table.rodeo.html_form("#{@appname}") {%>
      <%@table.rodeo.html_hidden("action","regmail_exec")%>
      <%@table.rodeo.html_submit("submit","送信")%><br>
    <%}%>
  </body>
</html>

#### regmail_done

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>登録完了メール</title>
  </head>
  <body>
    [[ 登録完了メール送信終了 ]]
    <hr>
    登録完了メールを送信しました。<br>
    <hr>
  </body>
</html>

#### regmail_nomailaddr

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>登録完了メール</title>
  </head>
  <body>
    [[ メールアドレスエラー ]]
    <hr>
    メールアドレスがありません。<br>
    <hr>
  </body>
</html>

#### regmail_nopasswd

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>登録完了メール</title>
  </head>
  <body>
    [[ パスワードエラー ]]
    <hr>
    仮パスワードがありません。<br>
    <hr>
  </body>
</html>

