# -*- coding: utf-8 -*-

#
# 論理型
#

class RFbool < RFBool
  def self.name(); "論理" end
  def self.type(); RFBool.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['check_type']  = false if(@conf['check_type'] == nil)
    @conf['true_label']  = "真"  unless(@conf['true_label'])
    @conf['false_label'] = "偽"  unless(@conf['false_label'])
  end
  # データのクリア
  def clear()
    @value = @conf['default']
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    case @table.rodeo["#{@fid}.default"]
    when "true"  then @conf['default'] = true
    when "false" then @conf['default'] = false
    else
      @conf['default'] = nil
    end
    @conf['check_type']  = (@table.rodeo["#{@fid}.check_type"]) == "true" ? true : false
    @conf['true_label']  = @table.rodeo["#{@fid}.true_label"]
    @conf['false_label'] = @table.rodeo["#{@fid}.false_label"]
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    case @table.rodeo["#{@fid}.operator"]
    when "true"   then where = %Q("#{@name}" = true)
    when "false"  then where = %Q("#{@name}" = false)
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    where = nil
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" = #{to_db()})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo.html_valid?(cgi_name()))
        case @table.rodeo[cgi_name()]
        when "true"  then @value = true
        when "false" then @value = false
        else
          @value = nil
        end
      else
        if(@conf['check_type'] == true)
          @value = false
        end
      end
    }
  end
  #
  def to_s()
    if    (@value == true)
      @conf['true_label']
    elsif(@value == false)
      @conf['false_label']
    else
      ""
    end
  end
  # 入力
  def entry(opt={})
    if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)
       Rodeo.print <<-EOS
       <div id="#{cgi_name()}" class="toggle" onclick="" toggled="#{@value}">
         <span class="thumb"></span>
         <span class="toggleOn">#{@conf['true_label']}</span>
         <span class="toggleOff">#{@conf['false_label']}</span>
       </div>
EOS
    else
      opt = {} unless(opt)
      opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
      super() {
        if(@conf['check_type'])
          @table.rodeo.html_checkbox(cgi_name(),@conf['true_label'],@value,opt)
        else
          @table.rodeo.html_radio(cgi_name(),@value) {
            @table.rodeo.html_item(true ,@conf['true_label'],opt)
            @table.rodeo.html_item(false,@conf['false_label'],opt)
          }
        end
      }
    end
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      case @value
      when true  then Rodeo.print @conf['true_label']
      when false then Rodeo.print @conf['false_label']
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("初期値")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.default",@conf['default']) {
        @table.rodeo.html_item(true ,"真値")
        @table.rodeo.html_item(false,"偽値")
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("選択形式")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.check_type",@conf['check_type']) {
        @table.rodeo.html_item(true ,"チェックボックス")
        @table.rodeo.html_item(false,"ラジオボタン")
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("真ラベル")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.true_label",@conf['true_label'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("偽ラベル")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.false_label",@conf['false_label'],{"style"=>"width:100%"})%></td>
</tr>

#### where_entry

<td class="field_conf">
  <%
    @table.rodeo.html_radio("#{@fid}.operator","true") {
      @table.rodeo.html_item("true" ,@conf['true_label'])
      @table.rodeo.html_item("false",@conf['false_label'])
    }
  %>
</td>

