export_mode: online
export_style: rodeo
export_hchar1: _
export_hchar2: _
export_hchar3: ■
export_hchar4: _
export_hchar5: _
export_pagelink: true
= Rodeo2.0マニュアル

　((*Rodeo*))は、以下のような特徴をもつ「データベースマネージャ／アプリケーションフレームワーク」です。

・データベースの操作をWebから行なうことができます。
・簡単にテーブルやフィールドを作ることができます。
・データの種類に適した入力・表示ができます。
・ユーザごと、グループごとのアクセス制限機能があります。
・柔軟なデータの絞り込み機能があります。
・Ajaxを活用した使いやすいユーザインターフェース。
・((*Rodeo*))ベースのアプリケーションが作成できます。
・モジュール追加で新しいフィールドが追加できます。
・柔軟な拡張性があります。
・Rubyで作っています。
・PostgreSQLを使用しています。

　これらの特徴について以下に説明します。

・<a href="rodeo2.0-2-index.html">((*Rodeo*))の概要</a>
・<a href="rodeo2.0-3-index.html">((*Rodeo*))の使い方</a>
・<a href="rodeo2.0-4-index.html">((*Rodeo*))のフィールド</a>
・<a href="rodeo2.0-5-index.html">((*Rodeo*))のプログラミング</a>
・<a href="rodeo2.0-6-index.html">((*Rodeo*))のリファレンス</a>

= Rodeoのコンセプト

　((*Rodeo*))はWebから使うことが出来るデータベースマネージャ／アプリケーションフレームワークです。単なるデータベースマネージャであれば、すでにPHPなどで作られたものもあります。しかし、((*Rodeo*))はデータベースマネージャの機能に加えてアプリケーションフレームワークとしての機能を持っています。正確に言うならば、アプリケーションフレームワークを作成し、その機能を使用してデータベースマネージャの機能を追加したものが((*Rodeo*))です。こういった経緯から、((*Rodeo*))は次のようなコンセプトの元に作成しました。

・誰でも扱えること。
・使いやすいユーザインターフェースであること。
・プログラマが楽にシステムを構築できること。
・データの追加や変更が楽にできること。
・システム全体のコストが安くなること。

== 誰でも扱えること

　((*Rodeo*))は専門家(システムエンジニアやプログラマなど)だけが使う道具ではありません。グループウエアなどと同じく、普通にWebから普通の人に使ってもらえる道具でなければなりません。一般にデータベースの操作と言うと((*SQL*))とか((*クエリー*))とか((*リレーション*))とか、結構難しい用語の意味や使い方を学習する必要があります。

　こういった知識を要求されるのでは、一般の人がデータベースを扱うのは非常に難しくなります。確かにデータベースを使いこなすにはこういった知識があった方が望ましいわけですが、このような専門的な知識がなければデータベースは扱えないものなのでしょうか？((*Rodeo*))はその「アンチテーゼ」的な意味も込めて作成しています。

　一般の方は、データベースは使えなくても表計算ソフトなら十分に活用しています。また((*ファイルメーカー*))などのように直感的に使用できるデータベースソフトもあります。このような機能をWebで実現できないものか？それが((*Rodeo*))のコンセプトのひとつです。

　((*Rodeo*))の操作は非常に簡単です。((*Rodeo*))にログインしたなら、((*[テーブルの名前]*))をクリックするだけで、そのテーブルのデータの一覧の表示になります。項目の多いテーブルであっても横スクロール機能を使用してページ切り替えなしに一覧を見る事ができます。

　レコードを追加するには、

・((*[レコード追加]*))をクリックする
・((*[項目]*))を入力しする
・((*[追加]*))ボタンをクリックする

　という操作で済みます。この場合もページ全体が書き換わることはありませんから、普通にアプリケーションを使うような感覚で操作する事ができます。

　((*Rodeo*))のユーザインターフェースは((*ログイン*))、((*ログアウト*))の時を除いて画面全体が切り替わることはありません。また
一覧表示も固定的に表示しますから表示位置がその都度変わるといったユーザを戸惑わせるような画面の変化を極力排除しています。

　また一度表示したテーブルは、テーブル上部の((*タブ*))で瞬時に切り替えることができますから、複数のテーブルを同時に参照したり、更新したりする作業が非常に簡単になっています。

　テーブル名やフィールド名には「漢字」を使うことが出来ます。システムエンジニアやプログラマはアルファベットの名前に慣れていますが一般の方が扱う場合には漢字を含むフィールド名が扱えるかどうかは操作性に大きく影響します。

　通常、データベースでテーブルを作る場合、まずテーブルの設計を行なう必要があります。後からテーブルの構造を変えることはシステムの構成に大きな影響を与えます。しかし、((*Rodeo*))では、まず空のテーブルを作って、そこに必要なフィールドを追加する、というまったく逆の作り方になります。そのため、最初に厳密なテーブルの設計をする必要はありません(ラフな設計は必要ですが)。

== 使いやすいユーザインターフェースであること

　((*誰でも扱えること*))でも述べましたが、((*Rodeo*))のユーザインターフェースはほとんど画面が切り替わりません。これはAjaxというテクニックを使用したもので、最近のWebプログラミングで流行しているものです。Ajaxを使って操作性を向上させた代表例としては「Googleマップ」があります。

　((*Rodeo*))も、できる限り良好な操作性を提供するため、Ajaxを使用しています。画面の一部だけ書き換えたり、タブで対象のテーブルを切り替えたり、データの追加や更新を行なったとき一覧の内容を最新の状態に変更したりと、ほとんどの操作はAjaxを使ったユーザインターフェースになっています。

　実は((*Rodeo*))はこれが三代目の((*Rodeo*))です。バージョン番号をつければ「Ver 3.0」に相当します。が、最初の((*Rodeo*))は「Ver 0.0」相当なので三代目が「Rodeo2.0」になります(((まあ、Web2.0とバージョン番号を合わせたかっただけというのも理由ですが(^^;)。)))。先代までの((*Rodeo*))はごく普通のWebアプリケーションだったため、画面遷移が多く、その反省からAjaxを使ったユーザインターフェースを使うように改良したわけです。

　ただし、Ajaxを使えば操作性が良くなるとは限りません。Ajaxを使うと弊害として、

・動作が遅くなる
・ダイアログなどを過剰に使用してしまうことがある

　といったことがあり、過剰にAjaxを使用することは逆効果にもなりかねないため、適正かつ必要不可欠な部分にとどめています。

　また、テーブル切り替えの他にも操作性を向上させるための工夫は至る所にあります。そのいくつかを紹介します。

・レコード一覧の横スクロール機能
・disable/enableによる機能制限
・ワークエリアのリサイズによるスクロールの抑止機能
・検索に変わる「絞り込み」機能

　など、細かな部分ではありますが、様々な操作性向上のための工夫をしています。

== プログラマが楽にシステムを構築できること

　世の中様々なWebサービスがありますが、そのほとんどは裏でデータベースが動いています。そのため、データベースはWebサービスを行なう上で必要不可欠な機能になっています。((*Rodeo*))はこのようなWebサービスを作成するための支援機能を持っています。それが((*Rodeo*))のアプリケーションフレームワークとしての機能です。((*Rodeo*))のフレームワーク機能は、主としてクラスライブラリとして提供しています。そのため、フレームワークというよりは((*ライブラリ*))としての意味合いが強いものになっています。

　Webサービスを作成する場合、データベースを操作してその値を表示したり、入力を行なったり、という動作を行なうわけですが、Webからのデータはデータベースの型に変換してからデータベースに書き込まなければなりません。表示する場合にも、その逆の動作を行なわなければなりません。

　何の支援もなしにこれらの機能を実装すると、型の変換や評価、特定の値の場合の例外処理、データの統合や分割、といった煩雑な処理が多く発生します。こういった処理を延々と書き連ねる作業は、苦労の割には報われない作業です。ならば、こういった処理をまとめてしまえば、苦労せずにWebサービスを作れるはずだ、というのが((*Rodeo*))として結実したわけです。

　((*Rodeo*))ではデータベースのフィールドとWeb入出力の項目を直結してしまいます。単純に対応付けただけでは機能が低すぎるので、それぞれにプロパティーを設定できるようにすることで、入出力のコントロールを行なうようにしています。それに加えて現在の((*Rodeo*))では「データベースフィールド」そのものを「仮想化」することで複雑な入出力を簡単に行なえるようになっています(((このあたりの詳しい説明はプログラミングマニュアルを参照してください。)))。

　また、データベースアプリケーションではプログラム検証に試験用のデータが必要になったりしますし、商品情報などのデータベースの管理者が管理するデータを入力するためのページを作成しなければなりません。これをデータベースマネージャとして実装してしまえば、お客さん毎にこれらのページを作成する必要がなくなります。こうすることで、作成しなければならないプログラムの量は半分以下に出来るようになります。

　このようなフレームワークを実現するため、((*Rodeo*))では、

・埋め込み型のeruby機能
・クラスライブラリ
・データの追加や変更が楽にできること
・カスタムアプリケーションが作成しやすいこと

　などの機能を実装し、その上で((*Rodeo*))のインターフェースを実装しています。

=== 埋め込み型のeruby機能

　erubyは、((* Enbedded-ruby*))のことで、一般にはHTMLの中に((*Ruby*))のプログラムを入れ込む事を意味します。次のリストは単なerubyの例です。

 === code erubyの例
<html>
  <body>
    <%=ENV['QUERY_STRING']%>
  </body>
</html>
 ===

　PHPなどと同じようにRubyのプログラムをhtmlに埋め込んで("<%","%>"で囲まれた部分がRubyのプログラム部分です)、動的にページを生成する機能は、Webプログラミングでは不可欠な機能です。((*Rodeo*))では、この機能を拡張して「入れ子」構造をサポートすることで、Rubyプログラムのみならず、HTMLの構成要素自体の「部品化」をサポートしています。

 === code 部品化されたerubyの例

class RFBase < RFobject
  def config_entry(su=false)
    if(su)
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
    end
    yield() if(block_given?)
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("必須項目",
      "この項目が必須項目かどうかを設定します。\\n「必須」にすると入力が無い場合にはエラーになります。")%>
  </th>
  <td class="field_conf">
  ....
</tr>
 ===

　詳しくはプログラミングマニュアルで解説しますが、((*Rodeo*))のeruby機能はHTML要素も部品化出来るため、クラスライブラリにHTMLを記述する事が可能になり、プログラミングの自由度が向上しています。

=== クラスライブラリ

　((*Rodeo*))では、((*使いやすいユーザインターフェース*))の実現や((*プログラマが楽にシステムを構築できること*))を実現するため、データベースのフィールドをerubyのオブジェクトとして実装しています。少し長くなりますが、「整数型」の例を次に示します。

 === code 整数型の例
#
# 整数型
#

class RFint < RFInt
  def self.name(); "整数" end
  def self.type(); RFInt.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['cols'] = 10 unless(@conf['cols'])
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['cols']    = @table.rodeo["#{@fid}.cols"]
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    case @table.rodeo['operator']
    when "="  then where = %Q("#{@name}" =  #{@table.rodeo['data']})
    when "!=" then where = %Q("#{@name}" != #{@table.rodeo['data']})
    when "<"  then where = %Q("#{@name}" <  #{@table.rodeo['data']})
    when "<=" then where = %Q("#{@name}" <= #{@table.rodeo['data']})
    when ">"  then where = %Q("#{@name}" >  #{@table.rodeo['data']})
    when ">=" then where = %Q("#{@name}" >= #{@table.rodeo['data']})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    if(@table.rodeo.html_valid?(cgi_name()))
      if(@table.rodeo[cgi_name()] != "")
        @value = @table.rodeo[cgi_name()].to_i
      else
        @value = nil
      end
    end
  end
  # 入力
  def entry()
    @table.rodeo.html_number(cgi_name(),@value.to_s,:size=>@conf['cols'])
  end
  # 表示
  def list()
    if(@value)
      Rodeo.print "#{@value}"
    end
  end
  # 表示
  def display()
    if(@value)
      Rodeo.print "#{@value}"
    end
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("入力桁数")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.cols",@conf['cols'],:size=>10)%></td>
</tr>

#### where_entry

<tr>
  <td class="field_conf">
    <%=@name%>が
    <%@table.rodeo.html_number("data","",:size=>@conf['cols'])%>
    <br>
    <%
      @table.rodeo.html_radio("operator","=") {
        @table.rodeo.html_item("=" ,"等しい")
        @table.rodeo.html_item("!=","等しくない")
        @table.rodeo.html_item("<" ,"小さい")
        @table.rodeo.html_item("<=","以下")
        @table.rodeo.html_item(">" ,"大きい")
        @table.rodeo.html_item(">=","以上")
      }
    %>
  </td>
</tr>
 ===

　先に示したようにこのプログラムもerubyを使って部品化してあります。__END__以降に書いてある####で始まる部分がHTMLとしてかかれた部分でこの中にまたrubyで書いたプログラムがある、と言う構造になっています。((*複雑怪奇*))だと思われるかもしれませんが、このような構造にすることで、

・全体的にはRubyのクラスとして扱うことが出来る
・複数のHTMLパートを含めることが出来る
・１つのファイルで記述が完結する

　といったことが可能になります。Ruby的には単なるクラスですから、この様なパーツを階層的に組み合わせて、継承したクラスや、複合的なクラスを作成する事ができます(((この例ではRFIntを継承したクラスになっています。)))。

=== データの追加や変更が楽にできること

　Webベースのサービスを作成していると、テーブルを追加したいとか、フィールドを追加したい、と言ったことは多々あります。これは仕様の不備の場合もありますが、初めは意図していなかった仕様の追加や変更にともなって発生することも多いことです。

　近年のWebアプリケーション開発はいわゆる「アジャイル型」とよばれる短期間でリリースとレビューを繰り替えしながら目的のサービスに近づけていく開発手法をとることが多くなってきています。

　((*Rodeo*))もこういった「アジャイル型」の開発をサポートできるよう、テーブルの追加やフィールドの追加は動的に行なうことができますし、アプリケーションの機能を組み込んだフィールド型を定義することで、動的なプログラミングをサポートしています。

　いままで単に((*フィールド*))とか((*フィールド型*))と呼んできたものは、実は((*Rodeo*))独自の型のことで、データベースのデータ型などとはまったく違ったものになっています。

　((*Rodeo*))のフィールド型はデータベースのフィールド型とは((*一対一に対応していません*))。実際に。データベースのデータを持たない型や複数のデータベースのフィールドに対応したフィールド型が多々あります。こういった意味では、((*Rodeo*))でフィールド型と言った場合、データベースのデータ型や構造とは切り離して考える必要があります。

　この様な構造になったのは、いくつかの理由があるのですが、主な理由としては、「複合型」のフィールドを実現するためです。その典型的な例として「Googleマップ型」があります。

　Googleマップ型では、

・point型の座標データ
・int型のズームレベル
・text型の住所データ

　を持つようになっています(それぞれの型はデータベース上の型を示します)が、これらのデータはまとめて扱うことができなければなりません。正統的なアプローチとしては、PostgreSQLの型を拡張すると言う手はありますが、プログラミングコストが高いこと、自由度が低いこと、などからあまり良い選択肢ではありません。

　そのため、((*Rodeo*))では、データベースのフィールド名にsufixを付けて、

・フィールド名+"_0"はpoint型
・フィールド名+"_1"はint型
・フィールド名+"_2"はtext型

　と言うデータベースのフィールド名の命名規則を採用し、３つのデータベースのフィールドを一つのデータとして扱うようにしています。こうすることで、アプリケーションレベルで複合データを扱えるようになります(((いわゆるＣ言語の構造体と概念的には同じになります。)))。かなりややこしい方法ですが、この方法であれば、データ構造が「入れ子」になった場合も問題なく扱えます。

=== カスタムアプリケーションが作成しやすいこと

　Webアプリケーションでは、単純に見える入力機能であっても、結構手間がかかったり、重要なデータをプログラム埋め込みにしてしまい、後から変更する事が難しくなったりすることがあります。手間がかかる機能の代表的な例として((*日付の入力*))機能を挙げることができます。

　日付の入力は一般的には、

 === quote
  <input type="text" size="4">年
  <input type="text" size="2">月
  <input type="text" size="2">日
 ===

　のようなものですが、

 === quote
  <input type="text" size="4">年
  <input type="text" size="2">月
  <input type="text" size="2">日
  <input type="text" size="2">時
  <input type="text" size="2">分
  <br>
<select id="rd.2.日付時刻.yy" name="rd.2.日付時刻.yy" >
<option value="2006" >2006</option>
<option value="2007"  selected>2007</option>

<option value="2008" >2008</option>
<option value="2009" >2009</option>
<option value="2010" >2010</option>
<option value="2011" >2011</option>
<option value="2012" >2012</option>
<option value="2013" >2013</option>
<option value="2014" >2014</option>
<option value="2015" >2015</option>
<option value="2016" >2016</option>

<option value="2017" >2017</option>
</select>
年
<select id="rd.2.日付時刻.mm" name="rd.2.日付時刻.mm" >
<option value="01" >01</option>
<option value="02" >02</option>
<option value="03" >03</option>
<option value="04" >04</option>
<option value="05" >05</option>
<option value="06" >06</option>

<option value="07" >07</option>
<option value="08" >08</option>
<option value="09" >09</option>
<option value="10" >10</option>
<option value="11"  selected>11</option>
<option value="12" >12</option>
</select>
月
<select id="rd.2.日付時刻.dd" name="rd.2.日付時刻.dd" >
<option value="01" >01</option>

<option value="02" >02</option>
<option value="03" >03</option>
<option value="04" >04</option>
<option value="05" >05</option>
<option value="06" >06</option>
<option value="07" >07</option>
<option value="08" >08</option>
<option value="09" >09</option>
<option value="10" >10</option>

<option value="11" >11</option>
<option value="12" >12</option>
<option value="13" >13</option>
<option value="14"  selected>14</option>
<option value="15" >15</option>
<option value="16" >16</option>
<option value="17" >17</option>
<option value="18" >18</option>
<option value="19" >19</option>

<option value="20" >20</option>
<option value="21" >21</option>
<option value="22" >22</option>
<option value="23" >23</option>
<option value="24" >24</option>
<option value="25" >25</option>
<option value="26" >26</option>
<option value="27" >27</option>
<option value="28" >28</option>

<option value="29" >29</option>
<option value="30" >30</option>
<option value="31" >31</option>
</select>
日
 ===

　のような様々なバリエーションが存在します。またこれらの入力値はプログラムで合成して、ひとつの値として扱わなければなりません。

　このような事をプログラミングの度に繰り替えしては、生産的とは到底言えません。そこでこれらの部分は前のプログラムからコピーすることになるわけですが、コピーしたとしても、フィールド名の調整やデータベースとのすり合わせなど、結構たくさんの作業が控えています。

　((*Rodeo*))では、フィールド毎にWebでの入出力機能の設定が出来るようになっています。例えば、日付時刻型の場合、次のような設定を行なえば、

 === quote
<style type="text/css" media="screen"><!--
/*
 body
*/

body {
  background-color: white;
  color:black;
}
body.margin0 {
  background-color: white;
  color: black;
  margin: 0px;
}
body.dummy {
  background-color: white;
  color:black;
  margin:0px;
  padding:0px;
  border-width:1px;
  border-style:solid;
  border-color:gray;
}

th,td,input,select,textarea,button {
//  font-size:80%;
}

/*
 div
*/

div.header {
  background:#eeeeee;
  border-width:1px;
  border-style:solid;
  border-color:gray;
  overflow-x:auto;
  overflow-y:auto;
  margin-bottom:2px;
}
div.tables {
  border-width:1px;
  border-style:solid;
  border-color:gray;
  overflow-x:auto;
  overflow-y:scroll;
  margin-bottom:2px;
}
div.frame {
  border-width:0px;
  overflow-x:scroll;
  overflow-y:hidden;
  margin:0px;
  padding:0px;
}
div.fields {
  border-width: 0px;
  background:#eeeeee;
  overflow-x:hidden;
  overflow-y:auto;
  margin:0px;
  padding:0px;
}
div.records {
  border-width:0px;
  border-color:#eeeeee;
  overflow-x:scroll;
  overflow-y:auto;
  margin:0px;
  padding:0px;
}
div.table_tools {
  border-width:1px;
  border-style:solid;
  border-color:gray;
  overflow-x:auto;
  overflow-y:auto;
  margin-bottom:2px;
  padding:0px;
}
div.browse_tools {
  border-width:0px;
  border-style:solid;
  border-color:gray;
  overflow-x:auto;
  overflow-y:auto;
  margin-bottom:2px;
  padding:0px;
}
div.status {
  border-width:0px;
  border-style:solid;
  border-color:#eeeeee;
  background:white;
  overflow-x:auto;
  overflow-y:auto;
  margin:0px;
}
div.work {
  border-width:1px;
  border-style:solid;
  border-color:gray;
  overflow-x:auto;
  overflow-y:scroll;
  margin:0px;
}
div.googlemap {
  border-width:1px;
  border-style:solid;
  border-color:gray;
  overflow-x:hidden;
  overflow-y:hidden;
  margin:0px;
}

/*
 span
*/

span.help {
  color:blue;
  font-weight:bold;
}

/*
 table
*/

table.login {
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  border-collapse: collapse;
  empty-cells: show;
  background-color:#eeeeee;
  padding: 2px;
  margin: 0px;
}
table.table_list {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 0px;
  border-collapse: collapse;
  empty-cells: show;
  background-color: white;
  padding: 0px;
  margin: 0px;
}
table.record_dummy {
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  border-collapse: collapse;
  empty-cells: show;
  background-color:#eeeeee;
  padding: 2px;
  margin: 0px;
}
table.record_frame {
  border-style: solid;
  border-width: 1px;
  border-color: gray;
  border-collapse: collapse;
  empty-cells: hide;
  background-color: white;
  padding: 0px;
  margin-bottom:2px;
}
table.field_list {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 0px;
  border-collapse: collapse;
  empty-cells: show;
  background-color: #eeeeee;
  padding: 0px;
  margin: 0px;
}
table.record_list {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 0px;
  border-collapse: collapse;
  empty-cells: show;
  background-color: white;
  padding: 0px;
  margin: 0px;
}
table.record_tools {
  border-style: solid;
  border-color: #eeeeee;
  border-width: 1px;
  border-collapse: collapse;
  empty-cells: show;
  background-color: #eeeeee;
  padding: 2px;
  margin: 0px;
}
table.record_status {
  border-style: solid;
  border-width: 1px;
  border-color: #eeeeee;
  border-collapse: collapse;
  empty-cells: show;
  background-color: #eeeeee;
  margin:0px;
  padding:0px;
}
table.record_spec {
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  border-collapse: collapse;
  empty-cells: show;
  padding: 0px;
  margin: 0px;
}
table.noborder {
  border-style: solid;
  border-color: gray;
  border-width: 0px 0px 1px 0px;
  border-collapse: collapse;
  border-spacing:10px;
  empty-cells: show;
  padding: 0px;
  margin: 0px;
}

/*
 iframe
*/

iframe.frame {
  border-style: solid;
  border-color: gray;
  border-width: 0px;
  background-color: white;
  padding: 0px;
  margin: 0px;
}
iframe.work {
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  background-color: white;
  padding: 0px;
  margin: 0px;
}
iframe.inserter {
  width: 100%;
  height: 0px;
  border-style: none;
  border-color: gray;
  border-width: 1px;
  background-color: white;
  padding: 0px;
  margin: 0px;
}
iframe.uploader {
  width: 100%;
  height: 0px;
  border-style: none;
  border-color: gray;
  border-width: 1px;
  background-color: white;
  padding: 0px;
  margin: 0px;
}

/*
 th
*/

th.login {
  border-style: solid;
  border-color: #eeeeee;
  border-width: 1px;
  border-collapse: collapse;
  empty-cells: show;
  background-color:#eeeeee;
  padding: 4px;
  margin: 0px;
}
th.table_list_odd {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 0px 1px 1px 0px;
  background:ivory;
  font-weight:normal;
  padding: 2px;
  margin: 0px;
}
th.table_list_even {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 0px 1px 1px 0px;
  background:white;
  font-weight:normal;
  padding: 2px;
  margin: 0px;
}
th.table_list {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 0px 1px 1px 0px;
  background:#eeeeee;
  font-weight:normal;
  padding: 2px;
  margin: 0px;
}
th.table_dummy {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 1px;
  background:#eeeeee;
  font-weight:normal;
  padding: 2px;
  margin: 0px;
}
th.field_list {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 1px;
  background:#eeeeee;
  font-weight:normal;
  padding: 2px 2px 2px 2px;
  margin: 0px;
}
th.field_element {
  border-style: solid;
  border-color: gray;
  border-width: 0px;
  background-color:#eeeeee;
  font-weight:normal;
  text-align:left;
  padding: 1px 1px 1px 2px;
  margin: 0px;
}
th.record_list {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 1px 1px 1px 1px;
  background:#eeeeee;
  font-weight:normal;
  padding: 2px;
  margin: 0px;
}
th.record_spec {
  white-space:nowrap;
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  background:#eeeeee;
  font-weight:normal;
  text-align:left;
  padding: 2px;
  margin: 0px;
}
th.field_conf {
  /* width:200; */
  white-space:nowrap;
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  background:#eeeeee;
  font-weight:normal;
  text-align:left;
  padding: 1px;
  margin: 0px;
}
th.field_conf2 {
  white-space:nowrap;
  border-style: solid;
  border-color: gray;
  border-width: 0px 1px 0px 1px;
  background:#eeeeee;
  font-weight:normal;
  text-align:left;
  padding: 1px;
  margin: 0px;
}

/*
 td
*/

td.login {
  border-style: solid;
  border-color: #eeeeee;
  border-width: 1px;
  border-collapse: collapse;
  empty-cells: show;
  background-color:#eeeeee;
  padding: 4px;
  margin: 0px;
}
td.table_list_odd {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 0px 0px 1px 0px;
  background:ivory;
  font-weight:normal;
  padding: 2px;
  margin: 0px;
}
td.table_list_even {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 0px 0px 1px 0px;
  background:white;
  font-weight:normal;
  padding: 2px;
  margin: 0px;
}
td.record_list {
  border-style: solid;
  border-color: #d3d3d3;
  border-width: 1px 1px 1px 1px;
  font-weight:normal;
  padding: 2px;
  margin: 0px;
}
td.record_frame {
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  padding: 0px;
  margin: 0px;
}
td.record_status {
  padding: 4px;
}
td.record_spec {
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  background:white;
  font-weight:normal;
  text-align:left;
  padding: 2px;
  margin: 0px;
}
td.record_edit {
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  background:white;
  font-weight:normal;
  text-align:left;
  padding: 0px;
  margin: 0px;
}
td.field_conf {
  white-space:nowrap;
  border-style: solid;
  border-color: gray;
  border-width: 1px;
  background:white;
  font-weight:normal;
  text-align:left;
  padding: 1px 3px 1px 1px;
  margin: 0px;
  width:100%;
}
td.field_conf0 {
  white-space:nowrap;
  border-style:none;
  border-width:0px;
  background:white;
  font-weight:normal;
  padding: 0px;
  margin: 0px;
}
td.field_conf2 {
  white-space:nowrap;
  border-style: solid;
  border-color: gray;
  border-width: 0px;
  background:white;
  font-weight:normal;
  text-align:left;
  padding: 0px;
  margin: 0px;
}

/*
 img
*/

img.icon {
  width:16px;
  height:16px;
  border:0px;
  vertical-align:middle;
}

/*
 form
*/

select {
  border: #808080 solid 1px;
  margin: 1px;
}
textarea {
  border: #808080 solid 1px;
  padding-left: 2px;
  padding-right: 2px;
  margin: 1px;
}
input[type="text"],input[type="password"] {
  border: #808080 solid 1px;
  padding-left: 2px;
  padding-right: 2px;
  margin: 1px;
}
input[type="checkbox"],input[type="radio"] {
  border: #808080 solid 0px;
  margin: 1px;
  vertical-align:middle;
}
input[type="submit"],input[type="cancel"],input[type="button"] {
  border: #808080 solid 1px;
  margin: 1px;
}
input[type="file"] {
  border: #808080 solid 0px;
  margin: 1px;
}
input[type="button"].tab-l {
  background:white;
  border: #808080 solid 1px;
  border-width: 1px 0px 0px 1px;
  margin: 1px 0px 0px 1px;
  padding: 0px;
}
input[type="button"].tab-r {
  background:white;
  border: #808080 solid 1px;
  border-width: 1px 1px 0px 0px;
  margin: 1px 1px 0px 0px;
  padding: 0px;
}--></style>
<table class="record_spec" width="100%" style="margin-bottom:1px">
<tr>
<th class="record_spec" width="100%" nowrap>
[日付時刻/日付時刻型]の設定

</th>
<th class="record_spec"><input type="submit" id="exec" name="exec" value="設定" >
</th>
</tr>
</table>
<table class="record_spec" width="100%" style="margin-bottom:1px">
<tr>
<th class="field_conf">
<script type="text/javascript"><!--
var tips_0 = "この項目が必須項目かどうかを設定します。\n「必須」にすると入力が無い場合にはエラーになります。";
--></script>
必須項目<span class="help"><tt>[?]</tt></span>
</th>
<td class="field_conf">
<input type="radio" id="e697a5e4bb98e69982e588bb.valid" name="e697a5e4bb98e69982e588bb.valid" value="true" >必須
<input type="radio" id="e697a5e4bb98e69982e588bb.valid" name="e697a5e4bb98e69982e588bb.valid" value="false"  checked>任意

</td>
</tr>
<tr>
<th class="field_conf">
<script type="text/javascript"><!--
var tips_1 = "この項目が重複するデータを許可するかどうかを設定します。\n「する」の場合データが重複しているとエラーになります。";
--></script>
重複許可<span class="help"><tt>[?]</tt></span>
</th>
<td class="field_conf">
<input type="radio" id="e697a5e4bb98e69982e588bb.duplicate" name="e697a5e4bb98e69982e588bb.duplicate" value="true"  checked>する
<input type="radio" id="e697a5e4bb98e69982e588bb.duplicate" name="e697a5e4bb98e69982e588bb.duplicate" value="false" >しない
</td>
</tr>
<tr>
<th class="field_conf">

<script type="text/javascript"><!--
var tips_2 = "<span class=\"help\"><tt>[?]</tt></span>にマウスを置いた時にこの項目を表示します。説明文にはHTMLのタグを書くことができます。";
--></script>
項目の説明<span class="help"><tt>[?]</tt></span>
</th>
<td class="field_conf"><textarea id="e697a5e4bb98e69982e588bb.description" name="e697a5e4bb98e69982e588bb.description" cols="60" rows="2"  style="width:100%"></textarea>
</td>
</tr>
<tr>
<th class="field_conf">年</th>
<td class="field_conf0">
<table class="noborder">
<td class="field_conf2"><input type="checkbox" id="e697a5e4bb98e69982e588bb.year_enable" name="e697a5e4bb98e69982e588bb.year_enable" value="true" checked >使う</td>

<th class="field_conf2">入力方法</th>
<td class="field_conf2">
<input type="radio" id="e697a5e4bb98e69982e588bb.year_select" name="e697a5e4bb98e69982e588bb.year_select" value="true"  checked>選択
<input type="radio" id="e697a5e4bb98e69982e588bb.year_select" name="e697a5e4bb98e69982e588bb.year_select" value="false" >入力
</td>
<th class="field_conf2">セパレータ</th>
<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.year_sep" name="e697a5e4bb98e69982e588bb.year_sep"  style="width:80px">
<option value="年"  selected>年</option>
<option value="/" >/</option>
<option value="-" >-</option>
<option value=" " >スペース</option>

<option value="" >なし</option>
</select>
</td>
<!--
<th class="field_conf2">西暦/和暦</th>
<td class="field_conf2">
<input type="radio" id="e697a5e4bb98e69982e588bb.year_wareki" name="e697a5e4bb98e69982e588bb.year_wareki" value="false"  checked>西暦
<input type="radio" id="e697a5e4bb98e69982e588bb.year_wareki" name="e697a5e4bb98e69982e588bb.year_wareki" value="true" >和暦
</td>
-->
<th class="field_conf2">選択範囲</th>
<td class="field_conf2">
<input type="text" id="e697a5e4bb98e69982e588bb.year_from" name="e697a5e4bb98e69982e588bb.year_from" value="2006"  size="4" style="text-align: right">&nbsp;〜&nbsp;
<input type="text" id="e697a5e4bb98e69982e588bb.year_to" name="e697a5e4bb98e69982e588bb.year_to" value="2017"  size="4" style="text-align: right">&nbsp;まで
</td>
<td class="field_conf2" width="100%"></td>
</table>
</td>
</tr>
<tr>

<th class="field_conf">月</th>
<td class="field_conf0">
<table class="noborder">
<td class="field_conf2"><input type="checkbox" id="e697a5e4bb98e69982e588bb.month_enable" name="e697a5e4bb98e69982e588bb.month_enable" value="true" checked >使う</td>
<th class="field_conf2">入力方法</th>
<td class="field_conf2">
<input type="radio" id="e697a5e4bb98e69982e588bb.month_select" name="e697a5e4bb98e69982e588bb.month_select" value="true"  checked>選択
<input type="radio" id="e697a5e4bb98e69982e588bb.month_select" name="e697a5e4bb98e69982e588bb.month_select" value="false" >入力
</td>
<th class="field_conf2">セパレータ</th>
<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.month_sep" name="e697a5e4bb98e69982e588bb.month_sep"  style="width:80px">
<option value="月"  selected>月</option>

<option value="/" >/</option>
<option value="-" >-</option>
<option value=" " >スペース</option>
<option value="" >なし</option>
</select>
</td>
<td class="field_conf2" width="100%"></td>
</table>
</td>
</tr>
<tr>
<th class="field_conf">日</th>

<td class="field_conf0">
<table class="noborder">
<td class="field_conf2"><input type="checkbox" id="e697a5e4bb98e69982e588bb.day_enable" name="e697a5e4bb98e69982e588bb.day_enable" value="true" checked >使う</td>
<th class="field_conf2">入力方法</th>
<td class="field_conf2">
<input type="radio" id="e697a5e4bb98e69982e588bb.day_select" name="e697a5e4bb98e69982e588bb.day_select" value="true"  checked>選択
<input type="radio" id="e697a5e4bb98e69982e588bb.day_select" name="e697a5e4bb98e69982e588bb.day_select" value="false" >入力
</td>
<th class="field_conf2">セパレータ</th>
<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.day_sep" name="e697a5e4bb98e69982e588bb.day_sep"  style="width:80px">
<option value="日"  selected>日</option>
<option value=" " >スペース</option>

<option value="" >なし</option>
</select>
</td>
<td class="field_conf2" width="100%"></td>
</table>
</td>
</tr>
<tr>
<th class="field_conf">時</th>
<td class="field_conf0">
<table class="noborder">
<td class="field_conf2"><input type="checkbox" id="e697a5e4bb98e69982e588bb.hour_enable" name="e697a5e4bb98e69982e588bb.hour_enable" value="true"  >使う</td>
<th class="field_conf2">入力方法</th>

<td class="field_conf2">
<input type="radio" id="e697a5e4bb98e69982e588bb.hour_select" name="e697a5e4bb98e69982e588bb.hour_select" value="true"  checked>選択
<input type="radio" id="e697a5e4bb98e69982e588bb.hour_select" name="e697a5e4bb98e69982e588bb.hour_select" value="false" >入力
</td>
<th class="field_conf2">セパレータ</th>
<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.hour_sep" name="e697a5e4bb98e69982e588bb.hour_sep"  style="width:80px">
<option value="時" >時</option>
<option value="hour"  selected>hour</option>
<option value="H" >H</option>
<option value=":" >:</option>
<option value="" >なし</option>

</select>
</td>
<th class="field_conf2">選択ステップ</th>
<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.hour_step" name="e697a5e4bb98e69982e588bb.hour_step"  style="width:80px">
<option value="1"  selected>１時間</option>
<option value="2" >２時間</option>
<option value="3" >３時間</option>
<option value="4" >４時間</option>
<option value="6" >６時間</option>
<option value="8" >８時間</option>

<option value="12" >12時間</option>
</select>
</td>
<td class="field_conf2" width="100%"></td>
</table>
</td>
</tr>
<tr>
<th class="field_conf">分</th>
<td class="field_conf0">
<table class="noborder">
<td class="field_conf2"><input type="checkbox" id="e697a5e4bb98e69982e588bb.min_enable" name="e697a5e4bb98e69982e588bb.min_enable" value="true"  >使う</td>
<th class="field_conf2">入力方法</th>

<td class="field_conf2">
<input type="radio" id="e697a5e4bb98e69982e588bb.min_select" name="e697a5e4bb98e69982e588bb.min_select" value="true"  checked>選択
<input type="radio" id="e697a5e4bb98e69982e588bb.min_select" name="e697a5e4bb98e69982e588bb.min_select" value="false" >入力
</td>
<th class="field_conf2">セパレータ</th>
<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.min_sep" name="e697a5e4bb98e69982e588bb.min_sep"  style="width:80px">
<option value="分" >分</option>
<option value="min"  selected>min</option>
<option value="M" >M</option>
<option value=":" >:</option>
<option value="" >なし</option>

</select>
</td>
<th class="field_conf2">選択ステップ</th>
<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.min_step" name="e697a5e4bb98e69982e588bb.min_step"  style="width:80px">
<option value="5" >５分</option>
<option value="10"  selected>10分</option>
<option value="15" >15分</option>
<option value="20" >20分</option>
<option value="30" >30分</option>
</select>

</td>
<td class="field_conf2" width="100%"></td>
</table>
</td>
</tr>
<tr>
<th class="field_conf">秒</th>
<td class="field_conf0">
<table class="noborder" style="border-bottom: 0px">
<td class="field_conf2"><input type="checkbox" id="e697a5e4bb98e69982e588bb.sec_enable" name="e697a5e4bb98e69982e588bb.sec_enable" value="true"  >使う</td>
<th class="field_conf2">入力方法</th>
<td class="field_conf2">
<input type="radio" id="e697a5e4bb98e69982e588bb.sec_select" name="e697a5e4bb98e69982e588bb.sec_select" value="true"  checked>選択
<input type="radio" id="e697a5e4bb98e69982e588bb.sec_select" name="e697a5e4bb98e69982e588bb.sec_select" value="false" >入力

</td>
<th class="field_conf2">セパレータ</th>
<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.sec_sep" name="e697a5e4bb98e69982e588bb.sec_sep"  style="width:80px">
<option value="秒" >秒</option>
<option value="sec"  selected>sec</option>
<option value="S" >S</option>
<option value="" >なし</option>
</select>
</td>
<th class="field_conf2">選択ステップ</th>

<td class="field_conf2">
<select id="e697a5e4bb98e69982e588bb.sec_step" name="e697a5e4bb98e69982e588bb.sec_step"  style="width:80px">
<option value="5" >５秒</option>
<option value="10" >10秒</option>
<option value="15" >15秒</option>
<option value="20" >20秒</option>
<option value="30"  selected>30秒</option>
</select>
</td>
<td class="field_conf2" width="100%"></td>
</table>
</td>

</tr>
</table>
<table class="record_spec" width="100%">
<tr>
<th class="record_spec" width="100%"></th>
<th class="record_spec"><input type="submit" id="exec" name="exec" value="設定" >
</th>
</tr>
<input type="hidden" id="table" name="table" value="test" >
<input type="hidden" id="table_list_limit" name="table_list_limit" value="5" >
<input type="hidden" id="record_list_limit" name="record_list_limit" value="10" >
<input type="hidden" id="where" name="where" value="" >
<input type="hidden" id="sort" name="sort" value="roid" >
<input type="hidden" id="sort_dir" name="sort_dir" value="asc" >
<input type="hidden" id="offset" name="offset" value="0" >
<input type="hidden" id="roid" name="roid" value="0" >
<input type="hidden" id="field" name="field" value="日付時刻" >
</table>
 ===

 === code
table['日付時刻'].entry()
 ===

　と書くだけで、

 === quote
<select id="rd.2.日付時刻.yy" name="rd.2.日付時刻.yy" >
<option value="2006" >2006</option>
<option value="2007"  selected>2007</option>

<option value="2008" >2008</option>
<option value="2009" >2009</option>
<option value="2010" >2010</option>
<option value="2011" >2011</option>
<option value="2012" >2012</option>
<option value="2013" >2013</option>
<option value="2014" >2014</option>
<option value="2015" >2015</option>
<option value="2016" >2016</option>

<option value="2017" >2017</option>
</select>
年
<select id="rd.2.日付時刻.mm" name="rd.2.日付時刻.mm" >
<option value="01" >01</option>
<option value="02" >02</option>
<option value="03" >03</option>
<option value="04" >04</option>
<option value="05" >05</option>
<option value="06" >06</option>

<option value="07" >07</option>
<option value="08" >08</option>
<option value="09" >09</option>
<option value="10" >10</option>
<option value="11"  selected>11</option>
<option value="12" >12</option>
</select>
月
<select id="rd.2.日付時刻.dd" name="rd.2.日付時刻.dd" >
<option value="01" >01</option>

<option value="02" >02</option>
<option value="03" >03</option>
<option value="04" >04</option>
<option value="05" >05</option>
<option value="06" >06</option>
<option value="07" >07</option>
<option value="08" >08</option>
<option value="09" >09</option>
<option value="10" >10</option>

<option value="11" >11</option>
<option value="12" >12</option>
<option value="13" >13</option>
<option value="14"  selected>14</option>
<option value="15" >15</option>
<option value="16" >16</option>
<option value="17" >17</option>
<option value="18" >18</option>
<option value="19" >19</option>

<option value="20" >20</option>
<option value="21" >21</option>
<option value="22" >22</option>
<option value="23" >23</option>
<option value="24" >24</option>
<option value="25" >25</option>
<option value="26" >26</option>
<option value="27" >27</option>
<option value="28" >28</option>

<option value="29" >29</option>
<option value="30" >30</option>
<option value="31" >31</option>
</select>
日
 ===

　と言った形式でWebの入力を設定する事ができます。数十行から場合によっては数百行になるコードを、たった一行書くだけで実現できるわけです。また、こうして入力したデータは自動的にデータベースに保存可能なデータに変換されて、対応するデータベースのフィールドに格納されます。

=== 用途型のフィールド

　((*Rodeo*))では一般的なデータベースと同じようにテーブル、レコード、フィールドの概念を使用していますが、フィールドに関しては独自のフィールド定義を行なっているため、データベースのフィールドの概念とは異なった扱いになっています。そのためデータベースのフィールドとRodeoのフィールドを混同しないように注意が必要です。

　Webページとデータベースフィールドとの関係は次の図のような関係になります。

### rodeo-field.gif フィールドの関係図

　この様に((*Rodeo*))のフィールドは、Webページとデータベースのフィールドとの橋渡しを行なうことによりお互いのデータの入出力を円滑に行なえるようにしています。((*Rodeo*))のフィールドには「プロパティー」があり、この値を設定することで、フィールドの動作をコントロールできます(先の((*日付時刻型の設定*))がこのプロパティーです)。

　以下に代表的な((*Rodeo*))のフィールド型を示します。

 === table ((*Rodeo*))のフィールド型
型名			機能の概要
整数型			整数型のフィールド、整数値(個数など)を格納する目的で使用します。
文字列型		文字列のフィールド
メモ型			複数行の入力が出来る文字列型のフィールド
論理型			論理値を扱うためのフィールド
日付時刻型		日付と時間の入出力が可能なフィールド
画像型			画像ファイルを管理するためのフィールド
整数配列型		整数型の配列のフィールド
文字列配列型		文字列の配列のフィールド
単純選択型		別のマスタテーブルを参照して選択できる機能
その他付き選択型	単純選択型に「その他」の項目を追加したもの
アカウント型		ユーザアカウントの管理
パスワード型		パスワードの入力ができる文字列型のフィールド
Googleマップ型		GoogleMapの機能を使用してフィールドとして地図を表示
 ===

　普通、データベースの型といえば、integer,char,string,text,timestampなどなど、プログラマでなければ到底わからないものがデータ型として定義されています。実際、((*Rodeo*))が裏で使用しているPostgreSQLにはこれらの型があり、((*Rodeo*))もこれらの型を使用しています。

　しかし、エンドユーザ(((いわゆる「普通の人」(^^;)。)))が、データベースを使おうとする時、こういった基本的なデータ型を使ってデータベースを管理するのはかなり大変であると思いますし、実際にはきちんとアプリケーションを作らなければ満足のいくものにはならないでしょう。

　((*Rodeo*))の型は「データ型」ではありません。正確な呼び名はないと思いますが、「用途型」と呼ぶことができそうな型になっています。表の最初の５つはデータ型と一致した型になっていますが、残りのフィールド型は普通のデータベースには存在しません。

　「googleマップ型」はデータベース上は、point型、文字列型などのデータを組み合わせて使用していますが、エンドユーザにはそんなことはどうでもいいことです。やりたいことは「データベースに地図を登録しておき」「webから簡単に利用できるよう」にしたいだけです。

　((*Rodeo*))では「やりたいこと」がフィールド型と直結しているため、目的に一番近いフィールド型を選んで、そのフィールド型の設定を目的に合わせて設定することで簡単にデータベースが構築できるようになっています。

　上記のフィールド型はこれで終わりと言うものではありません。ある属性を持ったデータを扱いたいとき、それに一致するフィールド型がなければ、それに「対応するフィールド型を作って」行くのが、((*Rodeo*))での正しいデータの扱い方です。例えば、住所を扱いたい場合、文字列型でも一応目的ははたしますが、「郵便番号を入れたら住所を補完できるようにしたい」とか「その住所の地図をすぐに表示したい」などの機能が欲しいとします。この場合、新しく「住所型」のフィールド型を作り、それに先ほどのような機能を実装します。

　このように((*Rodeo*))では「フィールド型」と「機能」および、その「目的」が一体となっていて、フィールド型を拡張することで様々なアプリケーションとしての機能を実装できるようになっています。

= Rodeoの使い方

　以下、((*Rodeo*))のデータベースマネージャの操作方法について説明します。

== ログイン

　以下のような((*rodeo*))のURLにブラウザでアクセスします。URLについてはシステム管理者にお問い合わせください。

 === quote ((*Rodeo*))のURL
http://www.sgmail.jp/rodeo/
 ===

　次のようなログイン画面になりますので、((*ユーザID*))と((*パスワード*))を入力して((*[ログイン]*))ボタンを押してください。

### login.png ログイン画面

　ログインすると次のような表示になります。

### window-paine.gif 操作画面の構成

　この画面以降は、ログアウトするまで画面全体が変化することはありません。画面の一部だけが変化します。以下に画面各部の主な役割を説明します。

 === table 画面各部の役割
領域			役割
ロゴ/クレジット表示	((*Rodeo*))のロゴとクレジットを表示します。ロゴをクリックすると((*[Welcome]*))領域に((*Rodeo*))のマニュアルを表示します。
テーブル操作領域	テーブルの一覧を表示します。テーブルの作成・表示・削除などの操作、パスワードの変更、ログアウトの操作を行なうことができます。
作業領域切り替えタブ	テーブルを選択するとレコード一覧を作業領域に表示しますが、それと同時にテーブル名のタブを表示します。タブをクリックすると操作を行ないたいテーブルを切り替えることができます。また((*[×]*))ボタンをクリックすると作業領域を閉じることができます。現在表示されているタブは白い背景色になります。
作業領域		ログイン直後はこのマニュアルを表示します。パスワード変更をクリックするとパスワード変更画面、テーブル名をクリックするとレコード一覧画面に変わります。表示は上部のタブで切り替えることができます。
 ===

== テーブル操作

　テーブルの操作は、上記のテーブル操作領域で行ないます。操作と動作を次に示します。

 === table テーブル操作
操作			動作
((*[削除]*))をクリック	同じ列に表示されているテーブルを削除します。確認ダイアログが表示されますので、削除して良ければ((*[OK]*))のボタンを押してください。一度削除したテーブルは復活できません。
テーブル名をクリック	テーブルのレコード一覧を作業領域に表示します。
スクロールバー		テーブル一覧をスクロールします。
パスワード変更		パスワード変更画面を作業領域に表示します。旧パスワードと新パスワード(確認のため２回入力します)を入力し、((*[実行]*))ボタンを押してください。
ログアウト		((*Rodeo*))データベースマネージャを終了します。ログアウト後は、ログイン画面の表示になります。
テーブル追加		ボタンの左側の領域にテーブル名を入力してこのボタンを押すと、新しいテーブルを作ることができます。新しいテーブルはすぐに作業領域に表示します。この状態では((*roid*))フィールドだけがある空のテーブルができますので、必要に応じてフィールドを追加します。
 ===

== レコード一覧

　先のテーブル操作領域でテーブル名をクリックしたり、新しくテーブルを作成した場合、((*作業領域*))の画面が次のような画面に変わります(((実際にレコードがある場合の表示です。)))。

### record-list.gif レコード一覧

　この画面もテーブル一覧と同じようにいくつかの領域に分かれています。それぞれの領域の主な役割は以下のようになります。

 === table レコード一覧画面各部の役割
領域		役割
フィールド領域	フィールドの設定、表示順の変更、レコードの並べ替え、レコードの絞り込み、フィールドの削除の機能があります。
レコード領域	レコードの操作を行ないます。左側の((*[表示]*))、((*[編集]*))、((*[削除]*))のリンクをクリックするとそれに応じた動作を行ないます。レコード一覧に((<../image/enable/camera-photo.png>))のようなアイコンがある場合、フィールドに依存する機能が付けられていることがあります。
操作領域	レコード追加、フィールド追加、表示レコードの切り替え、ステータス情報の表示などの機能を受け持っています。
作業領域	レコードの詳細表示、レコードの入力、レコードの編集、フィールドの設定などの表示や入力に使います。
 ===

== フィールド操作

　((*Rodeo*))のフィールドは自由に追加したり削除したりすることができます。

 === table フィールドの操作
操作						動作
フィールド名をクリック				作業領域にそのフィールドの設定画面を表示します。((*[設定]*))ボタンを押すとそのフィールドの設定を保存します。フィールドとその設定についての詳細は、((-4-))を参照してください。
数値を選択					フィールド名の隣にある数値を選択すると、そのフィールドの表示位置をその番号の位置に移動します。((*[roid]*))フィールドは移動できません。
((*[▲/▼]*))をクリック				レコードの表示順をそのフィールドの昇順・降順に並べ替えます。このマークがないフィールドは並べ替えに使用することはできません。
((*[全]*))をクリック				絞り込みを解除し、すべてのレコードを表示します。
((*[絞]*))をクリック				絞り込み条件入力画面を作業領域に表示します。絞り込みをおこなうと、絞り込み条件に一致したレコードだけを一覧に表示します。
((*[×]*))をクリック				そのフィールドを削除します。削除したデータやフィールドは復活できません。確認ダイアログがでますから削除したい場合は((*[OK]*))ボタンを押してください。
((<../image/enable/object-flip-vertical.png>))	ウインドウの大きさによっては作業領域が小さく、データ入力などが行いにくい場合があります。このボタンを押すとレコード一覧を縮小して作業領域を拡大します。再度押すと元に戻ります。
 ===

　フィールドを追加するには、((*操作領域*))でフィールドの型を選択し、フィールド名を入力して((*[追加]*))ボタンを押します。フィールドは一覧の最後に追加されます。次の図は作成直後の「test1」テーブルに「文字列」型の「氏名」フィールドを追加した状態を示します。

### shimei-add.png

== レコード操作

　レコード一覧は10件を1ページとした表示になっています。
　以下にレコードの操作方法を示します。

 === table レコードの操作
操作						動作
((*[表示]*))をクリック				クリックしたレコードの詳細を作業領域に表示します。
((*[編集]*))をクリック				クリックしたレコードの編集画面を作業領域に表示します(下図参照)。
((*[削除]*))をクリック				クリックしたレコードを削除します。削除したレコードは復活できません。確認ダイアログがでますので削除したい場合は((*[OK]*))ボタンを押してください。
((<../image/enable/stock_data-first.png>))	先頭のレコード一覧を表示します。
((<../image/enable/stock_data-previous.png>))	10件前のレコード一覧を表示します。
((<../image/enable/stock_data-next.png>))	10件後のレコード一覧を表示します。
((<../image/enable/stock_data-last.png>))	最後のレコード一覧を表示します。
[数値]						10件を1ページとしたレコード一覧の<数値>ページめのレコード一覧を表示します。
 ===

　次の図は、レコード編集画面の例です(((作業領域が小さいので((<../image/enable/object-flip-vertical.png>))を使って作業領域を拡大しています。)))。

### record-edit.png レコード編集画面

= フィールドの詳細

　((*Rodeo*))の本質とも言えるのがこのフィールドです。((-2.3.5-))でも説明したようにいかにフィールドを使いこなすかが((*Rodeo*))でデータを管理するコツとも言えます。

　まず、テーブルに追加するフィールドがどういった意味を持つフィールドか検討してください。例えば、単純に「住所だから文字列型だろう」と考えてはいけません。その住所を使って地図を表示したいとか、入力の補助として

　以下に((*Rodeo*))がデフォルトで提供するフィールドについてその詳細を説明します。

== 整数型

 === quote 設定項目
<link rel="stylesheet" href="rodeo.css" type="text/css"></link>
<table class="record_spec" width="100%" style="margin-bottom:1px">
<form action="rodeo.cgi" method="post"  style="margin: 0px"><input type="hidden" id="action" name="action" value="do_field_config" >
<tr>

<th class="record_spec" width="100%" nowrap>
[整数型]の設定
</th>
<th class="record_spec"><input type="submit" id="exec" name="exec" value="設定" >
</th>
</tr>
</table>
<table class="record_spec" width="100%" style="margin-bottom:1px">
<tr>
<th class="field_conf">
<script type="text/javascript"><!--
var tips_0 = "この項目が必須項目かどうかを設定します。\n「必須」にすると入力が無い場合にはエラーになります。";
--></script>
必須項目<span class="help" onMouseOver="return overlib(tips_0,CAPTION,'必須項目')" onMouseOut="return nd()"><tt>[?]</tt></span>
</th>
<td class="field_conf">

<input type="radio" id="e695b4e695b0.valid" name="e695b4e695b0.valid" value="true" >必須
<input type="radio" id="e695b4e695b0.valid" name="e695b4e695b0.valid" value="false"  checked>任意
</td>
</tr>
<tr>
<th class="field_conf">
<script type="text/javascript"><!--
var tips_1 = "この項目が重複するデータを許可するかどうかを設定します。\n「する」の場合データが重複しているとエラーになります。";
--></script>
重複許可<span class="help" onMouseOver="return overlib(tips_1,CAPTION,'重複許可')" onMouseOut="return nd()"><tt>[?]</tt></span>
</th>
<td class="field_conf">
<input type="radio" id="e695b4e695b0.duplicate" name="e695b4e695b0.duplicate" value="true"  checked>する
<input type="radio" id="e695b4e695b0.duplicate" name="e695b4e695b0.duplicate" value="false" >しない
</td>
</tr>

<tr>
<th class="field_conf">
<script type="text/javascript"><!--
var tips_2 = "<span class=\"help\"><tt>[?]</tt></span>にマウスを置いた時にこの項目を表示します。説明文にはHTMLのタグを書くことができます。";
--></script>
項目の説明<span class="help" onMouseOver="return overlib(tips_2,CAPTION,'項目の説明')" onMouseOut="return nd()"><tt>[?]</tt></span>
</th>
<td class="field_conf"><textarea id="e695b4e695b0.description" name="e695b4e695b0.description" cols="60" rows="2"  style="width:100%"></textarea>
</td>
</tr>
<tr>
<th class="field_conf">初期値
</th>
<td class="field_conf"><input type="text" id="e695b4e695b0.default" name="e695b4e695b0.default" value=""  size="10" style="text-align: right"></td>
</tr>

<tr>
<th class="field_conf">入力桁数
</th>
<td class="field_conf"><input type="text" id="e695b4e695b0.cols" name="e695b4e695b0.cols" value="10"  size="10" style="text-align: right"></td>
</tr>
</table>
<table class="record_spec" width="100%">
<tr>
<th class="record_spec" width="100%"></th>
<th class="record_spec"><input type="submit" id="exec" name="exec" value="設定" >
</th>
</tr>
<input type="hidden" id="table" name="table" value="test" >
<input type="hidden" id="table_list_limit" name="table_list_limit" value="5" >
<input type="hidden" id="record_list_limit" name="record_list_limit" value="10" >
<input type="hidden" id="where" name="where" value="" >
<input type="hidden" id="sort" name="sort" value="roid" >

<input type="hidden" id="sort_dir" name="sort_dir" value="asc" >
<input type="hidden" id="offset" name="offset" value="0" >
<input type="hidden" id="roid" name="roid" value="0" >
<input type="hidden" id="field" name="field" value="整数" >
</form>
</table>
 ===

　ごく一般的な整数の入出力が可能です。設定では、初期値及び入力桁数の設定が可能です。

= Rodeoを使ったプログラミング


= リファレンスマニュアル


