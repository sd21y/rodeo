# -*- coding: utf-8 -*-

#
# Googleマップ型
#

class RFgooglemap < RFBase
  attr_accessor :point
  attr_accessor :zoom
  def self.name(); "Googleマップ" end
  def self.type(); [RFPoint.type(),RFInt.type()] end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['point'] = {} unless(@conf['point'])
    @conf['zoom'] = {} unless(@conf['zoom'])
    @point = RFPoint.new(name+"_0",table,conf['point'])
    @zoom = RFInt.new(name+"_1",table,conf['zoom'])
    @conf['google_key'] = ""      unless(@conf['google_key'])
    @conf['map_size'] = [500,300] unless(@conf['map_size'])
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['addr1'] = @table.rodeo["#{@fid}.addr1"]
    @conf['addr2'] = @table.rodeo["#{@fid}.addr2"]
    @conf['addr3'] = @table.rodeo["#{@fid}.addr3"]
    @conf['marker'] = @table.rodeo["#{@fid}.marker"]
    @conf['google_key'] = @table.rodeo["#{@fid}.google_key"]
    @conf['map_size'] = [
      @table.rodeo["#{@fid}.map_size_x"],
      @table.rodeo["#{@fid}.map_size_y"]
    ]
  end
  # CGIからの取り込み
  def from_cgi()
    @point.value = [@table.rodeo[cgi_name("x")],@table.rodeo[cgi_name("y")]]
    @zoom.value = @table.rodeo[cgi_name("z")].to_i
  end
  # データベースからの取り込み
  def from_db(db)
    @point.from_db(db)
    @zoom.from_db(db)
  end
  # データベースへの書き込みデータの作成
  def to_db()
    [@point.to_db(),@zoom.to_db()]
  end
  def from_csv(data)
    if(data)
      ary = data.split(/,/,3)
      @point.from_csv("#{ary[0]},#{ary[1]}")
      @zoom.from_csv(ary[2])
    end
  end
  def to_csv()
    if(@point.value)
      %Q("#{@point.value[0]},#{@point.value[1]},#{@zoom.value}")
    else
      %Q("")
    end
  end
  # 入力
  def entry()
    super() {
      @table.rodeo.eruby(__FILE__,"google_map_entry",binding)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(list)
        if(@point.value)
          Rodeo.puts %Q(経度:#{@point.value[0]},緯度:#{@point.value[1]},ズーム:#{@zoom.value})
        end
      else
        if(@point.value)
          @table.rodeo.eruby(__FILE__,"google_map_display",binding)
        end
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("住所1")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.addr1",@conf['addr1']) {
        @table.rodeo.html_item(nil,"なし")
        @table.farray.each {|h|
          if(h['sort'] != 0)
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("住所2")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.addr2",@conf['addr2']) {
        @table.rodeo.html_item(nil,"なし")
        @table.farray.each {|h|
          if(h['sort'] != 0)
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("住所3")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.addr3",@conf['addr3']) {
        @table.rodeo.html_item(nil,"なし")
        @table.farray.each {|h|
          if(h['sort'] != 0)
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("マーカー")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.marker",@conf['marker']) {
        @table.rodeo.html_item(nil,"なし")
        @table.farray.each {|h|
          if(h['sort'] != 0)
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf">アクセスキー</th>
  <td class="field_conf">
    <%@table.rodeo.html_text("#{@fid}.google_key",@conf['google_key'],{"style"=>"width:100%"})%><br>
    <a href="http://www.google.com/apis/maps/signup.html" target="_blank">キーの取得</a>
  </td>
</tr>
<tr>
  <th class="field_conf">マップサイズ</th>
  <td class="field_conf">
    Ｘ&nbsp;<%@table.rodeo.html_number("#{@fid}.map_size_x",@conf['map_size'][0],{"size"=>4})%>&nbsp;
    Ｙ&nbsp;<%@table.rodeo.html_number("#{@fid}.map_size_y",@conf['map_size'][1],{"size"=>4})%>
  </td>
</tr>

#### google_map_display

<%
  @point.value = [140.33988118171692,37.39429192713356] unless(@point.value)
  @zoom.value = 13 unless(@zoom.value)
%>
<script src="http://maps.google.com/maps?file=api&v=2&key=<%=@conf['google_key']%>" type="text/javascript"></script>
<script type="text/javascript">
  on_load_fook[on_load_fook.length] = function() {
    if(GBrowserIsCompatible()) {
      var gmap = document.getElementById("googlemap");
      var pos = new GLatLng(<%=@point.value[1]%>,<%=@point.value[0]%>);
      map = new GMap2(gmap);
      map.setCenter(pos,<%=@zoom.value%>);
      map.addControl(new GSmallMapControl());
      map.addControl(new GOverviewMapControl());
      geo = new GClientGeocoder();
      var marker = new GMarker(pos);
      <%if(@conf['marker'] != "")%>
        GEvent.addListener(marker, "click", function() {
          var html = '<%=CGI::escapeHTML(@table[@conf['marker']].value)%>';
          marker.openInfoWindowHtml(html);
        });
      <%end%>
      map.addOverlay(marker);
    }
  }
</script>
<div class="googlemap" id="googlemap" style="width:<%=@conf['map_size'][0]%>px;height:<%=@conf['map_size'][1]%>px;"></div>

#### google_map_entry

<%if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)%>
<%else%>
<%
  @point.value = [140.33988118171692,37.39429192713356] unless(@point.value)
  @zoom.value = 13 unless(@zoom.value)
  @table.rodeo.html_hidden(cgi_name("x"),@point.value[0])
  @table.rodeo.html_hidden(cgi_name("y"),@point.value[1])
  @table.rodeo.html_hidden(cgi_name("z"),@zoom.value)
%>
<script src="http://maps.google.com/maps?file=api&v=2&key=<%=@conf['google_key']%>" type="text/javascript"></script>
<script type="text/javascript">
  map = null;
  on_load_fook[on_load_fook.length] = function() {
    if(GBrowserIsCompatible()) {
      var gmap = document.getElementById("googlemap");
      var posx = document.getElementById("<%=cgi_name("x")%>");
      var posy = document.getElementById("<%=cgi_name("y")%>");
      var zoom = document.getElementById("<%=cgi_name("z")%>");
      var pos = new GLatLng(Number(posy.value),Number(posx.value));
      map = new GMap2(gmap);
      map.setCenter(pos,Number(zoom.value));
      map.addControl(new GSmallMapControl());
      map.addControl(new GOverviewMapControl());
      geo = new GClientGeocoder();
      var marker = new GMarker(pos);
      map.addOverlay(marker);
      GEvent.addListener(map,'click',function(overlay,center) {
        if(center != null) {
          document.getElementById('<%=cgi_name("x")%>').value = center.x;
          document.getElementById('<%=cgi_name("y")%>').value = center.y;
          map.clearOverlays();
          var marker = new GMarker(center);
          map.addOverlay(marker);
        }
      });
      GEvent.addListener(map,'zoomend',function(oldzoom,newzoom) {
        document.getElementById('<%=cgi_name("z")%>').value = newzoom;
      });
    }
  }
  function gmap_find() {
    addr = "";
    <%if(@conf['addr1'] != "")%>
      addr += document.getElementById("<%=@table[@conf['addr1']].cgi_name()%>").value;
    <%end%>
    <%if(@conf['addr2'] != "")%>
      addr += document.getElementById("<%=@table[@conf['addr2']].cgi_name()%>").value;
    <%end%>
    <%if(@conf['addr3'] != "")%>
      addr += document.getElementById("<%=@table[@conf['addr3']].cgi_name()%>").value;
    <%end%>
    geo.getLatLng(addr,function(center) {
      if(center != null) {
        document.getElementById('<%=cgi_name("x")%>').value = center.x;
        document.getElementById('<%=cgi_name("y")%>').value = center.y;
        map.clearOverlays();
        map.setCenter(center);
        var marker = new GMarker(center);
        map.addOverlay(marker);
      }
    });
  }
</script>
<input type="button" onclick="gmap_find();return false;" value="地図検索">
<div class="googlemap" id="googlemap" style="width:<%=@conf['map_size'][0]%>px;height:<%=@conf['map_size'][1]%>px;"></div>
<%end%>

