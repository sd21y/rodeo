# -*- coding: utf-8 -*-

#
# メール送信型
#

require "#{File.dirname(__FILE__)}/../email.rb"

class RFmmagsend < RFPlugin
  def self.name(); "メールマガジン発行" end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['mail_title'] = @table.rodeo["#{@fid}.mail_title"]
    @conf['from_address'] = @table.rodeo["#{@fid}.from_address"]
    @conf['to_field'] = @table.rodeo["#{@fid}.to_field"]
    @conf['info_text'] = @table.rodeo["#{@fid}.info_text"]
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super()
    @table.rodeo.output()
  end
  # 
  def mmag_send()
    to = @table[@conf['to_field']].value.untaint
    if(to and to.strip != "")
      mail = Email.new()
      mail.smtpServer = "smtp.sgmail.jp"
      mail['To'] = to
      mail['From'] = (@conf['from_address']).untaint
      mail['Subject'] = @conf['mail_title'].mime_encode
      mail['Content-Type'] = "text/plain; charset=ISO-2022-JP"
      @conf['info_text'].each_line {|line|
        line.chomp!
        line.gsub!(/\r/,"")
        line.gsub!(/(%[^%]*%)/) {
          key = $1[1..-2]
          if(@table[key])
            text = @table[key].value.to_s
          else
            text = "?"
          end
          text
        }
        line.gsub(/㈱/,"(株)").gsub(/㈲/,"(有)")
        mail << line.tojis
      }
      begin
      mail.send()
       @log << %Q(roid:#{@table.roid} #{to} 送信しました。<br>\n)
      rescue Exception
       @log << %Q(roid:#{@table.roid} #{to} <font color="red">送信できませんでした。</font><br>\n)
      end
    else
      @log << %Q(roid:#{@table.roid} <font color="red">送信先アドレスがありません。</font><br>\n)
    end
  end
  # 登録完了メール送信タグ
  def display(list)
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super()
    @table.rodeo.eruby(__FILE__,"mmag_entry",binding)
    @table.rodeo.output()
  end
  # アプリケーション画面の表示
  def action_mmag_conf()
    @conf['mail_title'] = @table.rodeo["#{@fid}.mail_title"]
    @conf['info_text'] = @table.rodeo["#{@fid}.info_text"]
    where = nil
    where = @table.rodeo.where if(@table.rodeo.where)
    @table.fetch(0,1,where) {|roid|}
    @table.rodeo.eruby(__FILE__,"mmag_conf",binding)
    @table.rodeo.output()
  end
  # アプリケーション画面の表示
  def action_mmag_exec()
    @conf['mail_title'] = @table.rodeo["#{@fid}.mail_title"]
    @conf['info_text'] = @table.rodeo["#{@fid}.info_text"]
    @log = ""
    if(@table.rodeo['back'] != "")
      @table.rodeo.eruby(__FILE__,"mmag_entry",binding)
    else
      where = nil
      where = @table.rodeo.where if(@table.rodeo.where)
      @table.fetch(0,@table.count(where),where) {|roid|
        mmag_send()
      }
      @table.rodeo.eruby(__FILE__,"regmail_done",binding)
    end
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信タイトル","メールのタイトルを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.mail_title",@conf['mail_title'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信元アドレス","送信元のアドレスを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.from_address",@conf['from_address'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信先フィールド","宛先のFromアドレスとして使用するフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.to_field",@conf['to_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFstring")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("メール本文","メルマガのテンプレートを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_textarea("#{@fid}.info_text",@conf['info_text'],60,10,{"style"=>"width:100%"})%></td>
</tr>

#### mmag_entry

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>メールマガジン送信</title>
  </head>
  <body>
    [[ メールマガジン送信 ]]
    <hr>
    メールマガジンを送信します。<br>
    下記の文面を変更して確認ボタンを押してください。<br>
    <%@table.rodeo.html_form("#{@appname}") {%>
      <hr>
      <table class="field_conf">
        <tr>
          <th class="field_conf"><%@table.rodeo.html_tooltip("送信タイトル","メールのタイトルを設定します。")%></th>
          <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.mail_title",@conf['mail_title'],{"style"=>"width:100%"})%></td>
        </tr>
        <tr>
          <th class="field_conf"><%@table.rodeo.html_tooltip("メール本文","メルマガのテンプレートを設定します。")%></th>
          <td class="field_conf"><%@table.rodeo.html_textarea("#{@fid}.info_text",@conf['info_text'],60,10,{"style"=>"width:100%"})%></td>
        </tr>
      </table>
      <hr>
      <%@table.rodeo.html_hidden("action","mmag_conf")%>
      <%@table.rodeo.html_submit("submit","確認")%><br>
    <%}%>
  </body>
</html>

#### mmag_conf

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>メールマガジン送信</title>
  </head>
  <body>
    [[ メールマガジン送信 ]]
    <hr>
    下記の文面のメールマガジンを送信します。<br>
    よろしいですか？<br>
    <%@table.rodeo.html_form("#{@appname}") {%>
      <hr>
      <tt>
      Subject: <%=@conf['mail_title']%><br>
      From: <%=@conf['from_address']%><br>
      <br>
      <%
        @conf['info_text'].each_line {|line|
          line.chomp!
          line.gsub!(/\r/,"")
          line.gsub!(/(%[^%]*%)/) {
            key = $1[1..-2]
            if(@table[key])
              text = @table[key].value.to_s
            else
              text = "?"
            end
            text
          }
          line.gsub(/㈱/,"(株)").gsub(/㈲/,"(有)")
          Rodeo.print line
          Rodeo.puts "<br>"
        }
      %>
      </tt>
      <hr>
      <%@table.rodeo.html_hidden("#{@fid}.mail_title",@conf['mail_title'])%>
      <%@table.rodeo.html_hidden("#{@fid}.info_text",@conf['info_text'])%>
      <%@table.rodeo.html_hidden("action","mmag_exec")%>
      <%@table.rodeo.html_submit("submit","送信")%>
      <%@table.rodeo.html_submit("back","戻る")%><br>
    <%}%>
  </body>
</html>

#### regmail_done

<html>
  <head>
    <%@table.rodeo.html_rodeo_javascript()%>
    <title>メールマガジン送信終了</title>
  </head>
  <body>
    [[ メールマガジン送信終了 ]]
    <hr>
    メールマガジンの送信を終了しました。<br>
    送信結果は以下の通りです。<br>
    <hr>
    <%@log.each_line {|line|%>
      <%=line%>
    <%}%>
    <hr>
  </body>
</html>

