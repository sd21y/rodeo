# -*- coding: utf-8 -*-

#
# csv出力型
#

require 'csv'

class Iconv
	def initialize(incode,outcode)
  end
  def iconv(data)
  	data
  end
end

class RFcsv_export < RFPlugin
  def self.name(); "CSVエクスポート" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      @table.rodeo.eruby(__FILE__,"app_exec",binding)
      @table.rodeo.output()
    }
  end
  # CSVエクスポート
  def action_csv_export()
    @error = ""
    @message = ""
    su = Iconv.new("CP932","UTF8")
#    begin
      File.open("export/#{@table.name.untaint}.csv","w:cp932") {|csv|
        rec = []
        @table.farray.each {|v|
          if(v['fobj'].class.superclass != RFPlugin and v['fobj'].class != RFcalc)
            rec << '"'+su.iconv(v['name'])+'"'
          end
        }
        csv.puts rec.join(",")
        @table.fetch(0,@table.count(@table.rodeo.where),@table.rodeo.where,"roid") {|roid|
          rec = []
          @table.farray.each {|v|
            if(v['fobj'].class.superclass != RFPlugin and v['fobj'].class != RFcalc)
              if(v['name'] == "roid")
                rec << v['fobj'].value.to_s
              else
                data = v['fobj'].to_csv()
                begin
                  rec << su.iconv(data)
                rescue Exception
                  rec << "???"
                end
              end
            end
          }
          csv.puts rec.join(",")
        }
      }
#    rescue Exception
#      @error << "CSVファイルの出力に失敗しました(#{$!.message})。<br>\n"
#    end
    if(@error != "")
      @table.rodeo.eruby(__FILE__,"export_error",binding)
    else
      @table.rodeo.eruby(__FILE__,"export_done",binding)
    end
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<!-- config -->

#### app_exec

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body>
    <%@table.rodeo.html_form(@table.rodeo.appname) {%>
      <%@table.rodeo.html_hidden("action","csv_export")%>
      <table class="record_spec" width="100%"><tr>
        <th class="record_spec" width="100%"></th>
        <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
        <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","実行(R)",{"accesskey"=>"R"})%></th>
      </tr></table>
      データをCSVファイルに出力します。<br>
      <table class="record_spec" width="100%"><tr>
        <th class="record_spec" width="100%"></th>
        <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
        <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","実行(R)",{"accesskey"=>"R"})%></th>
      </tr></table>
    <%}%>
  </body>
</html>

#### export_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body onload="parent.record_list_update(true,true,true,true,null);">
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
    CSVファイルを作成しました。下記のリンクからダウンロードできます。<br>
    <a href="export/<%=@table.name%>.csv"><%=@table.name%>.csvファイル</a>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
  </body>
</html>

#### export_error

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
    CSVファイルの作成に失敗しました。<br>
    <br>
    <font color="red"><%=@error%></font><br>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
  </body>
</html>

