# -*- coding: utf-8 -*-

#
# 論理型
#

class RFBool < RFBase
  def self.name(); "論理" end
  def self.type(); "bool" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['default'] = true if(@conf['default'] == nil)
    @value = nil
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      yield() if(block_given?)
    }
  end
  # データベースからの取り込み
  def from_db(db)
    if(db[db_name])
      case db[db_name()]
      when "t" then @value = true
      when "f" then @value = false
      else
        @value = nil
      end
    else
      @value = nil
    end
  end
  # データベースへの書き込みデータの作成
  def to_db()
    case @value
    when true  then "true"
    when false then "false"
    else
      nil
    end
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name("hidden")] != "")
        case @table.rodeo[cgi_name("hidden")]
        when "true"  then @value = true
        when "false" then @value = false
        else
          @value = nil
        end
      else
        yield() if(block_given?)
      end
		}
  end
  #
  def hidden()
    @table.rodeo.html_hidden(cgi_name("hidden"),@value.to_s)
  end
  # データベースからの取り込み
  def from_csv(data)
    case data
    when "t",/^true$/i,/^1$/  then @value = true
    when "f",/^false$/i,/^0$/ then @value = false
    else
      @value = nil
    end
  end
  def to_csv()
    case @value
    when true  then %Q("true")
    when false then %Q("false")
    else
      %Q("")
    end
  end
end

__END__

