# -*- coding: utf-8 -*-

#
# パスワード型
#

class RFpasswd < RFString
  def self.name(); "パスワード" end
  def self.type(); RFString.type() end
  # CGIからの取り込み
  def from_cgi()
    super() {
      p1 = @table.rodeo[cgi_name("1")]
      p2 = @table.rodeo[cgi_name("2")]
      if(p1 !="" or p2 != "")
        if(p1 == p2)
          salt = "$1$"+[rand(62),rand(62),rand(62),rand(62),rand(62),rand(62),rand(62),rand(62)
          ].pack("C*").tr("\x00-\x3f","A-Za-z0-9")
          @value = p1.crypt(salt)
        else
          @error = "パスワードが一致していません。"
        end
      end
    }
  end
  # パスワードの設定
  def set_passwd(pwd)
    salt = "$1$"+[rand(62),rand(62),rand(62),rand(62),rand(62),rand(62),rand(62),rand(62)
    ].pack("C*").tr("\x00-\x3f","A-Za-z0-9")
    @value = pwd.crypt(salt)
  end
  # 入力
  def entry(n=nil)
    super() {
      case n
      when "1" then @table.rodeo.html_password(cgi_name("1"),"",{"size"=>16})
      when "2" then @table.rodeo.html_password(cgi_name("2"),"",{"size"=>16})
      else
        @table.rodeo.html_password(cgi_name("1"),"",{"size"=>16})
        @table.rodeo.html_password(cgi_name("2"),"",{"size"=>16})
      end
      @table.rodeo.html_hidden(cgi_name(),@value)
    }
  end
  # 入力
  def entry1()
    @table.rodeo.html_password(cgi_name("1"),"",{"size"=>16})
  end
  # 入力
  def entry2()
    @table.rodeo.html_password(cgi_name("2"),"",{"size"=>16})
  end
  # 表示
  def display(list=false)
    super(list) {|list|
#    if(@value)
#      Rodeo.print "********"
#    end
    }
  end
end

__END__

