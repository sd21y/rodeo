# -*- coding: utf-8 -*-

#
# メール送信型
#

require "#{File.dirname(__FILE__)}/../email.rb"

class RFenquete < RFPlugin
  def self.name(); "アンケート" end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['enq_title'] = @table.rodeo["#{@fid}.enq_title"]
    @conf['enq_header'] = @table.rodeo["#{@fid}.enq_header"]
    @conf['enq_footer'] = @table.rodeo["#{@fid}.enq_footer"]
    @conf['to_address'] = @table.rodeo["#{@fid}.to_address"]
    @conf['from_field'] = @table.rodeo["#{@fid}.from_field"]
  end
  def inserted(roid)
    mail = Email.new()
    mail.smtpServer = "smtp.sgmail.jp"
    mail['To'] = (@conf['to_address']).untaint
    mail['From'] = (@table[@conf['from_field']].value).untaint
    mail['Subject'] = @conf['enq_title'].mime_encode
    mail['Content-Type'] = "text/plain; charset=ISO-2022-JP"
    @table.farray.each {|h|
      if(h['name'] != "roid" and h['type'] != "RFmemo" and h['fobj'].class.superclass != RFPlugin)
        Rodeo.clear()
        h['fobj'].display(true)
        mail << "#{h['name']}：#{Rodeo.gettext()}".tojis
      end
    }
    Rodeo.clear()
    @table.farray.each {|h|
      if(h['type'] == "RFmemo")
        if(h['fobj'].value)
          mail << ""
          mail << "[[ #{h['name']} ]]".tojis
          h['fobj'].value.each_line {|line|
            line.chomp!
            line.sub!(/\r/,"")
            mail << line.tojis
          }
          mail << ""
        end
      end
    }
    mail.send()
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      @table.clear()
      @table.rodeo.eruby(__FILE__,"enq_start",binding)
      @table.rodeo.output()
    }
  end
  # アプリケーション画面の表示
  def action_enq_entry()
    if(@table.eval_cgi() > 0)
      @table.rodeo.eruby(__FILE__,"enq_start",binding)
    else
      @table.rodeo.eruby(__FILE__,"enq_entry",binding)
    end
    @table.rodeo.output()
  end
  # アプリケーション画面の表示
  def action_enq_done()
    @table.eval_cgi()
    if(@table.rodeo['back'] != "")
      @table.rodeo.eruby(__FILE__,"enq_start",binding)
    else
      @table.insert_mode = true
      @table.insert()
      @table.rodeo.eruby(__FILE__,"enq_done",binding)
    end
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("入力テンプレート","入力テンプレートを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.enq_entry",@conf['enq_entry'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("確認テンプレート","確認テンプレートを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.enq_confirm",@conf['enq_confirm'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("終了テンプレート","終了テンプレートを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.enq_done",@conf['enq_done'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信タイトル","送信時のタイトルを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.enq_title",@conf['enq_title'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("送信アドレス","メールの宛先を設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.to_address",@conf['to_address'],:size=>60)%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("Fromフィールド","メールを送信する時のFromアドレスとして使用するフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.from_field",@conf['from_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFstring")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>

#### enq_start

<html>
  <head>
    <title><%=@conf['enq_title']%></title>
  </head>
  <body>
    下記のアンケートにお答えください。<br>
    <font color="red">※</font>のついた項目は必須項目です。<br>
    <hr>
    <%
      pref = @conf['prefix']
      pref = "Q." if(pref == nil or pref.strip == "")
      @table.rodeo.html_form(@table.rodeo.appname) {
        @table.rodeo.html_hidden("action","enq_entry")
        i = 0
        # アンケート項目
        @table.farray.each {|h|
          case h['type']
          when 'RFroid','RFenquete'
          else
            if(h['fobj'].conf['description'] =~ /^Q./i)
              Rodeo.print "#{pref}#{i+1} "
              Rodeo.print %Q(<font color="red">※ </font>) if(h['fobj'].conf['valid'])
              Rodeo.print %Q(<font color="red">) if(h['fobj'].error)
              h['fobj'].conf['description'][2..-1].each_line {|line|
                Rodeo.print line
                Rodeo.puts "<br>"
              }
              Rodeo.print %Q(</font>) if(h['fobj'].error)
              Rodeo.puts "<br>"
              h['fobj'].entry()
              Rodeo.puts "<br>"
              Rodeo.puts "<br>"
              i += 1
            end
          end
        }
        Rodeo.print %Q(お客様情報をご記入ください。<br>)
        Rodeo.print %Q(<font color="red">※</font>のついた項目は必須項目です。<br>)
        Rodeo.puts "<hr>"
        # 個人情報等
        @table.farray.each {|h|
          case h['type']
          when 'RFroid','RFenquete'
          else
            if(h['fobj'].conf['description'] !~ /^Q./i)
              Rodeo.print %Q(<font color="red">※ </font>) if(h['fobj'].conf['valid'])
              Rodeo.print %Q(<font color="red">) if(h['fobj'].error)
              h['fobj'].conf['description'].each_line {|line|
                Rodeo.print line
                Rodeo.puts "<br>"
              }
              Rodeo.print %Q(</font>) if(h['fobj'].error)
              Rodeo.puts "<br>"
              h['fobj'].entry()
              Rodeo.puts "<br>"
              Rodeo.puts "<br>"
            end
          end
        }
        Rodeo.puts "<hr>"
        @table.rodeo.html_submit("submit","応募する")
      }
    %>
    <hr>
    ご記入いただいた個人情報は、商品のご案内及びマーケティング目的のための統計的データの形で利用させていただきます。<br>
  </body>
</html>

#### enq_entry

<html>
  <head>
    <title><%=@conf['enq_title']%></title>
  </head>
  <body>
    アンケートの内容をご確認ください。<br>
    <hr>
    <%
      pref = @conf['prefix']
      pref = "Q." if(pref == nil or pref.strip == "")
      @table.rodeo.html_form(@table.rodeo.appname) {
        @table.rodeo.html_hidden("action","enq_done")
        i = 0
        # アンケート項目
        @table.farray.each {|h|
          case h['type']
          when 'RFroid','RFenquete'
          else
            if(h['fobj'].conf['description'] =~ /^Q./i)
              Rodeo.print "#{pref}#{i+1} "
              h['fobj'].conf['description'][2..-1].each_line {|line|
                Rodeo.print line
                Rodeo.puts "<br>"
              }
              Rodeo.puts "<br>"
              h['fobj'].display()
              h['fobj'].hidden()
              Rodeo.puts "<br>"
              Rodeo.puts "<br>"
              i += 1
            end
          end
        }
        Rodeo.puts "<hr>"
        # 個人情報等
        @table.farray.each {|h|
          case h['type']
          when 'RFroid','RFenquete'
          else
            if(h['fobj'].conf['description'] !~ /^Q./i)
              h['fobj'].conf['description'].each_line {|line|
                Rodeo.print line
                Rodeo.puts "<br>"
              }
              Rodeo.puts "<br>"
              h['fobj'].display()
              h['fobj'].hidden()
              Rodeo.puts "<br>"
              Rodeo.puts "<br>"
            end
          end
        }
        Rodeo.puts "<hr>"
        @table.rodeo.html_submit("submit","送信する")
        @table.rodeo.html_submit("back","戻る")
      }
    %>
    <hr>
    ご記入いただいた個人情報は、商品のご案内及びマーケティング目的のための統計的データの形で利用させていただきます。<br>
  </body>
</html>

#### enq_done

<html>
  <head>
    <title><%=@conf['enq_title']%></title>
  </head>
  <body>
    ご応募有難うございました。<br>
  </body>
</html>

