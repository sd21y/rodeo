# -*- coding: utf-8 -*-

#
# 都道府県選択型
#

class RFselpref < RFIntArray
@@PREFS = [
  [ 1,"北海道"  ],[ 2,"青森県"  ],[ 3,"岩手県"  ],[ 4,"宮城県"  ],[ 5,"秋田県"  ],
  [ 6,"山形県"  ],[ 7,"福島県"  ],[ 8,"茨城県"  ],[ 9,"栃木県"  ],[10,"群馬県"  ],
  [11,"埼玉県"  ],[12,"千葉県"  ],[13,"東京都"  ],[14,"神奈川県"],[15,"新潟県"  ],
  [16,"富山県"  ],[17,"石川県"  ],[18,"福井県"  ],[19,"山梨県"  ],[20,"長野県"  ],
  [21,"岐阜県"  ],[22,"静岡県"  ],[23,"愛知県"  ],[24,"三重県"  ],[25,"滋賀県"  ],
  [26,"京都府"  ],[27,"大阪府"  ],[28,"兵庫県"  ],[29,"奈良県"  ],[30,"和歌山県"],
  [31,"鳥取県"  ],[32,"島根県"  ],[33,"岡山県"  ],[34,"広島県"  ],[35,"山口県"  ],
  [36,"徳島県"  ],[37,"香川県"  ],[38,"愛媛県"  ],[39,"高知県"  ],[40,"福岡県"  ],
  [41,"佐賀県"  ],[42,"長崎県"  ],[43,"熊本県"  ],[44,"大分県"  ],[45,"宮崎県"  ],
  [46,"鹿児島県"],[47,"沖縄県"  ],
]
  def self.name(); "選択(都道府県)" end
  def self.type(); RFIntArray.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # ソートフィールド名
  def order()
    db_name()
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    ary = []
    @table.rodeo.cgi.each {|k,v|
      if(k =~ /^operator\.(\d+)$/)
        ary << %Q(#{v} = ANY("#{db_name()}"))
      end
      if(k =~ /^operator\.$/)
        ary << %Q("#{db_name()}" = '{}')
      end
    }
    if(ary.size > 0)
      where = "(" + ary.join(" or ") + ")"
    else
      where = nil
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    ary = []
    @table.select(roid)
    if(@value)
      @value.each {|v|
        ary << %Q(#{v} = ANY("#{db_name()}"))
      }
    end
    if(ary.size > 0)
      where = "(" + ary.join(" or ") + ")"
    else
      where = nil
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo[cgi_name()])
        if(@table.rodeo[cgi_name()] != "")
          @value = [@table.rodeo[cgi_name()].to_i]
        else
          @value = []
        end
      else
        @value = nil
      end
    }
  end
  #
  def to_csv()
    rc = nil
    if(@value)
      @@PREFS.each {|v|
        if(@value[0] == v[0])
          rc = v[1]
        end
      }
    end
    rc
  end
  #
  def from_csv(data)
    @value = nil
    if(data)
      @@PREFS.each {|v|
        if(v[1] == data)
          @value = [v[0]]
        end
      }
    end
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      @table.rodeo.eruby(__FILE__,"select_entry",binding)
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      @@PREFS.each {|v|
        if(v[0] == @value[0])
          Rodeo.puts v[1]
        end
      }
    }
  end
end

__END__

#### select_entry

<span id="master_list_<%=@fid%>">
  <%
    @table.rodeo.html_select(cgi_name(),@value[0]) {
      @@PREFS.each {|pref| @table.rodeo.html_item(pref[0],pref[1]) }
    }
  %>
</span>

#### where_entry

<td class="field_conf">
  <%@table.rodeo.html_check("operator",[]) {%>
    <%i = 0%>
    <table><tr>
      <%@@PREFS.each {|pref|%>
        <td><%@table.rodeo.html_item(pref[0],pref[1])%></td>
        <%i += 1%>
        <%if((i % 10) == 0)%>
          </tr><tr>
        <%end%>
      <%}%>
      <td><%@table.rodeo.html_item("","該当なし")%></td>
    </tr></table>
  <%}%>
</td>

