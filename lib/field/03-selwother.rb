# -*- coding: utf-8 -*-

#
# その他付き選択型
#

class RFselwother < RFBase
  attr :select
  def self.name(); "選択(その他付き)" end
  def self.type(); [RFselect.type(),RFstring.type()] end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['select'] = {} unless(@conf['select'])
    @conf['select']['with_other'] = true
    @conf['select']['default'] = "その他"
    @select = RFselect.new(name+"_0",table,@conf['select'])
    @conf['other'] = {} unless(@conf['other'])
    @other = RFstring.new(name+"_1",table,@conf['other'])
  end
  def init()
    super()
    @select.init()
  end
  def value()
    @select.value
  end
  def clear()
    @select.clear()
    @other.clear()
  end
  # ソートフィールド名
  def order()
    db_name("0")
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      Rodeo.print %Q(<th class="field_conf" colspan="2">選択動作の設定</th>)
      @select.config_entry(false)
      Rodeo.print %Q(<th class="field_conf" colspan="2">その他の設定</th>)
      @other.config_entry(false)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @select.eval_config()
    @other.eval_config()
  end
  # where
  def where_entry(sw=false)
    @select.where_entry(sw)
  end
  # where
  def eval_where()
    @select.eval_where()
  end
  # where
  def eval_where_marked(roid)
    @select.eval_where_marked(roid)
  end
  # CGIデータの評価
  def from_cgi()
    @select.from_cgi()
    @other.from_cgi()
  end
  # データベースからの取り込み
  def from_db(db)
    @select.from_db(db)
    @other.from_db(db)
  end
  # データベースへの書き込みデータの作成
  def to_db()
    [@select.to_db(),@other.to_db()]
  end
  # CSV出力
  def to_csv()
    @select.to_csv()
  end
  # CSV入力
  def from_csv(data)
    @select.from_csv(data)
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      @select.entry(opt)
      @other.entry()
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      @select.display(list)
      Rodeo.print ","
      @other.display(list)
    }
  end
  # 隠しフィールド
  def hidden()
    @select.hidden()
    @other.hidden()
  end
end

class RFselwother < RFBase
  # フィールド一覧リロード用
  def action_select_field_update()
    @select.action_select_field_update()
  end
end

__END__

