# -*- coding: utf-8 -*-

#
# 価格.comエクスポート型
#

require 'csv'

class RFkakaku_export < RFPlugin
  def self.name(); "価格.comエクスポート" end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
  end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      @table.rodeo.eruby(__FILE__,"app_exec",binding)
      @table.rodeo.output()
    }
  end
  # CSVエクスポート
  def action_csv_export()
    @error = ""
    @message = ""
    su = Iconv.new("CP932","UTF8")
    File.open("export/#{@table.name.untaint}.csv","w") {|csv|
      rec = [
        "JAN",
        "価格.comの製品名・型番",
        "登録価格",
        "送料",
        "在庫・発送",
        "店頭",
        "カテゴリ",
        "メーカー",
        "製品名・型番",
        "リンク先URL",
        "画像URL",
        "ポイント",
        "一言コメント"
      ]
      csv.puts su.iconv(rec.join(","))
      maker = @table.rodeo.table_new("タイヤ-メーカー")
      series = @table.rodeo.table_new("タイヤ-シリーズ")
      zaiko = @table.rodeo.table_new("タイヤ-在庫")
      where = %Q("掲載" = true)
      @table.fetch(0,@table.count(@table.rodeo.where),@table.rodeo.where,"roid") {|roid|
        maker.select(@table["タイヤメーカー"].value[0])
        series.select(@table["タイヤシリーズ"].value[0])
        zaiko.select(@table["在庫"].value[0])
        rec = [
          @table['JAN'].to_s,
          @table['価格com品名'].to_s,
          @table['単価'].to_s,
          @table['送料'].to_s,
          zaiko['価格comコード'].value.to_s,
          "0",
          series['種別'].to_s,
          maker['タイヤメーカー'].to_s,
          roid.to_s,
          @table['リンク先URL'].to_s,
          @table['画像URL'].to_s,
          "0",
          @table['コメント'].to_s,
        ]
        csv.puts su.iconv(rec.join(","))
      }
    }
    if(@error != "")
      @table.rodeo.eruby(__FILE__,"export_error",binding)
    else
      @table.rodeo.eruby(__FILE__,"export_done",binding)
    end
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<!-- config -->

#### app_exec

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body>
    <%@table.rodeo.html_form(@table.rodeo.appname) {%>
      <%@table.rodeo.html_hidden("action","csv_export")%>
      <table class="record_spec" width="100%"><tr>
        <th class="record_spec" width="100%"></th>
        <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","実行(R)",{"accesskey"=>"R"})%></th>
      </tr></table>
      データをCSVファイルに出力します。<br>
      <table class="record_spec" width="100%"><tr>
        <th class="record_spec" width="100%"></th>
        <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
        <th class="record_spec"><%@table.rodeo.html_submit("exec","実行(R)",{"accesskey"=>"R"})%></th>
      </tr></table>
    <%}%>
  </body>
</html>

#### export_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body onload="parent.record_list_update(true,true,true,true,null);">
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
    CSVファイルを作成しました。下記のリンクからダウンロードできます。<br>
    <a href="export/<%=@table.name%>.csv"><%=@table.name%>.csvファイル</a>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
  </body>
</html>

#### export_error

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
    CSVファイルの作成に失敗しました。<br>
    <br>
    <font color="red"><%=@error%></font><br>
    <table class="record_spec" width="100%"><tr>
      <th class="record_spec" width="100%"></th>
      <th class="record_spec" nowrap><%@table.rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@table.rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
    </tr></table>
  </body>
</html>

