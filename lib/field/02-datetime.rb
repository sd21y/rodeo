# -*- coding: utf-8 -*-

#
# 日付時刻型
#

class RFdatetime < RFTimestamp
  def self.name(); "日付時刻" end
  def self.type(); RFTimestamp.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['valid_nil']    = true               if(@conf['valid_nil'] == nil)
    @conf['default_now']  = true               if(@conf['default_now'] == nil)
    @conf['default_offset']  = 0.0             if(@conf['default_offset'] == nil)
    @conf['entry_enable']  = true              if(@conf['entry_enable'] == nil)
    @conf['year_enable']  = true               if(@conf['year_enable'] == nil)
    @conf['month_enable'] = true               if(@conf['month_enable'] == nil)
    @conf['day_enable']   = true               if(@conf['day_enable'] == nil)
    @conf['hour_enable']  = true               if(@conf['hour_enable'] == nil)
    @conf['min_enable']   = true               if(@conf['min_enable'] == nil)
    @conf['sec_enable']   = true               if(@conf['sec_enable'] == nil)
    @conf['year_select']  = true               if(@conf['year_select'] == nil)
    @conf['month_select'] = true               if(@conf['month_select'] == nil)
    @conf['day_select']   = true               if(@conf['day_select'] == nil)
    @conf['hour_select']  = true               if(@conf['hour_select'] == nil)
    @conf['min_select']   = true               if(@conf['min_select'] == nil)
    @conf['sec_select']   = true               if(@conf['sec_select'] == nil)
    @conf['year_wareki']  = false              if(@conf['year_wareki'] == nil)
    @conf['hour_step']    = 1                  unless(@conf['hour_step'])
    @conf['min_step']     = 10                 unless(@conf['min_step'])
    @conf['sec_step']     = 30                 unless(@conf['sec_step'])
    @conf['year_sep']     = "年"               if(@conf['year_sep'] == nil)
    @conf['month_sep']    = "月"               if(@conf['month_sep'] == nil)
    @conf['day_sep']      = "日"               if(@conf['day_sep'] == nil)
    @conf['hour_sep']     = "時"               if(@conf['hour_sep'] == nil)
    @conf['min_sep']      = "分"               if(@conf['min_sep'] == nil)
    @conf['sec_sep']      = "秒"               if(@conf['sec_sep'] == nil)
    @conf['year_from']    = Time.now.year()-1  unless(@conf['year_from'])
    @conf['year_to']      = Time.now.year()+10 unless(@conf['year_to'])
  end
  # クリア
  def clear()
    super()
    @value = nil
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['valid_nil']      = ((@table.rodeo["#{@fid}.valid_nil"] == "true") ? true : false)
    @conf['default_now']    = ((@table.rodeo["#{@fid}.default_now"] == "true") ? true : false)
    @conf['default_offset'] = eval(@table.rodeo["#{@fid}.default_offset"]).to_f
    @conf['entry_enable']   = ((@table.rodeo["#{@fid}.entry_enable"] == "true") ? true : false)
    @conf['year_enable']    = ((@table.rodeo["#{@fid}.year_enable"] == "true") ? true : false)
    @conf['month_enable']   = ((@table.rodeo["#{@fid}.month_enable"] == "true") ? true : false)
    @conf['day_enable']     = ((@table.rodeo["#{@fid}.day_enable"] == "true") ? true : false)
    @conf['hour_enable']    = ((@table.rodeo["#{@fid}.hour_enable"] == "true") ? true : false)
    @conf['min_enable']     = ((@table.rodeo["#{@fid}.min_enable"] == "true") ? true : false)
    @conf['sec_enable']     = ((@table.rodeo["#{@fid}.sec_enable"] == "true") ? true : false)
    @conf['year_select']    = ((@table.rodeo["#{@fid}.year_select"] == "true") ? true : false)
    @conf['month_select']   = ((@table.rodeo["#{@fid}.month_select"] == "true") ? true : false)
    @conf['day_select']     = ((@table.rodeo["#{@fid}.day_select"] == "true") ? true : false)
    @conf['hour_select']    = ((@table.rodeo["#{@fid}.hour_select"] == "true") ? true : false)
    @conf['min_select']     = ((@table.rodeo["#{@fid}.min_select"] == "true") ? true : false)
    @conf['sec_select']     = ((@table.rodeo["#{@fid}.sec_select"] == "true") ? true : false)
    @conf['year_sep']       = @table.rodeo["#{@fid}.year_sep"]
    @conf['month_sep']      = @table.rodeo["#{@fid}.month_sep"]
    @conf['day_sep']        = @table.rodeo["#{@fid}.day_sep"]
    @conf['hour_sep']       = @table.rodeo["#{@fid}.hour_sep"]
    @conf['min_sep']        = @table.rodeo["#{@fid}.min_sep"]
    @conf['sec_sep']        = @table.rodeo["#{@fid}.sec_sep"]
    @conf['hour_step']      = @table.rodeo["#{@fid}.hour_step"].to_i
    @conf['min_step']       = @table.rodeo["#{@fid}.min_step"].to_i
    @conf['sec_step']       = @table.rodeo["#{@fid}.sec_step"].to_i
    @conf['year_from']      = @table.rodeo["#{@fid}.year_from"].to_i
    @conf['year_to']        = @table.rodeo["#{@fid}.year_to"].to_i
  end
  # 絞りこみ
  # < GR0033 2008.09.05 Chg tr >
  #   履歴表示の検索条件追加に伴い、日付範囲の初期表示を追加
  #   start : 開始日付の初期値（省略時は現在日付）
  #   last  : 終了日付の初期値（省略時は現在日付＋１）
  def where_entry(su=true,start=DateTime.now(),last=DateTime.now()+1)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    from = nil
    yy = (@table.rodeo[cgi_name("from.yy")] != "") ? @table.rodeo[cgi_name("from.yy")].to_i : 1902
    mm = (@table.rodeo[cgi_name("from.mm")] != "") ? @table.rodeo[cgi_name("from.mm")].to_i : 1
    dd = (@table.rodeo[cgi_name("from.dd")] != "") ? @table.rodeo[cgi_name("from.dd")].to_i : 1
    h =  (@table.rodeo[cgi_name("from.h")]  != "") ? @table.rodeo[cgi_name("from.h")].to_i  : 0
    m =  (@table.rodeo[cgi_name("from.m")]  != "") ? @table.rodeo[cgi_name("from.m")].to_i  : 0
    s =  (@table.rodeo[cgi_name("from.s")]  != "") ? @table.rodeo[cgi_name("from.s")].to_i  : 0
    begin
      from = Time.local(yy,mm,dd,h,m,s).strftime("%Y-%m-%d %H:%M:%S")
    rescue Exception
      from = nil
    end
    to = nil
    yy = (@table.rodeo[cgi_name("to.yy")] != "") ? @table.rodeo[cgi_name("to.yy")].to_i : 1902
    mm = (@table.rodeo[cgi_name("to.mm")] != "") ? @table.rodeo[cgi_name("to.mm")].to_i : 1
    dd = (@table.rodeo[cgi_name("to.dd")] != "") ? @table.rodeo[cgi_name("to.dd")].to_i : 1
    h =  (@table.rodeo[cgi_name("to.h")]  != "") ? @table.rodeo[cgi_name("to.h")].to_i  : 0
    m =  (@table.rodeo[cgi_name("to.m")]  != "") ? @table.rodeo[cgi_name("to.m")].to_i  : 0
    s =  (@table.rodeo[cgi_name("to.s")]  != "") ? @table.rodeo[cgi_name("to.s")].to_i  : 0
    begin
      to = Time.local(yy,mm,dd,h,m,s).strftime("%Y-%m-%d %H:%M:%S")
    rescue Exception
      to = nil
    end
    if(from)
      if(to)
        where = %Q("#{@name}" >= '#{from}' and "#{@name}" < '#{to}')
      else
        where = %Q("#{@name}" >= '#{from}')
      end
    else
      if(to)
        where = %Q("#{@name}" < '#{to}')
      else
        where = nil
      end
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    where = nil
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" = #{to_db()})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      yy = (@table.rodeo[cgi_name("yy")] != "") ? @table.rodeo[cgi_name("yy")].to_i : nil
      mm = (@table.rodeo[cgi_name("mm")] != "") ? @table.rodeo[cgi_name("mm")].to_i : nil
      dd = (@table.rodeo[cgi_name("dd")] != "") ? @table.rodeo[cgi_name("dd")].to_i : nil
      h =  (@table.rodeo[cgi_name("h")]  != "") ? @table.rodeo[cgi_name("h")].to_i  : nil
      m =  (@table.rodeo[cgi_name("m")]  != "") ? @table.rodeo[cgi_name("m")].to_i  : nil
      s =  (@table.rodeo[cgi_name("s")]  != "") ? @table.rodeo[cgi_name("s")].to_i  : nil
      yy = 1902 unless(@conf['year_enable'])
      mm = 1 unless(@conf['month_enable'])
      dd = 1 unless(@conf['day_enable'])
      h  = 0 unless(@conf['hour_enable'])
      m  = 0 unless(@conf['min_enable'])
      s  = 0 unless(@conf['sec_enable'])
      if(yy and mm and dd and h and m and s)
        begin
          @value = Time.local(yy,mm,dd,h,m,s)
        rescue Exception
          if(@conf['valid_nil'])
            @value = nil
          else
            @error = "日付が正しくありません。"
          end
        end
      else
        if(@conf['default_now'])
          @value = Time.now() + @conf['default_offset']
        else
          @value = nil
        end
      end
    }
  end
  # 入力
  def entry_body(base,opt={})
    # 年
    if(@conf['year_enable'])
      if(@conf['year_select'])
        default = nil
        default = @value.strftime("%Y") if(@value)
        @table.rodeo.html_select(base+".yy",default,opt) {
          @table.rodeo.html_item("--","----") if(@conf['valid_nil'])
          (@conf['year_from']..@conf['year_to']).each {|v|
            @table.rodeo.html_item(v.to_s,v.to_s)
          }
        }
      else
        @table.rodeo.html_text(base+".yy",@value.strftime("%Y"),{"size"=>4})
      end
      Rodeo.puts @conf['year_sep']
    end
    # 月
    if(@conf['month_enable'])
      if(@conf['month_select'])
        default = nil
        default = @value.strftime("%m") if(@value)
        @table.rodeo.html_select(base+".mm",default) {
          @table.rodeo.html_item("--","--") if(@conf['valid_nil'])
          (1..12).each {|v|
            @table.rodeo.html_item(format("%02d",v),format("%02d",v))
          }
        }
      else
        @table.rodeo.html_text(base+".mm",@value.strftime("%m"),{"size"=>2})
      end
      Rodeo.puts @conf['month_sep']
    end
    # 日
    if(@conf['day_enable'])
      if(@conf['day_select'])
        default = nil
        default = @value.strftime("%d") if(@value)
        @table.rodeo.html_select(base+".dd",default) {
          @table.rodeo.html_item("--","--") if(@conf['valid_nil'])
          (1..31).each {|v|
            @table.rodeo.html_item(format("%02d",v),format("%02d",v))
          }
        }
      else
        @table.rodeo.html_text(base+".dd",@value.strftime("%d"),{"size"=>2})
      end
      Rodeo.puts @conf['day_sep']
    end
    # 時
    if(@conf['hour_enable'])
      if(@conf['hour_select'])
        near = nil
        near = format("%02d",@value.hour() - (@value.hour() % @conf['hour_step'])) if(@value)
        @table.rodeo.html_select(base+".h",near) {
          @table.rodeo.html_item("--","--") if(@conf['valid_nil'])
          0.step(23,@conf['hour_step']) {|v|
            @table.rodeo.html_item(format("%02d",v),format("%02d",v))
          }
        }
      else
        @table.rodeo.html_text(base+".h",@value.strftime("%H"),{"size"=>2})
      end
      Rodeo.puts @conf['hour_sep']
    end
    # 分
    if(@conf['min_enable'])
      if(@conf['min_select'])
        near = nil
        near = format("%02d",@value.min() - (@value.min() % @conf['min_step'])) if(@value)
        @table.rodeo.html_select(base+".m",near) {
          @table.rodeo.html_item("--","--") if(@conf['valid_nil'])
          0.step(59,@conf['min_step']) {|v|
            @table.rodeo.html_item(format("%02d",v),format("%02d",v))
          }
        }
      else
        @table.rodeo.html_text(base+".m",@value.strftime("%M"),{"size"=>2})
      end
      Rodeo.puts @conf['min_sep']
    end
    # 秒
    if(@conf['sec_enable'])
      if(@conf['sec_select'])
        near = nil
        near = format("%02d",@value.sec() - (@value.sec() % @conf['sec_step'])) if(@value)
        @table.rodeo.html_select(base+".s",near) {
          @table.rodeo.html_item("--","--") if(@conf['valid_nil'])
          0.step(59,@conf['sec_step']) {|v|
            @table.rodeo.html_item(format("%02d",v),format("%02d",v))
          }
        }
      else
        @table.rodeo.html_text(base+".s",@value.strftime("%S"),{"size"=>2})
      end
      Rodeo.puts @conf['sec_sep']
    end
  end
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      # 入力許可？
      if(@conf['entry_enable'])
        if(@conf['default_now'])
          @value = Time.now() + @conf['default_offset'] unless(@value)
        end
        entry_body(cgi_name(),opt)
      else
        if(@value)
          @table.rodeo.html_hidden(cgi_name(),@value.to_s)
          display(false)
        end
      end
    }
  end
  # 表示
  def to_s
    s = ""
    if(@value)
      s << @value.strftime("%Y#{@conf['year_sep']}")  if(@conf['year_enable'])
      s << @value.strftime("%m#{@conf['month_sep']}") if(@conf['month_enable'])
      s << @value.strftime("%d#{@conf['day_sep']}")   if(@conf['day_enable'])
      s << @value.strftime("%H#{@conf['hour_sep']}")  if(@conf['hour_enable'])
      s << @value.strftime("%M#{@conf['min_sep']}")   if(@conf['min_enable'])
      s << @value.strftime("%S#{@conf['sec_sep']}")   if(@conf['sec_enable'])
    end
    s
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        Rodeo.print @value.strftime("%Y#{@conf['year_sep']}")  if(@conf['year_enable'])
        Rodeo.print @value.strftime("%m#{@conf['month_sep']}") if(@conf['month_enable'])
        Rodeo.print @value.strftime("%d#{@conf['day_sep']}")   if(@conf['day_enable'])
        Rodeo.print @value.strftime("%H#{@conf['hour_sep']}")  if(@conf['hour_enable'])
        Rodeo.print @value.strftime("%M#{@conf['min_sep']}")   if(@conf['min_enable'])
        Rodeo.print @value.strftime("%S#{@conf['sec_sep']}")   if(@conf['sec_enable'])
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf">入力を許可</th>
  <td class="field_conf">
    <%@table.rodeo.html_radio("#{@fid}.entry_enable",@conf['entry_enable']) {
      @table.rodeo.html_item(true ,"する")
      @table.rodeo.html_item(false,"しない")
    }%>
  </td>
</tr>  
<tr>
  <th class="field_conf">未定義を許可</th>
  <td class="field_conf">
    <%@table.rodeo.html_radio("#{@fid}.valid_nil",@conf['valid_nil']) {
      @table.rodeo.html_item(true ,"する")
      @table.rodeo.html_item(false,"しない")
    }%>
  </td>
</tr>  
<tr>
  <th class="field_conf">今日の日時で初期化</th>
  <td class="field_conf">
    <%@table.rodeo.html_radio("#{@fid}.default_now",@conf['default_now']) {
      @table.rodeo.html_item(true ,"する")
      @table.rodeo.html_item(false,"しない")
    }%>
    &nbsp;オフセット&nbsp;<%@table.rodeo.html_number("#{@fid}.default_offset",@conf['default_offset'],{"size"=>6})%>日
  </td>
</tr>
<tr>
  <th class="field_conf">年</th>
  <td class="field_conf0">
    <table class="noborder">
      <td class="field_conf2"><%@table.rodeo.html_checkbox("#{@fid}.year_enable","使う",@conf['year_enable'])%></td>
      <th class="field_conf2">入力方法</th>
      <td class="field_conf2">
        <%@table.rodeo.html_radio("#{@fid}.year_select",@conf['year_select']) {
          @table.rodeo.html_item(true ,"選択")
          @table.rodeo.html_item(false,"入力")
        }%>
      </td>
      <th class="field_conf2">セパレータ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.year_sep",@conf['year_sep'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item("年","年")
          @table.rodeo.html_item("/" ,"/")
          @table.rodeo.html_item("-" ,"-")
          @table.rodeo.html_item(" " ,"スペース")
          @table.rodeo.html_item("" ,"なし")
        }%>
      </td>
      <!--
      <th class="field_conf2">西暦/和暦</th>
      <td class="field_conf2">
        <%@table.rodeo.html_radio("#{@fid}.year_wareki",@conf['year_wareki']) {
          @table.rodeo.html_item(false,"西暦")
          @table.rodeo.html_item(true ,"和暦")
        }%>
      </td>
      -->
      <th class="field_conf2">選択範囲</th>
      <td class="field_conf2">
        <%@table.rodeo.html_number("#{@fid}.year_from",@conf['year_from'],{"size"=>4})%>
        &nbsp;〜&nbsp;
        <%@table.rodeo.html_number("#{@fid}.year_to",@conf['year_to'],{"size"=>4})%>
        &nbsp;まで
      </td>
      <td class="field_conf2" width="100%"></td>
    </table>
  </td>
</tr>
<tr>
  <th class="field_conf">月</th>
  <td class="field_conf0">
    <table class="noborder">
      <td class="field_conf2"><%@table.rodeo.html_checkbox("#{@fid}.month_enable","使う",@conf['month_enable'])%></td>
      <th class="field_conf2">入力方法</th>
      <td class="field_conf2">
        <%@table.rodeo.html_radio("#{@fid}.month_select",@conf['month_select']) {
          @table.rodeo.html_item(true ,"選択")
          @table.rodeo.html_item(false,"入力")
        }%>
      </td>
      <th class="field_conf2">セパレータ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.month_sep",@conf['month_sep'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item("月","月")
          @table.rodeo.html_item("/" ,"/")
          @table.rodeo.html_item("-" ,"-")
          @table.rodeo.html_item(" " ,"スペース")
          @table.rodeo.html_item("" ,"なし")
        }%>
      </td>
      <td class="field_conf2" width="100%"></td>
    </table>
  </td>
</tr>
<tr>
  <th class="field_conf">日</th>
  <td class="field_conf0">
    <table class="noborder">
      <td class="field_conf2"><%@table.rodeo.html_checkbox("#{@fid}.day_enable","使う",@conf['day_enable'])%></td>
      <th class="field_conf2">入力方法</th>
      <td class="field_conf2">
        <%@table.rodeo.html_radio("#{@fid}.day_select",@conf['day_select']) {
          @table.rodeo.html_item(true ,"選択")
          @table.rodeo.html_item(false,"入力")
        }%>
      </td>
      <th class="field_conf2">セパレータ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.day_sep",@conf['day_sep'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item("日","日")
          @table.rodeo.html_item(" " ,"スペース")
          @table.rodeo.html_item("" ,"なし")
        }%>
      </td>
      <td class="field_conf2" width="100%"></td>
    </table>
  </td>
</tr>
<tr>
  <th class="field_conf">時</th>
  <td class="field_conf0">
    <table class="noborder">
      <td class="field_conf2"><%@table.rodeo.html_checkbox("#{@fid}.hour_enable","使う",@conf['hour_enable'])%></td>
      <th class="field_conf2">入力方法</th>
      <td class="field_conf2">
        <%@table.rodeo.html_radio("#{@fid}.hour_select",@conf['hour_select']) {
          @table.rodeo.html_item(true ,"選択")
          @table.rodeo.html_item(false,"入力")
        }%>
      </td>
      <th class="field_conf2">セパレータ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.hour_sep",@conf['hour_sep'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item("時","時")
          @table.rodeo.html_item("hour" ,"hour")
          @table.rodeo.html_item("H" ,"H")
          @table.rodeo.html_item(":" ,":")
          @table.rodeo.html_item("" ,"なし")
        }%>
      </td>
      <th class="field_conf2">選択ステップ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.hour_step",@conf['hour_step'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item( 1,"１時間")
          @table.rodeo.html_item( 2,"２時間")
          @table.rodeo.html_item( 3,"３時間")
          @table.rodeo.html_item( 4,"４時間")
          @table.rodeo.html_item( 6,"６時間")
          @table.rodeo.html_item( 8,"８時間")
          @table.rodeo.html_item(12,"12時間")
        }%>
      </td>
      <td class="field_conf2" width="100%"></td>
    </table>
  </td>
</tr>
<tr>
  <th class="field_conf">分</th>
  <td class="field_conf0">
    <table class="noborder">
      <td class="field_conf2"><%@table.rodeo.html_checkbox("#{@fid}.min_enable","使う",@conf['min_enable'])%></td>
      <th class="field_conf2">入力方法</th>
      <td class="field_conf2">
        <%@table.rodeo.html_radio("#{@fid}.min_select",@conf['min_select']) {
          @table.rodeo.html_item(true ,"選択")
          @table.rodeo.html_item(false,"入力")
        }%>
      </td>
      <th class="field_conf2">セパレータ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.min_sep",@conf['min_sep'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item("分","分")
          @table.rodeo.html_item("min" ,"min")
          @table.rodeo.html_item("M" ,"M")
          @table.rodeo.html_item(":" ,":")
          @table.rodeo.html_item("" ,"なし")
        }%>
      </td>
      <th class="field_conf2">選択ステップ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.min_step",@conf['min_step'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item( 5,"５分")
          @table.rodeo.html_item(10,"10分")
          @table.rodeo.html_item(15,"15分")
          @table.rodeo.html_item(20,"20分")
          @table.rodeo.html_item(30,"30分")
        }%>
      </td>
      <td class="field_conf2" width="100%"></td>
    </table>
  </td>
</tr>
<tr>
  <th class="field_conf">秒</th>
  <td class="field_conf0">
    <table class="noborder" style="border-bottom: 0px">
      <td class="field_conf2"><%@table.rodeo.html_checkbox("#{@fid}.sec_enable","使う",@conf['sec_enable'])%></td>
      <th class="field_conf2">入力方法</th>
      <td class="field_conf2">
        <%@table.rodeo.html_radio("#{@fid}.sec_select",@conf['sec_select']) {
          @table.rodeo.html_item(true ,"選択")
          @table.rodeo.html_item(false,"入力")
        }%>
      </td>
      <th class="field_conf2">セパレータ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.sec_sep",@conf['sec_sep'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item("秒","秒")
          @table.rodeo.html_item("sec" ,"sec")
          @table.rodeo.html_item("S" ,"S")
          @table.rodeo.html_item("" ,"なし")
        }%>
      </td>
      <th class="field_conf2">選択ステップ</th>
      <td class="field_conf2">
        <%@table.rodeo.html_select("#{@fid}.sec_step",@conf['sec_step'],{"style"=>"width:80px"}) {
          @table.rodeo.html_item( 5,"５秒")
          @table.rodeo.html_item(10,"10秒")
          @table.rodeo.html_item(15,"15秒")
          @table.rodeo.html_item(20,"20秒")
          @table.rodeo.html_item(30,"30秒")
        }%>
      </td>
      <td class="field_conf2" width="100%"></td>
    </table>
  </td>
</tr>

#### where_entry

<td class="field_conf">
  <%@conf['valid_nil'] = true%>
  <%@value = start;entry_body(cgi_name("from"))%>&nbsp;～&nbsp;<%@value = last;entry_body(cgi_name("to"))%>
</td>

