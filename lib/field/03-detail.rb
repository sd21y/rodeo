# -*- coding: utf-8 -*-

#
# 明細型
#

class RFdetail < RFIntArray
  def self.name(); "明細" end
  def self.type(); RFIntArray.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['detail_table'] = nil unless(@conf['detail_table'])
    @conf['entry_direction'] = true if(@conf['entry_direction'] == nil)
    @conf['bgcolor'] = "white" unless(@conf['bgcolor'])
    @value = []
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['detail_table'] = @table.rodeo["#{@fid}.detail_table"]
    @conf['entry_direction']   = ((@table.rodeo["#{@fid}.entry_direction"] == "true") ? true : false)
    @conf['bgcolor'] = @table.rodeo["#{@fid}.bgcolor"]
    @conf['bgcolor'] = "white" if(@conf['bgcolor'] == "")
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      val = @table.rodeo[cgi_name()]
      @value = val.split(/,/).collect{|v| v.to_i}
##      @value = nil if(@value == [])
    }
  end
  # DB書き込みデータの作成
  def to_db()
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      table = @table.rodeo.table_new(@conf['detail_table'])
      if(@value)
        newv = []
        @value.each {|v|
          if(table.select(v))
            newv << v
          end
        }
        @value = newv
      end
    end
    super()
  end
  # 追加時の処理
  def inserted(roid)
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      # テンポラリテーブルから削除
      if(@value)
        @value.each {|oid|
          @table.rodeo.sql.exec(%Q(delete from "temps" where "tname" = '#{@conf['detail_table']}' and "roid" = #{oid}))
        }
      end
    end
  end
  # 削除時の処理
  def deleted(roid)
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      table = @table.rodeo.table_new(@conf['detail_table'])
      @value.each {|oid| table.delete(oid) }
    end
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    @value = [] unless(@value)
    @table.rodeo.eruby(__FILE__,"detail_entry",binding)
  end
  # 表示
  def display(list=false)
    super(list) {
      if(list)
        target = @table.rodeo.table_new(@conf['detail_table'])
        update_acct = (@table.rodeo.admin or (target['roid'].conf['record_update'] & @table.rodeo.user_record['グループ'].value != []))
		    item = []
		    @value.each {|roid|
		      if(update_acct)
		        item << %Q(<a href="javascript:record_ref('#{@conf['detail_table']}',#{roid},'edit')">#{roid}</a>)
		      else
		        item << %Q(<a href="javascript:record_ref('#{@conf['detail_table']}',#{roid},'spec')">#{roid}</a>)
		      end
		    }
		    Rodeo.puts item.join(",")
      else
        @display_mode = true
        @table.rodeo.eruby(__FILE__,"detail_display",binding)
      end
    }
  end
end

class RFdetail < RFIntArray
  def action_detail_target_entry()
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      table = @table.rodeo.table_new(@conf['detail_table'])
      table.insert_mode = true
      table.clear()
      @oid = nil
      @error = nil
      @table.rodeo.eruby(__FILE__,"detail_target_entry",binding)
      @table.rodeo.output()
    else
      Rodeo.puts %Q(<font color="red">詳細テーブルを設定してください。</font>)
      @table.rodeo.output()
    end
  end
  def action_detail_target_insert()
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      table = @table.rodeo.table_new(@conf['detail_table'])
      table.insert_mode = true
      @oid = nil
      @error = nil
      if(table.eval_cgi() == 0)
        @oid = table.insert()
        @table.select(@table.rodeo.roid)
        @value << @oid
        @table.update(@table.rodeo.roid)
        @table.rodeo.eruby(__FILE__,"detail_target_entry",binding)
        @table.rodeo.output()
      else
        @error = table.error
        @table.rodeo.eruby(__FILE__,"detail_target_entry",binding)
        @table.rodeo.output()
      end
    else
      Rodeo.puts %Q(<font color="red">詳細テーブルを設定してください。</font>)
      @table.rodeo.output()
    end
  end
  def action_detail_target_edit()
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      table = @table.rodeo.table_new(@conf['detail_table'])
      table.insert_mode = false
      @oid = @table.rodeo['edit'].to_i
      @error = nil
      table.select(@oid)
      @table.rodeo.eruby(__FILE__,"detail_target_edit",binding)
      @table.rodeo.output()
    else
      Rodeo.puts %Q(<font color="red">詳細テーブルを設定してください。</font>)
      @table.rodeo.output()
    end
  end
  def action_detail_target_update()
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      table = @table.rodeo.table_new(@conf['detail_table'])
      table.insert_mode = false
      @oid = @table.rodeo['edit'].to_i
      @error = nil
      table.select(@oid)
      if(table.eval_cgi() > 0)
        @error = table.error
        @table.rodeo.eruby(__FILE__,"detail_target_edit",binding)
        @table.rodeo.output()
      else
        table.update(@oid)
        @table.rodeo.eruby(__FILE__,"detail_target_edit_done",binding)
        @table.rodeo.output()
      end
    else
      Rodeo.puts %Q(<font color="red">詳細テーブルを設定してください。</font>)
      @table.rodeo.output()
    end
  end
  def action_detail_table_update()
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      @value = @table.rodeo['values'].split(/,/)
      @table.rodeo.eruby(__FILE__,"detail_table",binding)
      @table.rodeo.output()
    else
      Rodeo.puts %Q(<font color="red">詳細テーブルを設定してください。</font>)
      @table.rodeo.output()
    end
  end
  def action_detail_table_reload()
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      @table.rodeo.eruby(__FILE__,"detail_table",binding)
      @table.rodeo.output()
    else
      Rodeo.puts %Q(<font color="red">詳細テーブルを設定してください。</font>)
      @table.rodeo.output()
    end
  end
  def action_detail_table_delete()
    if(@conf['detail_table'] != nil and @conf['detail_table'] != "")
      oid = @table.rodeo['delete'].to_i
      #------------------------------------------------ < GR0015 2008.09.02 Chg Start tr >
      # 削除明細のroid(oid)が親のテーブルに登録されている数を調べ、登録数＞１であれば削除しない
      # （送付先の削除に対応）
#     table = @table.rodeo.table_new(@conf['detail_table'])
#     table.delete(oid)
      exist = 0
      @table.fetch(0,@table.count(),%Q('#{oid}' = ANY("#{@name}")),nil) {|id| exist += 1 }
      if(exist <= 1)
        table = @table.rodeo.table_new(@conf['detail_table'])
        table.delete(oid)
      end
      #------------------------------------------------ < GR0015 2008.09.02 Chg End   tr >
      # テンポラリテーブルから削除
      @table.rodeo.sql.exec(%Q(delete from "temps" where "tname" = '#{@conf['detail_table']}' and "roid" = #{oid}))
      @value = @table.rodeo['values'].split(/,/)
      @table.rodeo.eruby(__FILE__,"detail_table",binding)
      @table.rodeo.output()
    else
      Rodeo.puts %Q(<font color="red">詳細テーブルを設定してください。</font>)
      @table.rodeo.output()
    end
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf">詳細テーブル</th>
  <td class="field_conf">
    <%
      @table.rodeo.sql.exec(%Q(select "tablename" from "pg_tables" where tablename~'^rc_*' order by tablename)) {|res|
        @table.rodeo.html_select("#{@fid}.detail_table",@conf['detail_table']) {
          res.each {|ent|
            name = ent['tablename'].sub(/^rc_/,"")
            @table.rodeo.html_item(name,name)
          }
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf">入力方向</th>
  <td class="field_conf">
    <%@table.rodeo.html_radio("#{@fid}.entry_direction",@conf['entry_direction']) {
      @table.rodeo.html_item(true ,"横型")
      @table.rodeo.html_item(false,"縦型")
    }%>
  </td>
</tr>
<tr>
  <th class="field_conf">入力エリアのBGカラー</th>
  <td class="field_conf">
    <%@table.rodeo.html_text("#{@fid}.bgcolor",@conf['bgcolor'])%>
  </td>
</tr>

#### detail_list


<%=@value.collect{|v|v.to_s}.join(",")%>

#### detail_table

<script type="text/javascript">
  function detail_delete_<%=@fid%>(oid) {
    var list = document.getElementById("<%=cgi_name()%>");
    var ary = list.value.split(",");
    var nar = new Array();
    for(var i = 0; i < ary.length; i ++) {
      if(ary[i] != oid) nar.push(ary[i]);
    }
    list.value = nar.join(",");
    var arg = sysargs({
      "action":"detail_table_delete",
      "field":"<%=@name%>",
      "values":list.value,
      "delete":oid
    });
    postFormFunc(rodeo_appname,arg,function(resp) {
      document.getElementById("detail_list_<%=@fid%>").innerHTML = resp.responseText;
    });
  }
  function detail_inserter_<%=@fid%>() {
    var arg = sysargs({
      "action":"detail_target_entry",
      "field":"<%=@name%>"
    });
    document.getElementById("detail_target_<%=@fid%>").src = rodeo_appname+"?"+arg;
    document.getElementById("detail_target_<%=@fid%>").style.height = "100%";
  }
  function detail_editor_<%=@fid%>(oid) {
    var arg = sysargs({
      "action":"detail_target_edit",
      "field":"<%=@name%>",
      "edit":oid
    });
    document.getElementById("detail_target_<%=@fid%>").src = rodeo_appname+"?"+arg;
    document.getElementById("detail_target_<%=@fid%>").style.height = "100%";
  }
</script>
<%table = @table.rodeo.table_new(@conf['detail_table'])%>
<%if(@conf['entry_direction'])%>
  <%@table.rodeo.eruby(__FILE__,"detail_table_horizontal_view",binding)%>
<%else%>
  <%@table.rodeo.eruby(__FILE__,"detail_table_virtical_view",binding)%>
<%end%>

#### detail_table_virtical_view

<%@value.each {|v|%>
  <%if(table.select(v))%>
    <table class="record_spec" width="100%" style="margin-bottom:1px;">
      <%table.farray.each {|fld|%>
        <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true and fld['fobj'].pseudo != true)%>
          <%if(fld['name'] != "roid")%>
            <tr>
              <th class="record_spec" nowrap>
                <%=fld['name']%>
                <%if(fld['fobj'].conf['valid'])%>
                  <font color="red">※</font>
                <%end%>
              </th>
              <td class="record_spec" style="background-color:<%=@conf['bgcolor']%>;padding:0px 2px 0px 2px;" width="100%" colspan="3" nowrap>
                <%fld['fobj'].display(false)%>
              </td>
            </tr>
          <%end%>
        <%end%>
      <%}%>
    </table>
    <table class="record_spec" width="100%" style="margin-bottom:1px;">
      <tr>
        <th class="record_spec" width="100%"></th>
        <th class="record_spec" style="padding:1px;margin:0px;">
          <input type="button" width="0%" onclick="detail_editor_<%=@fid%>(<%=table['roid'].value%>)" value="編集">
        </th>
        <th class="record_spec" style="padding:1px;margin:0px;">
          <input type="button" width="0%" onclick="detail_delete_<%=@fid%>(<%=table['roid'].value%>)" value="削除">
        </th>
      </tr>
    </table>
  <%end%>
<%}%>
<table class="record_spec" width="100%" style="margin-bottom:1px;">
  <th class="record_spec" style="padding:1px;margin:0px; text-align:right;">
    <input type="button" onclick="detail_inserter_<%=@fid%>()" value="追加">
  </th>
</table>

#### detail_table_horizontal_view

<table class="record_spec" width="100%">
  <tr>
    <%table.farray.each {|fld|%>
      <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true and fld['fobj'].pseudo != true)%>
        <%if(table.insert_mode != true or fld['name'] != "roid")%>
          <th class="record_spec" style="text-align: left; background-color:<%=@conf['bgcolor']%>;" nowrap>
            <%=fld['name']%>
            <%if(fld['fobj'].conf['valid'])%>
              <font color="red">※</font>
            <%end%>
          </th>
        <%end%>
      <%end%>
    <%}%>
    <%if(@display_mode == true)%>
      <th class="record_spec" style="background-color:<%=@conf['bgcolor']%>;" width="100%"><br></th>
    <%else%>
      <th class="record_spec" colspan="2" style="background-color:<%=@conf['bgcolor']%>;" width="100%"><br></th>
      <th class="record_spec" style="padding:1px;margin:0px; background-color:<%=@conf['bgcolor']%>;">
        <input type="button" onclick="detail_inserter_<%=@fid%>()" value="新規(N)" accesskey="N">
      </th>
    <%end%>
  </tr>
  <%@value.each {|v|%>
    <%if(table.select(v))%>
      <tr ondblclick="detail_editor_<%=@fid%>(<%=table['roid'].value%>)">
        <%table.farray.each {|fld|%>
          <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true and fld['fobj'].pseudo != true)%>
            <%if(table.insert_mode != true or fld['name'] != "roid")%>
              <td class="record_spec" style="background-color:white;padding:0px 2px 0px 2px;<%=fld['fobj'].style%>" nowrap>
                <%fld['fobj'].display(true)%>
              </td>
            <%end%>
          <%end%>
        <%}%>
        <td class="record_spec" style="background-color:white;padding:0px" width="100%">
          <%if(@display_mode != true)%>
          <%else%>
            <br>
          <%end%>
        </td>
        <%if(@display_mode != true)%>
          <th class="record_spec" style="padding:1px;margin:0px">
            <input type="button" onclick="detail_editor_<%=@fid%>(<%=table['roid'].value%>)" value="編集(E)" accesskey="E">
          </th>
          <th class="record_spec" style="padding:1px;margin:0px">
            <input type="button" onclick="detail_delete_<%=@fid%>(<%=table['roid'].value%>)" value="削除(D)" accesskey="D">
          </th>
        <%end%>
      </tr>
    <%end%>
  <%}%>
</table>

#### detail_display

<div class="detail" id="detail_list_<%=@fid%>">
  <%if(@conf['detail_table'] != nil and @conf['detail_table'] != "")%>
    <%@table.rodeo.eruby(__FILE__,"detail_table",binding)%>
  <%end%>
</div>

#### detail_entry

<%@table.rodeo.html_hidden(cgi_name(),@value.join(","))%>
<%@table.rodeo.eruby(__FILE__,"detail_display",binding)%>
<iframe class="inserter" id="detail_target_<%=@fid%>" frameborder="0" style="margin:1px 0px 0px 0px"></iframe>
<script type="text/javascript">
  // 詳細表示のアップデート
  function detail_table_update_<%=@fid%>(oid) {
    var list = document.getElementById("<%=cgi_name()%>");
    if(list.value == "") { list.value = oid; } else { list.value = list.value + "," + oid; }
    var arg = sysargs({
      "action":"detail_table_update",
      "field":"<%=@name%>",
      "values":list.value
    });
    postFormFunc(rodeo_appname,arg,function(resp) {
      document.getElementById("detail_list_<%=@fid%>").innerHTML = resp.responseText;
    });
  }
  // 詳細表示のアップデート
  function detail_table_reload_<%=@fid%>() {
    var arg = sysargs({
      "action":"detail_table_reload",
      "field":"<%=@name%>",
    });
    postFormFunc(rodeo_appname,arg,function(resp) {
      document.getElementById("detail_list_<%=@fid%>").innerHTML = resp.responseText;
    });
  }
  //
  var arg = sysargs({
    "action":"detail_target_entry",
    "field":"<%=@name%>"
  });
//  document.getElementById("detail_target_<%=@fid%>").src = rodeo_appname+"?"+arg;
</script>

#### detail_target_entry

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      // テーブル名を付け替える
      rodeo_table = "<%=@conf['detail_table']%>";
      // iframeを表示する
      function on_load_func() {
        frame = parent.document.getElementById("detail_target_<%=@fid%>");
        h = document.body.offsetHeight;
        if(h == 0) {
          h = document.body.scrollHeight + 4;
        }
        frame.style.height = h;
        <%if(table.farray[1])%>
          document.getElementById("<%=table.farray[1]['fobj'].cgi_name()%>").focus();
        <%end%>
      }
      // 明細表示のアップデート
      <%if(@oid)%>
        if(binfo.ie) {
          parent.location.reload(true);
        } else {
          parent.detail_table_update_<%=@fid%>(<%=@oid%>);
        }
      <%end%>
    </script>
  </head>
  <body class="margin0" style="padding:0px" onload="on_load_func();">
    <%@table.rodeo.html_form(@table.rodeo.appname) {%>
      <%@table.rodeo.html_hidden("action","detail_target_insert")%>
      <table class="record_spec" width="100%" style="margin-bottom:1px;">
        <%table.farray.each {|fld|%>
          <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true and fld['fobj'].pseudo != true)%>
            <%if(table.insert_mode != true or fld['name'] != "roid")%>
              <tr>
                <th class="record_spec" style="text-align: left" nowrap>
                  <%=fld['name']%>
                  <%if(fld['fobj'].conf['accesskey'] != "")%>
                    <tt>(<u><%=fld['fobj'].conf['accesskey']%></u>)</tt>
                  <%end%>
                  <%if(fld['fobj'].conf['valid'])%>
                    <font color="red">※</font>
                  <%end%>
                </th>
                <td class="record_spec" width="100%" style="padding:1px;" nowrap><%fld['fobj'].entry()%></td>
              </tr>
            <%end%>
          <%end%>
        <%}%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" style="background-color:#666666;" width="100%">&nbsp;<font color="red"><%=@error%></font></th>
          <th class="record_spec" style="background-color:#666666;"><%@table.rodeo.html_button("CLOSE","閉じる(X)",{""=>"","accesskey"=>"X","onclick"=>"parent.document.getElementById('detail_target_#{@fid}').style.height = 0;"})%></th>
          <th class="record_spec" style="background-color:#666666;"><%@table.rodeo.html_submit("OK","追加(A)",{"accesskey"=>"A"})%></th>
        </tr>
      </table>
    <%}%>
  </body>
</html>

#### detail_target_edit

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      // テーブル名を付け替える
      rodeo_table = "<%=@conf['detail_table']%>";
      // iframeを表示する
      function on_load_func() {
        frame = parent.document.getElementById("detail_target_<%=@fid%>");
        h = document.body.offsetHeight;
        if(h == 0) {
          h = document.body.scrollHeight + 4;
        }
        frame.style.height = h;
        <%if(table.farray[1])%>
          document.getElementById("<%=table.farray[1]['fobj'].cgi_name()%>").focus();
        <%end%>
      }
    </script>
  </head>
  <body class="margin0" style="padding:0px" onload="on_load_func();">
    <%@table.rodeo.html_form(@table.rodeo.appname) {%>
      <%@table.rodeo.html_hidden("action","detail_target_update")%>
      <%@table.rodeo.html_hidden("edit",@oid)%>
      <table class="record_spec" style="margin-bottom:1px;">
        <%table.farray.each {|fld|%>
          <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true and fld['fobj'].pseudo != true)%>
            <%if(table.insert_mode != true or fld['name'] != "roid")%>
              <tr>
                <th class="record_spec" style="text-align: center" nowrap>
                  <%=fld['name']%>
                  <%if(fld['fobj'].conf['valid'])%>
                    <font color="red">※</font>
                  <%end%>
                </th>
                <td class="record_spec" width="100%" style="padding:1px;" nowrap><%fld['fobj'].entry()%></td>
              </tr>
            <%end%>
          <%end%>
        <%}%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" style="background-color:#666666;" width="100%">&nbsp;<font color="red"><%=@error%></font></th>
          <th class="record_spec" style="background-color:#666666;"><%@table.rodeo.html_button("CLSE","閉じる(X)",{"accesskey"=>"X","onclick"=>"parent.document.getElementById('detail_target_#{@fid}').style.height = 0;"})%></th>
          <th class="record_spec" style="background-color:#666666;"><%@table.rodeo.html_submit("OK","更新(U)",{"accesskey"=>"U"})%></th>
        </tr>
      </table>
    <%}%>
  </body>
</html>

#### detail_target_edit_done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@table.rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load() {
        parent.detail_table_reload_<%=@fid%>();
        parent.document.getElementById('detail_target_#{@fid}').style.height = 0;
      }
    </script>
  </head>
  <body class="margin0" onload="on_load();">
  </body>
</html>

