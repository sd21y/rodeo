# -*- coding: utf-8 -*-

#
# Roid型
#

class RFroid < RFBase
attr_accessor :owner
attr_accessor :group
attr_accessor :ctime
attr_accessor :uuser
attr_accessor :ugroup
attr_accessor :utime
attr_accessor :mark
  def self.name(); "Roid" end
  def self.type(); [
    RFSerial.type(),
    RFselect.type(),RFselect.type(),RFdatetime.type(),
    RFselect.type(),RFselect.type(),RFdatetime.type(),
    RFbool.type()]
  end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['table_alias']  = ""     unless(@conf['table_alias'])
    @conf['order_field']  = "roid" unless(@conf['order_field'])
    @owner = RFselect.new(name+"_1",table,{
      'select_type'=>"select",
      'master_table'=>"ユーザ",
      'master_field'=>"アカウント",
      'master_sort'=>"roid",
    })
    @group = RFselect.new(name+"_2",table,{
      'select_type'=>"check",
      'master_table'=>"グループ",
      'master_field'=>"グループ名",
      'master_sort'=>"roid",
    })
    @ctime = RFdatetime.new(name+"_3",table,{
      'year_select'=>false,
      'mon_select' =>false,
      'day_select' =>false,
      'hour_select'=>false,
      'min_select' =>false,
      'sec_select' =>false,
      'year_sep'   => "/",
      'month_sep'  => "/",
      'day_sep'    => " ",
      'hour_sep'   => ":",
      'min_sep'    => ":",
      'sec_sep'    => "",
    })
    @uuser = RFselect.new(name+"_4",table,{
      'select_type'=>"select",
      'master_table'=>"ユーザ",
      'master_field'=>"アカウント",
      'master_sort'=>"roid",
    })
    @ugroup = RFselect.new(name+"_5",table,{
      'select_type'=>"check",
      'master_table'=>"グループ",
      'master_field'=>"グループ名",
      'master_sort'=>"roid",
    })
    @utime = RFdatetime.new(name+"_6",table,{
      'year_select'=>false,
      'mon_select' =>false,
      'day_select' =>false,
      'hour_select'=>false,
      'min_select' =>false,
      'sec_select' =>false,
      'year_sep'   => "/",
      'month_sep'  => "/",
      'day_sep'    => " ",
      'hour_sep'   => ":",
      'min_sep'    => ":",
      'sec_sep'    => "",
    })
    @mark = RFbool.new(name+"_7",table,{})
    @conf['record_display'] = "ALL" unless(@conf['record_display'])
    @conf['access_account'] = []    unless(@conf['access_account'])
    @conf['record_insert']  = []    unless(@conf['record_insert'])
    @conf['record_update']  = []    unless(@conf['record_update'])
    @conf['record_delete']  = []    unless(@conf['record_delete'])
    @conf['field_insert']   = []    unless(@conf['field_insert'])
    @conf['field_delete']   = []    unless(@conf['field_delete'])
    @conf['field_config']   = []    unless(@conf['field_config'])
  end
  def init()
    @owner.init()
    @group.init()
    @uuser.init()
    @ugroup.init()
  end
  # ソート用フィールド名を返す
  def order()
    db_name()
  end
  # 設定
  def config_entry(su=true)
    super(false) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['table_alias'] = @table.rodeo["#{@fid}.table_alias"]
    @conf['order_field'] = @table.rodeo["#{@fid}.order_field"]
    [
      "access_account",
      "record_insert",
      "record_update",
      "record_delete",
      "field_insert",
      "field_delete",
      "field_config",
    ].each {|name|
      @conf[name] = []
      @table.rodeo.cgi.each {|k,v|
        if(k =~ /^#{name}\.\d+$/)
          @conf[name] << v.to_i
        end
      }
    }
=begin
    group = @table.rodeo.table_new("グループ")
    group.fetch(0,group.count(),%Q("管理者" != 'true')) {|roid|
      name = "record_view.#{group['グループ名'].value}"
      @conf['record_view'][group['グループ名'].value] = @table.rodeo[name]
    }
=end
  end
  # データベースからの取り込み
  def from_db(db)
    @value = db[db_name()].to_i
    @table.roid = @value
    @owner.from_db(db)
    @group.from_db(db)
    @ctime.from_db(db)
    @uuser.from_db(db)
    @ugroup.from_db(db)
    @utime.from_db(db)
    @mark.from_db(db)
  end
  # データベースの書き込みデータ
  def to_db()
    if(@value)
      [
        @value.to_s,
        @owner.to_db(),
        @group.to_db(),
        @ctime.to_db(),
        @uuser.to_db(),
        @ugroup.to_db(),
        @utime.to_db(),
        @mark.to_db()
      ]
    else
      [
        "DEFAULT",
        "'{"+@table.rodeo.user_record.roid.to_s+"}'",
        "'{"+@table.rodeo.user_record['グループ'].value.join(",")+"}'",
        "'"+DateTime.now.strftime("%Y-%m-%d %H:%M:%S")+"'",
        nil,
        nil,
        nil,
        false
      ]
    end
  end
  # CGIからの取り込み
  def from_cgi()
    @owner.from_cgi()
    @group.from_cgi()
    @ctime.from_cgi()
    @uuser.from_cgi()
    @ugroup.from_cgi()
    @utime.from_cgi()
    @mark.from_cgi()
  end
  # 入力
  def entry(opt={})
    super() {
      if(@table.insert_mode != true)
        @owner.hidden()
        @group.hidden()
        @ctime.hidden()
        @uuser.hidden()
        @ugroup.hidden()
        @utime.hidden()
        @mark.hidden()
        Rodeo.print "#{@value},"
        Rodeo.print "作成 ["
        @owner.display(true)
        @ctime.display(true)
        Rodeo.print "] 更新 ["
        @uuser.display(true)
        @utime.display(true)
        Rodeo.puts "]"
      end
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(list)
        Rodeo.print "#{@value}"
      else
        Rodeo.print "#{@value},"
        Rodeo.print "作成 ["
        @owner.display(false)
        @ctime.display(false)
        Rodeo.print "] 更新 ["
        @uuser.display(false)
        @utime.display(false)
        Rodeo.puts "]"
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("テーブル名エイリアス","テーブル名の表示を変更します。DB上のテーブル名は変更しません。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.table_alias",@conf['table_alias'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("初期表示順","テーブルを開いたときどのフィールド順に表示するかを設定します。")%></th>
  <td class="field_conf">
    <%
      value = @conf['order_field']
      @table.rodeo.html_select("#{@fid}.order_field",value) {
        @table.farray.each {|h|
          if(h['name'] == "roid" or eval("#{h['type']}.type().class") != Array)
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<%group = @table.rodeo.table_new("グループ")%>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("アクセス権","このテーブルのアクセス権を設定します。許可したいチェックボックスをマークしてください。")%>
  </th>
  <td class="field_conf">
    <table class="border0" width="100%">
      <tr>
        <th class="field_conf_conf" rowspan="2"></th>
        <th class="field_conf_conf">テーブル</th>
        <th class="field_conf_conf" colspan="3">レコード</th>
        <th class="field_conf_conf" colspan="3">フィールド</th>
        <!-- th class="field_conf_conf" rowspan="2">レコード一覧</th -->
      </tr>
      <tr>
        <!-- <th class="field_conf_conf"></th> -->
        <th class="field_conf_conf">一覧</th>
        <th class="field_conf_conf">追加</th>
        <th class="field_conf_conf">更新</th>
        <th class="field_conf_conf">削除</th>
        <th class="field_conf_conf">追加</th>
        <th class="field_conf_conf">削除</th>
        <th class="field_conf_conf">設定</th>
        <!-- <th class="field_conf_conf">レコード一覧</th> -->
      </tr>
      <%group.fetch(0,group.count(),%Q("管理者"!='true')) {|roid|%>
        <tr>
          <th class="field_conf_conf"><%=group['グループ名'].value%></th>
          <%account = [
              "access_account",
              "record_insert",
              "record_update",
              "record_delete",
              "field_insert",
              "field_delete",
              "field_config",
          ]%>
          <%account.each_index {|i|%>
            <%checked = ((@conf[account[i]].include?(roid)) ? "checked" : "")%>
            <%name = "#{account[i]}.#{roid}"%>
            <td class="field_conf_conf">
              <input type="checkbox" id="<%=name%>" name="<%=name%>" value="<%=roid%>" <%=checked%>>許可
            </td>
          <%}%>
          <!-- td class="field_conf_conf" style="text-align:left" -->
            <%#name = "record_view.#{group['グループ名'].value}"%>
            <%#@table.rodeo.html_radio(name,@conf['record_view'][group['グループ名'].value]) {
              #@table.rodeo.html_item("ALL","全て")
              #@table.rodeo.html_item("GROUP","同一グループ")
              #@table.rodeo.html_item("OWNER","オーナーのみ")
            #}%>
          <!-- /td -->
        </tr>
      <%}%>
    </table>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("テーブル構造",nil)%>
  </th>
  <td class="field_conf" width="100%">
    <table class="border0" width="100%">
      <tr>
        <th class="field_conf" nowrap>フィールド名</th>
        <th class="field_conf" nowrap>フィールド型</th>
        <th class="field_conf" nowrap>オブジェクト型</th>
        <th class="field_conf" nowrap>項目の説明</th>
      </tr>
      <%@table.farray.each {|f|%>
        <tr>
          <th class="field_conf" nowrap><%=f['name']%></th>
          <td class="field_conf" nowrap><%=instance_eval("#{f['type']}.name()".untaint)%></td>
          <td class="field_conf" nowrap><%=f['type']%></td>
          <td class="field_conf" nowrap><%=f['fobj'].conf['description']%></td>
        </tr>
      <%}%>
    </table>
  </td>
</tr>

