# -*- coding: utf-8 -*-

#
# 計算型
#

class RFcalc < RFBase
  def self.name(); "計算" end
  def self.type(); nil end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @pseudo = true
    @conf['expr'] = ""  unless(@conf['expr'])
    @conf['type'] = nil unless(@conf['type'])
    @conf['conf'] = {}  unless(@conf['conf'])
    @value = nil
    if(@conf['type'])
      @obj = instance_eval("#{@conf['type']}.new(name,table,@conf['conf'])".untaint)
    else
      @obj = nil
    end
  end
  # 計算式の評価
  def calc()
    @value = nil
    @error = nil
    if(@conf['expr'] != nil and @obj != nil)
      begin
        @value = instance_eval(@conf['expr'].unpack("m")[0].untaint)
        @obj.value = @value
      rescue Exception
        @error = "#{$!}"
      end
    end
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      @obj.config_entry(false) if(@obj)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['expr'] = [@table.rodeo["#{@fid}.expr"]].pack("m")
    @conf['type'] = @table.rodeo["#{@fid}.type"]
    @obj.eval_config() if(@obj)
  end
  # 表示
  def display(list)
    super() {
      if(@error)
        Rodeo.print %Q(<font color="red">#{@error}</font>)
      else
        @obj.display(list) if(@obj)
      end
    }
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      if(@error)
        Rodeo.print %Q(<font color="red">#{@error}</font>)
      else
        @obj.display(false) if(@obj)
      end
    }
  end
  def to_s()
    @conf['prefix']+@obj.value.to_s+@conf['postfix']
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("オブジェクト","どのオブジェクトとして計算値を扱うかを指定します。")%>
  </th>
  <td class="field_conf">
    <%disable = ["RFroid","RFid","RFsession","RFpasswd"]%>
    <%@table.rodeo.html_select("#{@fid}.type",@conf['type']) {%>
      <%i = 0%>
      <%@table.rodeo.flist.each {|key,hash|%>
        <%@table.rodeo.html_optgroup("#{@table.rodeo.flabel[key]}",{"class"=>"flist#{i}"}) {%>
          <%hash.sort{|a,b|a[1]<=>b[1]}.each {|k,v|%>
            <%if(not disable.include?(k))%>
              <%@table.rodeo.html_item("#{k}","#{v}")%>
            <%end%>
          <%}%>
        <%}%>
        <%i += 1%>
      <%}%>
    <%}%>
  </td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("計算式",
      "計算式を設定します。Rubyのプログラムをそのまま書いてください。")%>
  </th>
  <td class="field_conf"><%@table.rodeo.html_textarea("#{@fid}.expr",@conf['expr'].unpack("m")[0],60,10,{"style"=>"width:100%"})%></td>
</tr>

