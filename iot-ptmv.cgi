#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require "#{File.dirname(__FILE__)}/lib/rodeo.rb"

dbname = File.basename(File.dirname(File.expand_path(__FILE__)))
sql = Rodeo::SQL.new(dbname,nil)
rodeo = Rodeo.new(__FILE__,nil,sql)
rodeo.get_cgi_data(nil,nil)
iot = rodeo.table_new("IoT-ptmv")
iot.insert_mode = true
iot.clear
begin
  iot["日時"].value = Time.strptime(rodeo['d'],"%Y/%m/%dT%H:%M:%S")
  iot["気圧"].value = rodeo["p"].to_f
  iot["気温"].value = rodeo["t"].to_f
  iot["湿度"].value = rodeo["m"].to_f
  iot["電圧"].value = rodeo["v"].to_f
  roid = iot.insert()
  if(roid)
    rodeo.eruby(__FILE__,"OK",binding)
  else
    rodeo.eruby(__FILE__,"NG",binding)
  end
rescue
  rodeo.eruby(__FILE__,"ER",binding)
end
rodeo.output()

__END__

#### OK

<html><body>OK</body></html>

#### NG

<html><body>NG</body></html>

#### ER

<html><body>ER</body></html>
