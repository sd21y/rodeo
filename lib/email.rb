# -*- coding: utf-8 -*-

require "net/smtp.rb"
require "base64"

################################################################################
#
#	Email
#
class Email

	attr_reader		:header
	attr_reader		:body
	attr_accessor	:smtpServer

	#---------------------------------------------------------------------------
	#
	#	Initialize メールオブジェクトを生成する
	#
	def initialize(mail = nil)	# mail = Array or IO(ex.STDIN) or nil
		key     = nil
		@header = {}
		@body   = []

		return if mail == nil

		inBody = false
		mail.each {|line|
			line.chomp!
			unless inBody
				if line =~ /^$/					# 空行
					inBody = true
				elsif line =~ /^(\S+?):\s*(.*)/	# ヘッダ行
					key = $1.capitalize
					@header[key] = $2
				elsif key						# ヘッダ行が2行に渡る場合
					@header[key] += "\n" + line.sub(/^\s*/, "\t")
				end
			else
				@body.push(line)
			end
		}
	end

	#---------------------------------------------------------------------------
	#
	#	[] ヘッダを参照
	#
	def [](key)
		@header[key.capitalize]
	end

	#---------------------------------------------------------------------------
	#
	#	[]= ヘッダを設定
	#
	def []=(key, value)
		@header[key.capitalize] = value
	end

	#---------------------------------------------------------------------------
	#
	#	<< ボディにテキストを追加
	#
	def <<(message)
		@body.push message
	end

	#---------------------------------------------------------------------------
	#
	#	encode メールをテキストストリームにエンコード
	#
	def encode
		mail = ""
		@header.each {|key, value|
			if(value.class == Array)
				mail += "#{key}: #{value.join(",")}\n"
			else
				mail += "#{key}: #{value}\n"
			end
		}
		mail += "\n"							# ヘッダ/ボディのセパレータ
		@body.each {|message|
			mail += "#{message}\n"
		}
		return mail
	end

	#---------------------------------------------------------------------------
	#
	#	send メールを送る
	#
	def send(from = nil, *to)
		from  = @header['From'] unless from
		to.push @header['To']   if to.size == 0
		Net::SMTP.start(smtpServer ? smtpServer : "localhost") {|smtp|
			smtp.send_mail(self.encode, from, *to)
		}
	end
end

################################################################################
#
#	String
#
class String

	#---------------------------------------------------------------------------
	#
	#	mime encode
	#
	def mime_encode
		return "=?ISO-2022-JP?B?" + Base64.encode64(self.tojis).gsub!(/\n/, "") + "?="
	end

	#---------------------------------------------------------------------------
	#
	#	mime decode
	#
	def mime_decode
		return self unless self =~ /^=\?ISO-2022-JP\?B\?(.+)\?=$/i
		return Base64.decode64($1)
	end
end

################################################################################
#
#	EncodedEmail
#
class EncodedEmail < Email

	#---------------------------------------------------------------------------
	#
	#	Decode メールブロックをデコード
	#
	def decode
		if self['Content-transfer-encoding'] =~ /base64/i
			return Base64.decode64(@body.join)
		else
			return @body.join("\n")
		end
	end

	#---------------------------------------------------------------------------
	#
	#	<< ボディにコンテントを追加
	#
	def <<(content)
		if self['Content-transfer-encoding'] =~ /base64/i
			@body = Base64.encode64(content).split("\n")
		else
			@body = content.split("\n")
		end
	end
end

################################################################################
#
#	AttachedEmail
#
class AttachedEmail < EncodedEmail

	attr_reader	:block

	#---------------------------------------------------------------------------
	#
	#	Initialize 添付メールオブジェクトを生成する
	#
	def initialize(mail = nil)

		super(mail)

		if mail == nil
			@separator = "separator" + (rand 65536).to_s
			self['MIME-Version'] = "1.0"
			self['Content-Type'] = "Multipart/Mixed; boundary=\"#{@separator}\""
			@block = []
			return
		end

		return unless self['Content-Type'] =~ /^Multipart\/Mixed;\s*boundary=(["']?)(.*)\1/i
		@separator = $2
		@block = []

		buf = []
		@body.each {|line|
			if line =~ /^--#{@separator}/
				@block.push(EncodedEmail.new(buf))
				buf = []
				next
			end
			buf.push(line)
		}

		@block.shift
		@body = []
	end

	#---------------------------------------------------------------------------
	#
	#	<< メールブロックを追加
	#
	def <<(block)
		@block.push(block)
	end

	#---------------------------------------------------------------------------
	#
	#	encode メールをテキストストリームにエンコード
	#
	def encode
		@block.each {|block|
			@body.push("--" + @separator)
			@body += block.encode.split("\n")
		}
		@body.push("--" + @separator + "--")
		super
	end
end
