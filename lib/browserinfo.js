/////////////////////////////////////////////////////
//
//  BrowserInfo クラス ver 0.01
//
//	http://www.horori.net 
//
/////////////////////////////////////////////////////

function BrowserInfo(userAgent) {

	/*****************************************
	 * Geckoエンジンバージョンチェッカー
	 * (返値 int 合致しない場合は-1)
	 *****************************************/
	this.geckoVersionChecker = function(){
		var num = this.ua.match(new RegExp("Gecko/20[0-9]{6}"));
		return ( num == null ) ? -1 : parseInt(String(num).replace("Gecko/",""));
	}

	/*****************************************
	 * WebKitバージョンチェッカー
	 * (返値 float 合致しない場合は-1)
	 *****************************************/
	this.webkitVersionChecker = function(){
		var num = this.ua.match(new RegExp("WebKit/[0-9]{1,4}(\.[0-9]{1,2})?"));
		return ( num == null ) ? -1 : parseFloat(String(num).replace("Firefox/",""));
	}

	/*****************************************
	 * IEバージョンチェッカー
	 * (返値 float 合致しない場合は-1)
	 *****************************************/
	this.ieVersionChecker = function(){
		var ienum = this.ua.match(new RegExp("MSIE [0-9]{1,2}\.[0-9]{1,3}"));
		return ( ienum == null ) ? -1 : parseFloat(String(ienum).replace("MSIE ",""));
	}
	

	/*****************************************
	 * Firefoxバージョンチェッカー
	 * (返値 float 合致しない場合は-1)
	 *****************************************/
	this.firefoxVersionChecker = function(){
		var num = this.ua.match(new RegExp("Firefox/[0-9]{1,2}\.[0-9]{1,2}"));
		return ( num == null ) ? -1 : parseFloat(String(num).replace("Firefox/",""));
	}

	/*****************************************
	 * Operaバージョンチェッカー
	 * (返値 float 合致しない場合は-1)
	 *****************************************/
	this.operaVersionChecker = function(){
		var num = this.ua.match(new RegExp("Opera[/ ][0-9]{1,2}\.[0-9]{1,2}"));
		return ( num == null ) ? -1 : parseFloat(String(num).substr(6));
	}

	/*****************************************
	 * Safariバージョンチェッカー
	 * (返値 float 合致しない場合は-1)
	 *****************************************/
	this.safariVersionChecker = function(){
		var num = this.ua.match(new RegExp("Safari/[0-9]{1,4}\.[0-9]{1,2}"));
		return ( num == null ) ? -1 : parseFloat(String(num).replace("Safari/",""));
	}

	/*****************************************
	 * Netscapeバージョンチェッカー
	 * (返値 float 合致しない場合は-1)
	 *****************************************/
	this.netscapeVersionChecker = function(){
		var num = this.ua.match(new RegExp("Netscape[0-9]?/[0-9]{1,2}\.[0-9]{1,3}"));
		return ( num == null ) ? -1 : parseFloat(String(num).replace(new RegExp("Netscape[0-9]?/"),""));
	}

	/*****************************************
	 * Mozillaバージョンチェッカー
	 * (返値 float 合致しない場合は-1)
	 *****************************************/
	this.mozillaVersionChecker = function(){
		var num = this.ua.match(new RegExp("Mozilla/[0-9]{1,2}\.[0-9]{1,2}"));
		return ( num == null ) ? -1 : parseFloat(String(num).replace("Mozilla/",""));
	}
	// -- コンストラクタ --

	this.ua = (userAgent) ? userAgent : navigator.userAgent;

	this.geckoVersion = this.geckoVersionChecker();
	this.gecko = (this.geckoVersion > 0 );

	this.webkitVersion = this.webkitVersionChecker();
	this.webkit = (this.webkitVersion > 0 );

	this.ieVersion = this.ieVersionChecker();
	this.ieMVersion = Math.floor(this.ieVersion);
	this.ie = ( this.ieVersion >= 3 );
	this.macie = ( this.ua.match("Mac_PowerPC") != null );

	this.firefoxVersion = this.firefoxVersionChecker();
	this.firefoxMVersion = Math.floor(this.firefoxVersion);
	this.firefox = (this.firefoxVersion > 0 );

	this.safariVersion = this.safariVersionChecker();
	this.safariMVersion = Math.floor(this.safariVersion);
	this.safari = (this.safariVersion > 85 );

	this.operaVersion = this.operaVersionChecker();
	this.operaMVersion = Math.floor(this.operaVersion);
	this.opera = (this.operaVersion > 1 );
	
	this.netscapeVersion = this.netscapeVersionChecker();
	this.netscapeMVersion = Math.floor(this.netscapeVersion);
	this.netscape = (this.natscapeVersion > 1 );
	
	this.mozillaVersion = this.mozillaVersionChecker();
	this.mozilla = ( !this.firefox && !this.opera && !this.ie && !this.netscape && this.mozillaVersion > 0 ); 
	
	this.toString = function(){
		return ("[ua:"+this.ua+"  netscapeVersion:"+this.netscapeVersion+"  operaVersion:" + this.operaVersion + "  webkitVersion:"+ this.webkitVersion +"  safariVersion:"+this.safariVersion+"  ieVersion:"+ this.ieVersion +"  macie:" + this.macie  + "  geckoVersion:" +this.geckoVersion + " firefoxVersion:" +this.firefoxVersion +"]" );
	}
}