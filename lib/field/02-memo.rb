# -*- coding: utf-8 -*-

#
# メモ型
#

class RFmemo < RFString
  def self.name(); "メモ" end
  def self.type(); RFString.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['rows'] = 3 unless(@conf['rows'])
    @conf['richedit'] = false if(@conf['richedit'] == nil)
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['rows'] = @table.rodeo["#{@fid}.rows"]
    @conf['richedit'] = (@table.rodeo["#{@fid}.richedit"] == 'true') ? true : false
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    case @table.rodeo["#{@fid}.operator"]
    when "~"  then where = %Q("#{@name}" ~  '#{@table.rodeo["#{@fid}.data"].gsub(/\'/,"''")}')
    when "!~" then where = %Q("#{@name}" !~ '#{@table.rodeo["#{@fid}.data"].gsub(/\'/,"''")}')
    when "^"  then where = %Q("#{@name}" ~  '^#{@table.rodeo["#{@fid}.data"].gsub(/\'/,"''")}')
    when "$"  then where = %Q("#{@name}" ~  '#{@table.rodeo["#{@fid}.data"].gsub(/\'/,"''")}$')
    end
    where
  end
  # 絞りこみの設定
  def eval_where_marked(roid)
    where = nil
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" = #{to_db()})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      if(@table.rodeo.html_valid?(cgi_name()))
        @value = @table.rodeo[cgi_name()]
      end
      @value = nil if(@value == "")
    }
  end
  def from_db(db)
    @value = db[db_name()]
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['style'] = "width:100%"
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      @table.rodeo.eruby(__FILE__,"entry",binding)
#      @table.rodeo.html_textarea(cgi_name(),@value,60,@conf['rows'],{"style"=>"width:100%"})
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        if(list)
          if(@conf['richedit'])
            Rodeo.puts "&lt;HTML&gt;"
          else
            @value.each_line {|line|
              line.chomp!
              line.gsub!(/\r/,"") if(line)
              # 先頭10文字のみ表示
              Rodeo.print CGI::escapeHTML(line.split(//u)[0..19].join(""))
              Rodeo.puts "..."
              break
            }
          end
        else
          if(@conf['richedit'])
            Rodeo.print @value
          else
            @table.rodeo.html_textarea(cgi_name(),@value,60,@conf['rows'],{"style"=>"width:100%","readonly"=>true})
          end
        end
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("行数")%></th>
  <td class="field_conf"><%@table.rodeo.html_number("#{@fid}.rows",@conf['rows'],{"size"=>10})%></td>
</tr>
<tr>
  <th class="field_conf">
    <%@table.rodeo.html_tooltip("リッチテキストエディタ",
      "リッチテキスト編集を行なうかどうかを設定します。")%>
  </th>
  <td class="field_conf">
    <%
      @table.rodeo.html_radio("#{@fid}.richedit",@conf['richedit']) {
        @table.rodeo.html_item(true,"使う")
        @table.rodeo.html_item(false,"使わない")
      }
    %>
  </td>
</tr>

#### where_entry

<td class="field_conf">
  <%@table.rodeo.html_text("#{@fid}.data","",{"size"=>@conf['cols']})%>
  <%
    @table.rodeo.html_radio("#{@fid}.operator","~") {
      @table.rodeo.html_item("~"  ,"を含む")
      @table.rodeo.html_item("!~" ,"を含まない")
      @table.rodeo.html_item("^"  ,"で始まる")
      @table.rodeo.html_item("$"  ,"で終わる")
    }
  %>
</td>

#### entry

<%if(@conf['richedit'])%>
  <script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "<%=cgi_name()%>",
		theme : "advanced",
		language : "ja",
		// skin : "o2k7",
		plugins : "safari,pagebreak,style,layer,insertdatetime,searchreplace,paste,directionality,noneditable,nonbreaking,template,inlinepopups",
		// Theme options
		theme_advanced_buttons1 : "cut,copy,paste,pastetext,pasteword,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,|,insertdate,inserttime,|,forecolor,backcolor,|,hr,removeformat,visualaid,|,sub,sup,|,help,code",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		// theme_advanced_statusbar_location : "bottom",
		// theme_advanced_resizing : true,
		// Example content CSS (should be your site CSS)
		// content_css : "css/content.css",
		// Drop lists for link/image/media/template dialogs
		// template_external_list_url : "lists/template_list.js",
		// external_link_list_url : "lists/link_list.js",
		// external_image_list_url : "lists/image_list.js",
		// media_external_list_url : "lists/media_list.js",
		// Replace values for the template plugin
	  // template_replace_values : {
		// 	username : "Some User",
		// 	staffid : "991234"
		// }
	});
  </script>
<%end%>
<%@table.rodeo.html_textarea(cgi_name(),@value,60,@conf['rows'],opt)%>

