# -*- coding: utf-8 -*-

require 'csv'
require 'psych'
require 'yaml'

$pg = false
begin
  require 'pg'
  $pg = true
rescue Exception
  require 'postgres'
end

#
# Rodeo::SQLクラス
#

class Rodeo
  # SQLオブジェクト
  class SQL
    attr :sql
    # 初期化
    def initialize(rdb,host=nil,port=5432,opt=nil,tty=nil,user=nil,passwd=nil)
      @sql = PGconn.new(host,port,opt,tty,rdb,user,passwd)
      at_exit {
        if(@sql)
          @sql.close()
          @sql = nil
        end
      }
    end
    # DB名を返す
    def db()
      @sql.db
    end
    # SQL文の呼び出し
    def exec(sql)
      begin
        if($pg)
          res = @sql.exec(sql)
        else
          res = []
          def res.num_tuples
            self.size
          end
          r = @sql.exec(sql)
          r.each_with_index {|d,i|
            res[i] = {}
            d.each_index {|j|
              res[i][r.fieldname(j)] = d[j]
            }
          }
          r.clear()
        end
      rescue Exception
        print "Content-Type: text/plain\n\n"
        puts "SQL["+sql+"]\n"
        puts $!
        exit()
      end
      yield(res) if(block_given?)
      if($pg)
        res.clear()
      end
    end
  end
end

#
# Rodeoクラス
#

class Rodeo
  # テーブルオブジェクトの生成
  def table_new(name)
    if(name)
      if(name.class == String)
        Rodeo::Table.new(name,self)
      else
        table = nil
        self.sql.exec(%Q(select "name" from "rodeo" where "tid" = #{name})) {|res|
          if(res.num_tuples > 0)
            table = res[0]['name']
          end
        }
        Rodeo::Table.new(table,self)
      end
    else
      nil
    end
  end
  # Tableオブジェクト
  class Table
    attr :name
    attr :rodeo
    attr :exist
    attr :fields
    attr :farray
    attr :tid
    attr_accessor :error
    attr_accessor :roid
    attr_accessor :insert_mode
    def initialize(name,rodeo)
      @name = name
      @rodeo = rodeo
      @exist = false
      @fields = {}
      @farray = nil
      @roid = nil
      @tid = nil
      @error = nil
      @insert_mode = nil
      @cursor = "0000"
      @rodeo.sql.exec(%Q(select "tablename" from "pg_tables" where "tablename"='rc_#{@name}')) {|res|
        if(res.num_tuples == 1)
          @rodeo.sql.exec(%Q(select "tablename" from "pg_tables" where "tablename"='rd_#{@name}')) {|res|
            if(res.num_tuples == 1)
              @exist = true
            else
              @rodeo.sql.exec(%Q(select "viewname" from "pg_views" where viewname='rd_#{@name}')) {|res|
                if(res.num_tuples == 1)
                  @exist = true
                end
              }
            end
          }
        end
      }
      if(@exist)
        @tid = find_tid(@name)
        read_field()
      end
    end
    # フィールドのツリーからfieldとvalueの配列を作る
    def _make_field_and_type(name,ttree,narray,tarray)
      ttree.each_index {|i|
        if(ttree[i].class == Array)
          _make_field_and_type(name+"_#{i}",ttree[i],narray,tarray)
        else
          if(ttree[i])
            narray << name+"_#{i}"
            tarray << ttree[i]
          end
        end
      }
    end
    def _make_field_and_value(name,ttree,vtree,narray,varray)
      ttree.each_index {|i|
        if(ttree[i].class == Array)
          if(vtree)
            _make_field_and_value(name+"_#{i}",ttree[i],vtree[i],narray,varray)
          else
            _make_field_and_value(name+"_#{i}",ttree[i],nil,narray,varray)
          end
        else
          if(ttree[i])
            narray << name+"_#{i}"
            if(varray)
              if(vtree[i])
                varray << vtree[i]
              else
                varray << "NULL"
              end
            end
          end
        end
      }
    end
    def _make_value_pear(name,ttree,vtree,vpear)
      ttree.each_index {|i|
        if(ttree[i].class == Array)
          _make_value_pear(name+"_#{i}",ttree[i],vtree[i],vpear)
        else
          if(ttree[i])
            if(vtree[i])
              if(name == "roid" and i == 0)
                vpear << "\"#{name}\"=#{vtree[i]}"
              else
                vpear << "\"#{name}_#{i}\"=#{vtree[i]}"
              end
            else
              if(name == "roid" and i == 0)
                vpear << "\"#{name}\"=NULL"
              else
                vpear << "\"#{name}_#{i}\"=NULL"
              end
            end
          end
        end
      }
    end
    # フィールドオブジェクトの参照
    def [](name)
      raise "Rodeo field \"#{name}\" not found." unless(@fields[name])
      obj = @fields[name]['fobj']
      obj
    end
    # 固有値(tid)を探す
    def find_tid(name)
      tid = 0
      @rodeo.sql.exec(%Q(select "tid" from "rodeo" where "name" = '#{name}')) {|res|
        if(res.num_tuples == 0)
          @rodeo.sql.exec(%Q(insert into "rodeo" ("name") values ('#{name}')))
          @rodeo.sql.exec(%Q(select "tid" from "rodeo" where "name" = '#{name}')) {|res2|
            tid = res2[0]['tid'].to_i
          }
        else
          tid = res[0]['tid'].to_i
        end
      }
      tid
    end
    # 固有値(tid)からテーブル名を探す
    def refer_tid(tid)
      name = nil
      @rodeo.sql.exec(%Q(select "name" from "rodeo" where "tid" = #{tid})) {|res|
        if(res.num_tuples > 0)
          name = res[0]['name']
        end
      }
      name
    end
    # テーブル作成
    def create()
      @rodeo.sql.exec(%Q(create table "rc_#{@name}" ("name" text,"type" text,"delf" bool,"sort" int,"conf" text)))
      @rodeo.sql.exec(%Q(insert into  "rc_#{@name}" ("name","type","delf","sort","conf") values ('roid','RFroid','t',0,NULL)))
      @rodeo.sql.exec(%Q(create table "rd_#{@name}" ("roid" serial,"roid_1" int[],"roid_2" int[],"roid_3" timestamp,"roid_4" int[],"roid_5" int[],"roid_6" timestamp,"roid_7" bool)))
      @rodeo.sql.exec(
      %Q(create index "index_rd_#{@name}" on "rd_#{@name}" ("roid")))
      @tid = find_tid(@name)
      read_field()
    end
    # テーブル削除
    def delete_table()
      @rodeo.sql.exec(%Q(drop table "rc_#{@name}"))
      @rodeo.sql.exec(%Q(drop table "rd_#{@name}"))
    end
    # フィールド情報の読み込み
    def read_field()
      @fields = {}
      @farray = nil
      @rodeo.sql.exec(%Q(select "name","type","delf","sort","conf" from "rc_#{@name}")) {|res|
        array = []
        res.each {|ent|
          conf = {}
          begin
            if(ent['conf'])
              conf = Marshal.load(ent['conf'].unpack('m')[0])
            end
          rescue Exception
            begin
              conf = YAML::load(ent['conf']) if(ent['conf'])
              # 強制的にencodingをUTF-8(__ENCODING__)に変更
              conf.each {|k,v|
              	if(v.kind_of?(String))
              		v.force_encoding(__ENCODING__)
              	end
              }
            rescue Exception
              conf = {}
            end
          end
          begin
            obj = instance_eval("#{ent['type']}.new(ent['name'],self,conf)".untaint)
          rescue Exception
            obj = RFerror.new(ent['name'],self,nil)
          end
          hash = {
            'name'=>ent['name'],
            'type'=>ent['type'],
            'delf'=>((ent['delf']=="t") ? true : false),
            'sort'=>ent['sort'].to_i,
            'fobj'=>obj
          }
          array << hash
          @fields[ent['name']] = hash
#          config_field(ent['name'])
        }
        # fid,プラグインの設定
        array.each {|hash|
          if(hash['fobj'].plugin == true)
            main = @fields[hash['fobj'].conf['plugin_field']]
            if(main)
              hash['fobj'].main = main['fobj']
            end
          end
          hash['fobj'].fid = hash['sort'].to_s
        }
        # 表示順にソート
        @farray = array.sort() {|a,b| a['sort'] <=> b['sort'] }
      }
    end
    # フィールドの追加
    def add_field(name,type,delf)
      if(not @fields[name])
        count = 1
        @rodeo.sql.exec(%Q(select count(*) from "rc_#{@name}")) {|res|
          count = res[0]['count'].to_i
        }
        @rodeo.sql.exec(%Q(insert into "rc_#{@name}" ("name","type","delf","sort","conf") \
                           values ('#{name}','#{type}','#{((delf)?"t":"f")}',#{count},NULL)))
        qtype = instance_eval("#{type}.type()".untaint)
        if(qtype)
          if(qtype.class == Array)
            farray = []
            tarray = []
            _make_field_and_type(name,qtype,farray,tarray)
            farray.each_index {|i|
              @rodeo.sql.exec(%Q(alter table "rd_#{@name}" add column "#{farray[i]}" #{tarray[i]}))
            }
          else
            @rodeo.sql.exec(%Q(alter table "rd_#{@name}" add column "#{name}" #{qtype}))
          end
        end
        hash = {
          'name'=>name,
          'type'=>type,
          'delf'=>delf,
          'sort'=>count,
          'fobj'=>instance_eval("#{type}.new(name,self,{})".untaint)
        }
        @farray << hash
        @fields[hash['name']] = hash
        # fid,プラグインの設定
        if(hash['fobj'].plugin == true)
          main = @fields[hash['fobj'].conf['plugin_field']]
          if(main)
            hash['fobj'].main = main['fobj']
          end
        end
        hash['fobj'].fid = hash['sort'].to_s
      else
        raise "Field '#{name}' is already exist."
        exit
      end
    end
    # フィールドの削除
    def delete_field(name)
      if(@fields[name])
        # フィールド、レコードを削除
        begin
          qtype = instance_eval("#{@fields[name]['type']}.type()".untaint)
        rescue Exception
          qtype = true
        end
        if(qtype)
          if(qtype.class == Array)
            farray = []
            tarray = []
            _make_field_and_type(name,qtype,farray,tarray)
            farray.each_index {|i|
              @rodeo.sql.exec(%Q(alter table "rd_#{@name}" drop column "#{farray[i]}"))
            }
          else
            begin
              @rodeo.sql.exec(%Q(alter table "rd_#{@name}" drop column "#{name}"))
            rescue Exception
            end
          end
        end
        @rodeo.sql.exec(%Q(delete from "rc_#{@name}" where "name" = '#{name}'))
        # 表示順を詰める
        @rodeo.sql.exec(%Q(update "rc_#{@name}" set sort = sort - 1 where sort > #{@fields[name]['sort']}))
        @fields[name] = nil
        @farray.each_index {|i|
          if(@farray[i]['name'] == name)
            @farray[i] = nil
          end
        }
        @farray.compact!
      else
        raise "Field '#{name}' is not exist."
        exit
      end
    end
    # フィールド順の変更
    def reorder_field(name,to)
      from = @fields[name]['sort']
      if(to != from)
        if(from > to)
          @rodeo.sql.exec(%Q(update "rc_#{@name}" set sort = sort + 1 where sort != 0 and sort >= #{to} and sort < #{from}))
          @rodeo.sql.exec(%Q(update "rc_#{@name}" set sort = #{to} where name = '#{name}'))
        else
          @rodeo.sql.exec(%Q(update "rc_#{@name}" set sort = sort - 1 where sort != 0 and sort <= #{to} and sort > #{from}))
          @rodeo.sql.exec(%Q(update "rc_#{@name}" set sort = #{to} where name = '#{name}'))
        end
        read_field()
      end
    end
    # 汎用チェックのON/OFF
    def check_onoff(name)
      @rodeo.sql.exec(%Q(update "rc_#{@name}" set delf = not delf where name = '#{name}'))
      read_field()
    end
    # フィールド設定の保存
    def config_field(name)
      conf = @fields[name]['fobj'].conf.to_yaml().gsub(/\\/,"\\\\").gsub(/\'/,"''").gsub(/\"/,"\\\"")
#      conf = [Marshal.dump(@fields[name]['fobj'].conf)].pack('m')
      @rodeo.sql.exec(%Q(update "rc_#{@name}" set conf = E'#{conf}' where name = '#{name}'))
    end
    # レコード数の取得
    def realcount()
      count = 0
      @rodeo.sql.exec(%Q(select count(*) from "rd_#{@name}")) {|res|
        count = res[0]['count'].to_i
      }
      count
    end
    # レコード数の取得
    def count(where = nil)
      if(where) then where = " where #{where}"; else where = ""; end
      count = 0
      @rodeo.sql.exec(%Q(select count(*) from "rd_#{@name}"#{where})) {|res|
        count = res[0]['count'].to_i
      }
      count
    end
    # レコードの簡易検索
    def find(fname,value,sort=nil)
      @roid = nil
      sort = %Q("roid") unless(sort)
      @rodeo.sql.exec(%Q(select * from "rd_#{@name}" where "#{fname}" = '#{value}' order by #{sort})) {|res|
        if(res.num_tuples > 0)
          @roid = res[0]['roid']
          @farray.each {|h| h['fobj'].clear() }
          @farray.each {|h| h['fobj'].from_db(res[0]) }
          @farray.each {|h| h['fobj'].calc() }
        end
      }
      @roid
    end
    # レコード取得
    def fetch(offs = 0,limit = nil,where = nil,sort = nil)
      limit = count(where) unless(limit)
      if(where) then where = " where #{where}"; else where = ""; end
      order = "roid"
      order = self['roid'].conf['order_field'] if(self['roid'].conf['order_field'])
      if(sort)  then sort = " order by #{sort}"; else sort = " order by \"#{order}\""; end
      @farray.each {|h| h['fobj'].init() }
      @rodeo.sql.exec(%Q(select * from "rd_#{@name}"#{where}#{sort} offset #{offs} limit #{limit})) {|res|
        res.each {|rec|
          @farray.each {|h| h['fobj'].clear() }
          @farray.each {|h| h['fobj'].from_db(rec) }
          @farray.each {|h| h['fobj'].calc() }
          yield(@roid)
        }
      }
    end
    # 指定レコードの読み込み
    def select(roid)
      rc = false
      if(roid)
        @rodeo.sql.exec(%Q(select * from "rd_#{@name}" where "roid"=#{roid})) {|res|
          if(res.num_tuples > 0)
            @farray.each {|h| h['fobj'].clear() }
            @farray.each {|h| h['fobj'].from_db(res[0]) }
            @farray.each {|h| h['fobj'].calc() }
            rc = true
          end
        }
      end
      rc
    end
    # CGI引数を評価しフィールドに設定する
    def eval_cgi()
      count = 0
      @farray.each {|h|
        h['fobj'].from_cgi()
        if(h['fobj'].error)
          @error = h['fobj'].error if(count == 0)
          count += 1
        end
      }
      count
    end
    # エラーチェック
    def error_check()
      @error = nil
      @farray.each {|h|
        if(h['fobj'].error)
          @error = h['fobj'].error
          break
        end
      }
      @error
    end
    # レコードの初期化
    def clear()
      @farray.each {|h|
        h['fobj'].clear()
      }
    end
    # レコード追加
    def insert()
      flds = []
      vals = []
      @farray.each {|h|
        type = instance_eval("#{h['type']}.type()".untaint)
        if(type.class == Array)
          _make_field_and_value(h['name'],type,h['fobj'].to_db(),flds,vals)
        else
          if(type)
            f = h['fobj'].db_name()
            v = h['fobj'].to_db()
            flds << f
            vals << ((v) ? v : "NULL")
          end
        end
      }
      fld = ("\""+flds.join("\",\"")+"\"").sub(/roid_0/,"roid")
      val = vals.join(",")
      @rodeo.sql.exec(%Q(begin))
      @rodeo.sql.exec(%Q(insert into "rd_#{@name}" (#{fld}) values (#{val})))
      @rodeo.sql.exec(%Q(select max("roid") from "rd_#{@name}")) {|res|
        if(res.num_tuples > 0)
          @roid = res[0]['max'].to_i
        end
      }
      @rodeo.sql.exec(%Q(end))
      self['roid'].from_db({
        'roid'=>@roid,
        'roid_1'=>"{"+@rodeo.user_record.roid.to_s+"}",
        'roid_2'=>"{"+@rodeo.user_record['グループ'].value.collect{|n| n.to_s}.join(",")+"}",
        'roid_3'=>'"'+DateTime.now().strftime("%Y-%m-%d %H:%M:%S")+'"',
        'roid_4'=>nil,
        'roid_5'=>nil,
        'roid_6'=>nil,
        'roid_7'=>false,
      })
      # 挿入フック
      @farray.each {|h| h['fobj'].inserted(@roid) }
      @roid
    end
    # レコード更新
    def update(roid)
      self['roid'].uuser.from_db({
        'roid_4'=>"{"+@rodeo.user_record.roid.to_s+"}",
      })
      self['roid'].ugroup.from_db({
        'roid_5'=>"{"+@rodeo.user_record['グループ'].value.collect{|n| n.to_s}.join(",")+"}",
      })
      self['roid'].utime.from_db({
        'roid_6'=>"'"+DateTime.now().strftime("%Y-%m-%d %H:%M:%S")+"'",
      })
      vpear = []
      @farray.each {|h|
        value = nil
        type = instance_eval("#{h['type']}.type()".untaint)
        value = h['fobj'].to_db() if(type)
        if(type.class == Array)
          _make_value_pear(h['name'],type,value,vpear)
        else
          if(type)
            if(value)
              vpear << "\"#{h['name']}\"=#{value}"
            else
              vpear << "\"#{h['name']}\"=NULL"
            end
          end
        end
      }
      exp = vpear.join(",")
      if(exp != "")
        @rodeo.sql.exec(%Q(update "rd_#{@name}" set #{exp} where "roid"=#{roid}))
      end
      # 更新フック
      @farray.each {|h| h['fobj'].updated(roid) }
    end
    # レコードの削除
    def delete(roid)
      @rodeo.sql.exec(%Q(delete from "rd_#{@name}" where "roid" = '#{roid}'))
      # 削除フック
      @farray.each {|h| h['fobj'].deleted(roid) }
    end
    # 全レコードの削除
    def delete_all(where)
      array = []
      if(where) then where = " where #{where}"; else where = ""; end
      @rodeo.sql.exec(%Q(select * from "rd_#{@name}"#{where})) {|res|
        res.each {|rec|
          @farray.each {|h| h['fobj'].clear() }
          @farray.each {|h| h['fobj'].from_db(rec) }
          if(@fields['roid']['fobj'].mark.value != true)
            array << @fields['roid']['fobj'].value
          end
        }
      }
      array.each {|roid| delete(roid) }
    end
    # 全マークON
    def all_mark_on(where)
      fetch(0,count(where),where,nil) {|roid|
        self['roid'].mark.value = true
        update(roid)
      }
    end
    # 全マークOFF
    def all_mark_off(where)
      fetch(0,count(where),where,nil) {|roid|
        self['roid'].mark.value = false
        update(roid)
      }
    end
    # 全マークREV
    def all_mark_rev(where)
      fetch(0,count(where),where,nil) {|roid|
        self['roid'].mark.value = !self['roid'].mark.value
        update(roid)
      }
    end
    # roidのシーケンス値のリセット
    def roid_reset()
      @rodeo.sql.exec(%Q(select setval('"rd_#{@name}_roid_seq"',1,false)))
    end
  end
end

# テーブルI/O

class Rodeo
  class Table
    # 表示制限用where句生成
    def where_string()
      where = @rodeo.where
      if((not @rodeo.admin) or (@rodeo.fakeuser and @rodeo.fakeuser.to_i != 1))
        warray = []
        @rodeo.user_record.fields.each {|k,v|
          case k
          when "roid","アカウント","グループ","ユーザ名"
          else
            if(@fields[k])
              if(@fields[k]['type'] == v['type'])
                if(v['fobj'].value and v['fobj'].value != "" and v['fobj'].value != [])
                  warray << %Q("#{@fields[k]['fobj'].db_name()}" = #{v['fobj'].to_db()})
                end
              end
            end
          end
        }
        if(warray.size > 0)
          if(where)
            where = where + " and " + warray.join(" and ")
          else
            where = warray.join(" and ")
          end
        end
      end
      where
    end
    def order_string()
      ord = @fields[@rodeo.sort]['fobj'].order()
      if(ord)
        %Q("#{ord}" #{@rodeo.sort_dir},"roid")
      else
        %Q("roid")
      end
    end
    # レコード一覧
    def html_record_list()
      @rodeo.eruby(__FILE__,"rodeo_record_list",binding)
    end
    # ブラウザ表示
    def action_rodeo_browser()
      if(self['roid'].conf['order_field'])
        @rodeo.sort = self['roid'].conf['order_field']
      end
      @rodeo.eruby(__FILE__,"rodeo_browser",binding)
      @rodeo.output()
    end
    # ブラウザ表示
    def action_rodeo_browser_edit()
      @rodeo.eruby(__FILE__,"rodeo_browser_edit",binding)
      @rodeo.output()
    end
    # フィールド一覧のアップデート
    def action_update_field()
      @rodeo.eruby(__FILE__,"rodeo_field_list",binding)
      @rodeo.output()
    end
    # レコード一覧のアップデート
    def action_update_record()
      @rodeo.eruby(__FILE__,"rodeo_record_table",binding)
      @rodeo.output()
    end
    # 操作フィールドのアップデート
    def action_update_function()
      @rodeo.eruby(__FILE__,"rodeo_record_function",binding)
      @rodeo.output()
    end
    # ステータスのアップデート
    def action_update_status()
      @rodeo.eruby(__FILE__,"rodeo_record_status",binding)
      @rodeo.output()
    end
    # JSON形式アップデート
    def action_update()
      part = []
      if(@rodeo['update_field'] == "true")
        @rodeo.eruby(__FILE__,"rodeo_field_list",binding)
        text =  %Q("field":)
        text << "\""
        text << Rodeo.gettext().gsub(/\n/,"\\n").gsub(/\"/,"\\\"")
        text << "\"\n"
        part << text
        Rodeo.clear()
      end
      if(@rodeo['update_list'] == "true")
        @rodeo.eruby(__FILE__,"rodeo_record_table",binding);
        text =  %Q("table":)
        text << "\""
        text << Rodeo.gettext().gsub(/\n/,"\\n").gsub(/\"/,"\\\"")
        text << "\"\n"
        part << text
        Rodeo.clear()
      end
      if(@rodeo['update_func'] == "true")
        @rodeo.eruby(__FILE__,"rodeo_record_function",binding)
        text =  %Q("function":)
        text << "\""
        text << Rodeo.gettext().gsub(/\n/,"\\n").gsub(/\"/,"\\\"")
        text << "\"\n"
        part << text
        Rodeo.clear()
      end
      if(@rodeo['update_stat'] == "true")
        @rodeo.eruby(__FILE__,"rodeo_record_status",binding)
        text =  %Q("status":)
        text << "\""
        text << Rodeo.gettext().gsub(/\n/,"\\n").gsub(/\"/,"\\\"")
        text << "\"\n"
        part << text
        Rodeo.clear()
      end
      json =  "({\n"
      json << part.join(",")
      json << "})\n"
      Rodeo.settext(json)
      @rodeo.output()
    end
    # レコード詳細
    def action_record_spec()
      select(@rodeo.roid)
      @rodeo.eruby(__FILE__,"rodeo_record_spec",binding)
      @rodeo.output()
    end
    # レコード詳細
    def action_record_spec_window()
      select(@rodeo.roid)
      @rodeo.eruby(__FILE__,"rodeo_record_spec_window",binding)
      @rodeo.output()
    end
    # レコード追加
    def action_record_new()
      @insert_mode = true
      clear()
      @rodeo.eruby(__FILE__,"rodeo_record_new",binding)
      @rodeo.output()
    end
    def action_record_new_window()
      @insert_mode = true
      clear()
      @rodeo.eruby(__FILE__,"rodeo_record_new_window",binding)
      @rodeo.output()
    end
    def action_do_record_new()
      @insert_mode = true
      # エラーチェック
      if(eval_cgi() > 0)
        # エラーの時は再入力
        @rodeo.eruby(__FILE__,"rodeo_record_new",binding)
        @rodeo.output()
      else
        roid = insert()
        @rodeo.eruby(__FILE__,"rodeo_record_new_done",binding)
        @rodeo.output()
      end
    end
    def action_do_record_new_window()
      @insert_mode = true
      # エラーチェック
      if(eval_cgi() > 0)
        # エラーの時は再入力
        @rodeo.eruby(__FILE__,"rodeo_record_new_window",binding)
        @rodeo.output()
      else
        roid = insert()
        @rodeo.eruby(__FILE__,"rodeo_record_new_done_window",binding)
        @rodeo.output()
      end
    end
    # レコード編集
    def action_record_edit()
      clear()
      @insert_mode = false
      select(@rodeo.roid)
      @rodeo.eruby(__FILE__,"rodeo_record_edit",binding)
      @rodeo.output()
    end
    def action_record_edit_window()
      @insert_mode = false
      select(@rodeo.roid)
      @rodeo.eruby(__FILE__,"rodeo_record_edit_window",binding)
      @rodeo.output()
    end
    def action_do_record_edit()
      select(@rodeo.roid)
      @insert_mode = false
      # エラーチェック
      if(eval_cgi() > 0)
        # エラーの時は再入力
        @rodeo.eruby(__FILE__,"rodeo_record_edit",binding)
        @rodeo.output()
      else
        update(@rodeo.roid)
        reload = true
        @rodeo.eruby(__FILE__,"rodeo_record_edit_done",binding)
        @rodeo.output()
      end
    end
    def action_do_record_edit_window()
      select(@rodeo.roid)
      @insert_mode = false
      # エラーチェック
      if(eval_cgi() > 0)
        # エラーの時は再入力
        @rodeo.eruby(__FILE__,"rodeo_record_edit_window",binding)
        @rodeo.output()
      else
        update(@rodeo.roid)
        reload = true
#        @rodeo.eruby(__FILE__,"rodeo_record_edit_done_window",binding)
        @rodeo.eruby(__FILE__,"rodeo_record_edit_window",binding)
        @rodeo.output()
      end
    end
    # レコード削除
    def action_record_delete()
      code = "ERR"
      rc = "不明なエラーです"
      if(@rodeo.html_valid?("roid"))
        select(@rodeo.roid)
        delete(@rodeo.roid)
        roid_reset() if(count() == 0)
        code = "OK"
        rc = "「#{@rodeo.roid}」を削除しました"
      end
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # フィールド追加
    def action_field_add()
      code = "ERR"
      rc = "不明なエラーです"
      if(@rodeo.html_valid?("ftype"))
        if(@rodeo.html_valid?("fname") and @rodeo['fname'].strip != "")
          if(not @fields[@rodeo['fname']])
            add_field(@rodeo['fname'],@rodeo['ftype'],true)
            code = "OK"
            rc = "「#{@rodeo['fname']}」を追加しました"
          else
            rc = "「#{@rodeo['fname']}」は既に存在します"
          end
        else
          rc = "フィールド名を指定してください"
        end
      end
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # フィールド削除
    def action_field_delete()
      code = "ERR"
      rc = "不明なエラーです"
      if(@rodeo.html_valid?("fname"))
        if(@fields[@rodeo['fname']])
          delete_field(@rodeo['fname'])
          code = "OK"
          rc = "「#{@rodeo['fname']}」を削除しました"
        else
          code = "ERR"
          rc = "「#{@rodeo['fname']}」がありません"
        end
      end
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # フィールド順の変更
    def action_field_reorder()
      code = "OK"
      rc = "「#{@rodeo.field}」を#{@rodeo['to']}番目に移動しました"
      reorder_field(@rodeo.field,@rodeo['to'].to_i)
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # チェックボックスのON/OFF
    def action_check_onoff()
      code = "ERR"
      rc = "不明なエラーです"
      if(@rodeo.html_valid?("fname"))
        if(@fields[@rodeo['fname']])
          check_onoff(@rodeo['fname'])
          code = "OK"
          rc = "「#{@rodeo['fname']}」のチェックを変更しました"
        else
          code = "ERR"
          rc = "「#{@rodeo['fname']}」がありません"
        end
      end
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # マークのON/OFF
    def action_mark_onoff()
      rc = "ERR:不明なエラーです"
      if(@rodeo.html_valid?("roid"))
        select(@rodeo.roid)
        if(self['roid'].mark.value)
          self['roid'].mark.value = false
          rc = "OK:「#{@rodeo.roid}」をアンマークしました"
        else
          self['roid'].mark.value = true
          rc = "OK:「#{@rodeo.roid}」をマークしました"
        end
        update(@rodeo.roid)
      end
      Rodeo.puts rc
      @rodeo.output()
    end
    # 全マークON
    def action_all_mark_on()
      all_mark_on(@rodeo.where)
      code = "OK"
      rc = "全てのマークをONにしました"
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # 全マークOFF
    def action_all_mark_off()
      all_mark_off(@rodeo.where)
      code = "OK"
      rc = "全てのマークをOFFにしました"
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # 全マークREV
    def action_all_mark_rev()
      all_mark_rev(@rodeo.where)
      code = "OK"
      rc = "全てのマークを反転しました"
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # マークされたレコードの削除
    def action_mark_delete()
      code = "ERR"
      rc = "マークレコードがありません"
      order = order_string()
      where = where_string()
      delete = []
      fetch(0,count(where),where,order) {|roid|
        if(self['roid'].mark.value)
          delete << roid
        end
      }
      if(delete .size > 0)
        delete.each {|roid|
          delete(roid)
        }
        roid_reset() if(count() == 0)
        code = "OK"
        rc = "マークレコードを削除しました"
      end
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # マークされたレコードで補完
    def action_where_fill()
      code = "ERR"
      rc = "マークレコードがありません"
      order = order_string()
      where = where_string()
      fill = nil
      fetch(0,count(where),where,order) {|roid|
        if(self['roid'].mark.value)
          fill = self[@rodeo.field].value.dup
          break
        end
      }
      if(fill)
        where = where_string()
        fetch(0,count(where),where,order) {|roid|
          self[@rodeo.field].value = fill
          update(roid)
        }
        code = "OK"
        rc = "マークレコードのデータで「#{@rodeo.field}」を補完しました"
      end
      all_mark_off(@rodeo.where)
      Rodeo.puts "#{code}:#{rc}"
      @rodeo.output()
    end
    # マークされたレコードの編集
    def action_mark_edit()
      @rodeo.eruby(__FILE__,"rodeo_mark_edit",binding)
      @rodeo.output()
      all_mark_off(@rodeo.where)
    end
    # フィールド設定
    def action_field_config()
      @rodeo.eruby(__FILE__,"rodeo_field_config",binding)
      @rodeo.output()
    end
    def action_do_field_config()
      self[@rodeo.field].eval_config()
      config_field(@rodeo.field)
      @rodeo.eruby(__FILE__,"rodeo_field_config_done",binding)
      @rodeo.output()
    end
    # 絞りこみ設定
    def action_where_config()
      order = order_string()
      where = %Q("roid_7" = true)
      roid = nil
      fetch(0,count(where),where,order) {|roid|}
      if(roid)
        where = self[@rodeo.field].eval_where_marked(roid)
        @rodeo.eruby(__FILE__,"rodeo_where_config_done",binding)
        @rodeo.output()
      else
        @rodeo.eruby(__FILE__,"rodeo_where_config",binding)
        @rodeo.output()
      end
    end
    def action_do_where_config()
      where = self[@rodeo.field].eval_where()
      @rodeo.eruby(__FILE__,"rodeo_where_config_done",binding)
      @rodeo.output()
    end
    def action_do_where_clear()
      @rodeo.eruby(__FILE__,"rodeo_where_clear",binding)
      @rodeo.output()
    end
  end
end

__END__

#### rodeo_browser

<%#if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)%>
<%#@rodeo.eruby(__FILE__,"iphone_browser",binding)%>
<%#else%>
<html>
  <head>
    <%dname = name%>
    <%dname = self['roid'].conf['table_alias'] if(self['roid'].conf['table_alias'] != "")%>
    <title><%=@rodeo.db%>/<%=dname%></title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function calc_elems() {
      }
    </script>
  </head>
  <!-- body
   onload  ="calc_elems(); height_adjusted = false; height_adjust(); table_adjusted = false; table_adjust();"
   onresize="calc_elems(); height_adjusted = false; height_adjust(); table_adjusted = false; table_adjust();" -->
  <body
   onload  ="calc_elems(); height_adjusted = false; height_adjust(); table_adjusted = false; table_adjust();"
   onresize="calc_elems(); height_adjusted = false; height_adjust(); table_adjusted = false; table_adjust();"
  >
    <%html_record_list()%>
    <%@rodeo.eruby(__FILE__,"rodeo_browse_work",binding)%>
  </body>
</html>
<%#end%>

#### rodeo_browser_edit

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@rodeo.html_rodeo_javascript()%>
    <title><%=@rodeo.db%>/<%=name%></title>
  </head>
  <!-- body
   onload  ="height_adjusted = false; height_adjust(); table_adjusted = false; table_adjust(); record_edit(<%=@rodeo['roid']%>);"
   onresize="height_adjusted = false; height_adjust(); table_adjusted = false; table_adjust();" -->
  <body
   onload  ="height_adjusted = false; height_adjust(); table_adjusted = false; table_adjust(); record_edit(<%=@rodeo['roid']%>);">
    <%html_record_list()%>
    <%@rodeo.eruby(__FILE__,"rodeo_browse_work",binding)%>
  </body>
</html>

#### rodeo_logo

<%insert_acct = (@rodeo.admin or (self['roid'].conf['record_insert'] & @rodeo.user_record['グループ'].value != []))%>
<%delete_acct = (@rodeo.admin or (self['roid'].conf['record_delete'] & @rodeo.user_record['グループ'].value != []))%>
<%update_acct = (@rodeo.admin or (self['roid'].conf['record_update'] & @rodeo.user_record['グループ'].value != []))%>
<div id="rodeo_logo" class="header">
  <table class="table_tools">
    <tr>
      <td class="tools" nowrap><span style="color:#590000;font-weight:bold;">Rodeo</span>&nbsp;</td>
      <%dname = name%>
      <%dname = self['roid'].conf['table_alias'] if(self['roid'].conf['table_alias'] != "")%>
      <td class="tools" nowrap><%=@rodeo.user_record['ユーザ名'].value%>@<%=@rodeo.db%>/<%=dname%></td>
      <td class="tools" width = "100%" nowrap></td>
      <td class="tools" nowrap>
        <%@rodeo.eruby(__FILE__,"rodeo_record_tools",binding)%>
      </td>
    </tr>
  </table>
  <table class="table_tools">
    <tr>
      <td class="tools" nowrap>
        <%@rodeo.html_imglink("javascript:record_new()", "stock_data-sources-new.png","追加(A)","新しいレコードの追加",insert_acct,{"accesskey"=>"A"})%>
        &nbsp;
      </td>
      <td class="tools" nowrap>
        <%@rodeo.html_imglink("javascript:mark_delete()","stock_data-sources-delete.png","削除(D)","マークしたレコードの削除",delete_acct,{"accesskey"=>"D"})%>
        &nbsp;
      </td>
      <td class="tools" id="rodeo_message_box" style="width:100%; vertical-align:top;" nowrap>
        <style type="text/css">
          div.popup {
            visibility: hidden;
            position: absolute;
            background-color: ivory;
            border-style: solid;
            border-color: gray;
            border-width: 1px;
            color: red;
            padding: 2px 4px 2px 4px;
          }
        </style>
        <div class="popup" id="rodeo_message" onclick="message_clear();"><%=error%></div>
      </td>
      <td class="tools" nowrap>
        <div id="rodeo_record_status">
          <%@rodeo.eruby(__FILE__,"rodeo_record_status",binding)%>
        </div>
      </td>
    </tr>
  </table>
</div>

#### rodeo_calendar

<table id="calendar" class="record_list">
  <%
    n = 0
    order = order_string()
    where = where_string()
  %>
  <%
    now = Time.now()
    y = now.year
    m = now.month
    ny = y
    nm = m + 1
    if(nm > 12)
      ny += 1
      nm = 1
    end
    days = Time.local(ny,nm,1).yday - Time.local(y,m,1).yday
    days += 365 if(days < 0)
    week = ["日","月","火","水","木","金","土"]
    wcol = ["red","black","black","black","black","black","blue"]
    s = -Time.local(y,m,1).wday()
  %>
  <tr>
    <%(0..6).each {|i|%>
      <th class="record_list"><font color="<%=wcol[i]%>"><%=week[i]%></font></th>
    <%}%>
  </tr>
  <%j = 0%>
  <%(s .. days-1).each {|i|%>
    <%if((j % 7) == 0)%>
      <tr>
    <%end%>
    <%w = i % 7%>
    <%if(i < 0)%>
      <td class="record_list"><br></td>
    <%else%>
      <td class="record_list" style="text-align:right;"><%=i+1%></td>
    <%end%>
    <%if((j % 7) == 6)%>
      </tr>
    <%end%>
    <%j += 1%>
  <%}%>
  <%if((j % 7) != 0)%>
    <%while((j % 7) != 0)%>
      <td class="record_list"><br></td>
      <%j += 1%>
    <%end%>
    </tr>
  <%end%>
</table>

#### rodeo_record_tools

<%disable = ["RFroid","RFid","RFsession","RFpasswd"]%>
<%insert_acct = (@rodeo.admin or (self['roid'].conf['record_insert'] & @rodeo.user_record['グループ'].value != []))%>
<%delete_acct = (@rodeo.admin or (self['roid'].conf['record_delete'] & @rodeo.user_record['グループ'].value != []))%>
<%field_insert = (@rodeo.admin or (self['roid'].conf['field_insert'] & @rodeo.user_record['グループ'].value != []))%>
<%@rodeo.html_form("javascript:field_add('field_data');void(0)",:id=>"field_data") {%>
  <td class="tools">
    <%opt = {}%>
    <%opt['disabled'] = "true" unless(field_insert)%>
    <%@rodeo.html_select("ftype","",opt) {%>
      <%i = 0%>
      <%rodeo.flist.sort(){|a,b| a[0]<=>b[0]}.each {|key,hash|%>
        <%rodeo.html_optgroup("#{@rodeo.flabel[key]}",{"class"=>"flist#{i}"}) {%>
          <%hash.sort{|a,b|a[1]<=>b[1]}.each {|k,v|%>
            <%if(not disable.include?(k))%>
              <%@rodeo.html_item("#{k}","#{v}")%>
            <%end%>
          <%}%>
        <%}%>
        <%i += 1%>
      <%}%>
    <%}%>
  </td>
  <td class="tools">型</td>
  <%opt = {'size'=>16}%>
  <%opt['disabled'] = "true" unless(field_insert)%>
  <td class="tools"><%@rodeo.html_text("fname","",opt)%></td>
  <%opt = {}%>
  <%opt['disabled'] = "true" unless(field_insert)%>
  <td class="tools"><%@rodeo.html_submit("submit","F追加",opt)%></td>
<%}%>

#### rodeo_record_status

<table class="table_tools">
  <tr>
    <%
      where = where_string()
      reccount = count(where)
      pages = reccount / @rodeo.elems.to_i
      pages += 1 if((reccount % @rodeo.elems.to_i) != 0)
      now_page = @rodeo.offset / @rodeo.elems.to_i
      if(where)
        marked = count(%Q(#{where} and "roid_7" = true))
      else
        marked = count(%Q("roid_7" = true))
      end
    %>
    <script type="text/javascript">
      rodeo_records = <%=reccount%>;
    </script>
    <!-- レコード/マーク件数 -->
    <td class="tools" nowrap>Rec：<%=@rodeo.offset + 1%>-<%=@rodeo.offset + @rodeo.elems.to_i%>/<%=reccount%>件</td>
    <!-- 並べ替え条件 -->
    <td class="tools" nowrap>
      <%if(@rodeo.sort)%>
        <%if(@rodeo.sort_dir == "asc")%>
          Ord：<%=@rodeo.sort%>/昇順
        <%else%>
          Ord：<%=@rodeo.sort%>/降順
        <%end%>
      <%else%>
        Ord：roid/昇順
      <%end%>
    </td>
    <%if(count() == 0)%>
      <%if(realcount() == 0)%>
        <script type="text/javascript">
          parent.message_show("レコードがありません");
        </script>
      <%else%>
        <script type="text/javascript">
          parent.message_show("条件に一致するレコードがありません");
        </script>
      <%end%>
    <%end%>
    <!-- 絞り込み条件 -->
    <%if(@rodeo.where)%>
      <td class="tools">
        <span class="help" onMouseOver="return where_popup();" onMouseOut="return nd();">
          <a href="javascript:nd();where_clear();"><img class="icon" src="image/enable/stock_delete-autofilter.png"></a>
        </span>
      </td>
    <%end%>
    <!-- 先頭 -->
    <%sw = ((now_page == 0) ? false : true)%>
    <td class="tools"><%@rodeo.html_imglink("javascript:page_jump(0)","stock_data-first.png","",nil,sw)%></td>
    <!-- 前へ -->
    <%sw = ((now_page == 0) ? false : true)%>
    <td class="tools"><%@rodeo.html_imglink("javascript:page_jump(#{@rodeo.offset - @rodeo.elems.to_i})","stock_data-previous.png","",nil,sw)%></td>
    <!-- 各ページ -->
    <%
      delta = 5
      if(pages > delta * 2)
        lo = now_page - delta
        hi = now_page + delta
        if(lo < 0)
          hi -= lo
          lo = 0
        end
        if(hi >= pages)
          hi = pages - 1
          lo = hi - delta * 2
        end
      else
        lo = 0
        hi = pages - 1
        if(hi < 0)
          hi = 0
        end
      end
    %>
    <%(lo..hi).each {|i|%>
      <%sw = ((now_page == i) ? false : true)%>
      <td class="tools"><%@rodeo.html_imglink("javascript:page_jump(#{i * @rodeo.elems.to_i})",nil,"#{i+1}",nil,sw)%></td>
    <%}%>
    <!-- 次へ -->
    <%sw = ((now_page >= pages - 1) ? false : true)%>
    <td class="tools"><%@rodeo.html_imglink("javascript:page_jump(#{@rodeo.offset + @rodeo.elems.to_i})","stock_data-next.png","",nil,sw)%></td>
    <!-- 末尾 -->
    <%sw = ((now_page >= pages - 1) ? false : true)%>
    <td class="tools"><%@rodeo.html_imglink("javascript:page_jump(#{@rodeo.elems.to_i * (pages - 1)})","stock_data-last.png","",nil,sw)%></td>
    <!-- 畳む/展開 -->
    <td class="tools"><%@rodeo.html_imglink("javascript:list_expand();","object-flip-vertical.png","",nil,true)%></td>
  </tr>
</table>

#### rodeo_browse_work

<iframe id="rodeo_work" class="work" src="" width="1" height="0" frameborder="0" style="overflow-x:auto; overflow-y:auto">
</iframe>

#### rodeo_record_list

<%
  update_acct = (@rodeo.admin or (self['roid'].conf['record_update'] & @rodeo.user_record['グループ'].value != []))
  delete_acct = (@rodeo.admin or (self['roid'].conf['record_delete'] & @rodeo.user_record['グループ'].value != []))
%>
<%@rodeo.eruby(__FILE__,"rodeo_logo",binding)%>
<table id="rodeo_browser" class="record_frame" style="border-width:1px;border-style:solid" width="100%">
  <tr>
    <td class="record_frame" style="padding:2px; background-color:#eeeeee">
      <%#@rodeo.html_imglink("javascript:all_mark_on()", "stock_form-checkon.png", "O N","すべてのマークをON",delete_acct)%><!-- br -->
      <%@rodeo.html_imglink("javascript:all_mark_off()","stock_form-checkoff.png","OFF","すべてのマークをOFF",delete_acct)%><br>
      <%@rodeo.html_imglink("javascript:all_mark_rev()","stock_form-checkrev.png","REV","すべてのマークを反転",delete_acct)%>
    </td>
    <td class="record_frame" width="100%">
      <div id="rodeo_field_list" class="fields" style="width:100%; overflow-y:auto;">
        <%@rodeo.eruby(__FILE__,"rodeo_field_list",binding)%>
      </div>
    </td>
  </tr>
    <td class="record_frame" style="vertical-align: top;">
      <div id="rodeo_record_function" class="frame" style="vertical-align: top;">
        <%@rodeo.eruby(__FILE__,"rodeo_record_function",binding)%>
      </div>
    </td>
    <td class="record_frame" style="vertical-align: top;">
      <div id="rodeo_record_list" class="records" onscroll="scroll_sync()" style="width:100%;">
        <%@rodeo.eruby(__FILE__,"rodeo_record_table",binding)%>
      </div>
    </td>
  </tr>
</table>

<!--
</td>
</table>
-->

#### rodeo_field_list

<table id="if_head" class="field_list" style="padding:0px">
  <tr>
    <%
      field_delete = (@rodeo.admin or (self['roid'].conf['field_delete'] & @rodeo.user_record['グループ'].value != []))
      field_config = (@rodeo.admin or (self['roid'].conf['field_config'] & @rodeo.user_record['グループ'].value != []))
      update_acct = (@rodeo.admin or (self['roid'].conf['record_update'] & @rodeo.user_record['グループ'].value != []))
      delete_acct = (@rodeo.admin or (self['roid'].conf['record_delete'] & @rodeo.user_record['グループ'].value != []))
    %>
    <%@farray.each_index {|i|%>
      <th class="record_list" style="padding:0px" nowrap>
        <!-- 1 -->
        <table class="record_frame_top" width="100%">
          <tr style="margin:0px">
            <!-- チェックボックス -->
            <th class="field_element" nowrap>
              <%#if(@farray[i]['fobj'].icon() != nil)%>
                <%#@rodeo.html_checkbox("","",@farray[i]['delf'],{"onchange"=>"check_onoff('#{@farray[i]['name']}')","disabled"=>"true"})%>
              <%#else%>
              <%#end%>
            </th>
            <!-- フィールド名 -->
            <%if(@farray[i]['delf'])%>
              <th class="field_element" nowrap>
                <%if(field_config)%>
                  <%
                    if(@farray[i]['fobj'].class != RFerror)
                      type = instance_eval("#{@farray[i]['type']}.name()".untaint)
                      name = @farray[i]['name']
                      name = @farray[i]['fobj'].conf['field_alias'] if(@farray[i]['fobj'].conf['field_alias'] != "")
                      @rodeo.html_imglink("javascript:field_config('#{@farray[i]['name']}')",nil,name,"#{type}型",field_config)
                    else
                      @rodeo.html_imglink("javascript:message_show('#{@farray[i]['type']}型オブジェクトが見つかりません');",
                        nil,"#{@farray[i]['name']}","#{@farray[i]['type']}型",field_config,{"style"=>"color:red;"})
#                      Rodeo.print %Q(<span style="color:red;">#{@farray[i]['name']}</span>)
                    end
                  %>
                  <%#@rodeo.html_imglink("javascript:field_config('#{@farray[i]['name']}')",nil,"#{@farray[i]['name']}","#{name}",field_config)%>
                <%else%>
                  <%name = @farray[i]['name']%>
                  <%name = @farray[i]['fobj'].conf['field_alias'] if(@farray[i]['fobj'].conf['field_alias'] != "")%>
                  <%=name%>
                <%end%>
                <%if(@farray[i]['fobj'].conf['accesskey'] != "")%>
                  <tt>(<u><%=@farray[i]['fobj'].conf['accesskey']%></u>)</tt>
                <%end%>
              </th>
              <!-- スペーサー -->
              <th class="field_element" width="100%" style="padding:0px">&nbsp;</th>
              <!-- 並べ替え -->
              <%if(@farray[i]['sort'] != 0)%>
                <%opt = {"onchange"=>"field_reorder('#{@farray[i]['name']}',this)"}%>
                <%opt = {"disabled"=>"true"} if(field_config == false)%>
                <th class="field_element" nowrap>
                  <%@rodeo.html_select("order",@farray[i]['sort'],opt) {%>
                    <%@farray.each {|h|%>
                      <%if(h['sort'] != 0 and h['delf'])%>
                        <%@rodeo.html_item(h['sort'],"#{h['sort']}")%>
                      <%end%>
                    <%}%>
                  <%}%>
                </th>
              <%else%>
                <th class="field_element" nowrap>
                  <%@rodeo.html_select("order",@farray[i]['sort'],{"disabled"=>"true"}) {%>
                  <%}%>
                </th>
              <%end%>
            <%else%>
              <th class="field_element" nowrap>
                <img src="image/enable/blank.png" style="width:1px;height:<%=@rodeo.line_height%>px;vertical-align:middle">
              </th>
            <%end%>
          </tr>
        </table>
        <table class="record_frame_top" width="100%">
          <tr>
            <%if(@farray[i]['delf'])%>
              <!-- 並べ替え -->
              <%if(@farray[i]['fobj'].order())%>
                <th class="field_element" nowrap>
                  <%#@rodeo.html_imglink("javascript:sort_asc('#{@farray[i]['name']}')","stock_navigate-prev.png","","昇順",true)%>
                  <%@rodeo.html_imglink("javascript:sort_asc('#{@farray[i]['name']}')",nil,"▲","昇順",true)%>
                </th>
                <th class="field_element" nowrap>
                  <%#@rodeo.html_imglink("javascript:sort_dsc('#{@farray[i]['name']}')","stock_navigate-next.png","","降順",true)%>
                  <%@rodeo.html_imglink("javascript:sort_dsc('#{@farray[i]['name']}')",nil,"▼","降順",true)%>
                </th>
              <%end%>
              <!-- 絞り込み -->
              <%if(@farray[i]['sort'] != 0 and @farray[i]['fobj'].icon() == nil)%>
                <%if(defined?(@farray[i]['fobj'].eval_where))%>
                  <th class="field_element" nowrap>
                    <%@rodeo.html_imglink("javascript:where_config('#{@farray[i]['name']}')","stock_autofilter.png","","絞りこみの設定",true)%>
                    <%#@rodeo.html_imglink("javascript:where_config('#{@farray[i]['name']}')",nil,"絞","絞りこみの設定",true)%>
                  </th>
                <%end%>
              <%else%>
                <%if(@farray[i]['sort'] == 0)%>
                  <th class="field_element" nowrap>
                    <%@rodeo.html_imglink("javascript:where_clear()","stock_delete-autofilter.png","","絞りこみの解除",true)%>
                    <%#@rodeo.html_imglink("javascript:where_clear()",nil,"全","絞りこみの解除",true)%>
                  </th>
                <%end%>
              <%end%>
              <!-- コピー -->
              <%if(@farray[i]['sort'] != 0 and @farray[i]['fobj'].icon() == nil and @farray[i]['fobj'].conf['complete'] == true)%>
                <%if(defined?(@farray[i]['fobj'].eval_where))%>
                  <th class="field_element" nowrap>
                    <%@rodeo.html_imglink("javascript:where_fill('#{@farray[i]['name']}')","edit-find-replace.png",nil,"カラムデータで補完",update_acct)%>
                    <%#@rodeo.html_imglink("javascript:where_fill('#{@farray[i]['name']}')",nil,"補","カラムデータで補完",update_acct)%>
                  </th>
                <%end%>
              <%end%>
              <!-- アプリケーション実行 -->
              <%if(@farray[i]['fobj'].icon())%>
                <th class="field_element" nowrap>
                  <%@rodeo.html_imglink("javascript:app_exec('#{@farray[i]['name']}',0)",
                    @farray[i]['fobj'].icon(),nil,"アプリケーションの実行",true)%>
                </th>
              <%end%>
              <!-- スペーサー -->
              <th class="field_element" width="100%" style="padding:0px">&nbsp;</th>
              <!-- フィールド削除 -->
              <th class="field_element" nowrap>
                <%if(@farray[i]['sort'] != 0)%>
                  <%@rodeo.html_imglink("javascript:field_delete('#{@farray[i]['name']}')","stock_delete-column.png",nil,"フィールド削除",field_delete)%>
                  <%#@rodeo.html_imglink("javascript:field_delete('#{@farray[i]['name']}')",nil,"×","フィールド削除",field_delete)%>
                <%else%>
                  &nbsp;
                <%end%>
              </th>
            <%else%>
              <th class="field_element" nowrap>
                <img src="image/enable/blank.png" style="width:1px;height:<%=@rodeo.line_height%>px;vertical-align:middle">
              </th>
            <%end%>
          </tr>
        </table>
        <!-- 統計情報 -->
        <table class="record_frame_top" width="100%">
          <tr style="margin:0px">
            <!-- チェックボックス -->
            <th class="field_element" nowrap>
              <%if(@farray[i]['delf'])%>
                <%@rodeo.html_imglink("javascript:check_onoff('#{@farray[i]['name']}')","stock_data-show.png",nil,"#{@farray[i]['name']}を隠す",update_acct)%>
              <%else%>
                <%@rodeo.html_imglink("javascript:check_onoff('#{@farray[i]['name']}')","stock_data-next.png",nil,"#{@farray[i]['name']}を表示",update_acct)%>
              <%end%>
            </th>
            <%if(@farray[i]['delf'])%>
              <!-- 統計情報 -->
              <th class="field_element" width="100%" style="padding:0px">&nbsp;</th>
              <%obj = @farray[i]['fobj']%>
              <%if(@farray[i]['sort'] != 0 and @farray[i]['fobj'].conf['stats'])%>
                <th class="field_element" nowrap>
                  <%sum = obj.sum()%>
                  <%max = obj.max()%>
                  <%min = obj.min()%>
                  <%ave = obj.ave()%>
                  <%val = obj.val()%>
                  <%nval = obj.nval()%>
                  <%if(sum or max or min or ave or val or nval)%>
                    <%@rodeo.html_select("",@farray[i]['sort']) {%>
                      <%if(sum) then obj.value = sum; Rodeo.print %Q(<option value="">合計:); obj.display(true); Rodeo.print %Q(</option>); end%>
                      <%if(max) then obj.value = max; Rodeo.print %Q(<option value="">最大:); obj.display(true); Rodeo.print %Q(</option>); end%>
                      <%if(min) then obj.value = min; Rodeo.print %Q(<option value="">最小:); obj.display(true); Rodeo.print %Q(</option>); end%>
                      <%if(ave) then obj.value = ave; Rodeo.print %Q(<option value="">平均:); obj.display(true); Rodeo.print %Q(</option>); end%>
                      <%if(val) then Rodeo.print %Q(<option value="">有効:#{val}</option>); end%>
                      <%if(nval) then Rodeo.print %Q(<option value="">無効:#{nval}</option>); end%>
                    <%}%>
                  <%else%>
                    <%@rodeo.html_select("order",@farray[i]['sort'],{"disabled"=>"true"}) {}%>
                  <%end%>
                </th>
              <%else%>
                <th class="field_element" nowrap>
                  <%@rodeo.html_select("order",@farray[i]['sort'],{"disabled"=>"true"}) {}%>
                </th>
              <%end%>
            <%end%>
          </tr>
        </table>
      </th>
    <%}%>
    <th class="field_list" width="100%" style="padding:0px"></th>
  </tr>
</table>

#### rodeo_record_function

<table id="if_func" class="record_list">
  <%n = 0%>
  <%
    update_acct = (@rodeo.admin or (self['roid'].conf['record_update'] & @rodeo.user_record['グループ'].value != []))
    delete_acct = (@rodeo.admin or (self['roid'].conf['record_delete'] & @rodeo.user_record['グループ'].value != []))
  %>
  <%order = order_string()%>
  <%where = where_string()%>
  <%fetch(@rodeo.offset,@rodeo.elems.to_i,where,order) {|roid|%>
    <tr>
      <td class="record_list" nowrap>
        <%
          if(not update_acct)
            opt = {"disabled"=>"yes"}
          else
            opt = {"onchange"=>"mark_onoff('#{roid}');"}
          end
        %>
        <%@rodeo.html_checkbox("","",self['roid'].mark.value,opt)%>
        <img src="image/enable/blank.png" style="width:1px;height:<%=@rodeo.line_height%>px;vertical-align:middle">
      </td>
      <td class="record_list" nowrap>
        <%@rodeo.html_imglink("javascript:record_delete(#{roid})","stock_data-sources-delete.png",nil,"削除する",update_acct)%>
        <%#@rodeo.html_imglink("javascript:record_delete(#{roid})",nil,"削除","削除する",update_acct)%>
      </td>
      <td class="record_list" nowrap>
        <%@rodeo.html_imglink("javascript:record_show(#{roid})","stock_data-edit-table.png",nil,"編集する",update_acct)%>
        <%#@rodeo.html_imglink("javascript:record_edit(#{roid})",nil,"編集","編集する",update_acct)%>
      </td>
    </tr>
    <%n += 1%>
  <%}%>
  <%#while(n < @rodeo.elems.to_i)%>
  <%while(n < @rodeo.elems.to_i)%>
    <tr>
      <td class="record_list" nowrap>
        <%@rodeo.html_checkbox("","",false,{"disabled"=>"yes"})%>
        <img src="image/enable/blank.png" style="width:1px;height:<%=@rodeo.line_height%>px;vertical-align:middle">
      </td>
      <td class="record_list" nowrap>
        <%@rodeo.html_imglink("","stock_data-sources-delete.png",nil,nil,false)%>
        <%#@rodeo.html_imglink("",nil,"削除",nil,false)%>
      </td>
      <td class="record_list" nowrap>
        <%@rodeo.html_imglink("","stock_data-edit-table.png",nil,nil,false)%>
        <%#@rodeo.html_imglink("",nil,"編集",nil,false)%>
      </td>
    </tr>
    <%n += 1%>
  <%end%>
</table>

#### rodeo_record_table

<table id="if_body" class="record_list" onscroll="scroll_sync()">
  <%
    update_acct = @rodeo.admin or (self['roid'].conf['record_update'] & @rodeo.user_record['グループ'].value != [])
    delete_acct = @rodeo.admin or (self['roid'].conf['record_delete'] & @rodeo.user_record['グループ'].value != [])
    n = 0
    order = order_string()
    where = where_string()
  %>
  <script type="text/javascript">
    rodeo_records = <%=count(where)%>;
  </script>
  <%fetch(@rodeo.offset,@rodeo.elems.to_i,where,order) {|roid|%>
    <tr onclick="table_click(this,'<%=roid%>')" ondblclick="table_dblclick(this,'<%=roid%>',<%=((update_acct)?"true":"false")%>)">
      <%@farray.each_index {|i|%>
        <td class="record_list" style="<%=@farray[i]['fobj'].style%>" nowrap>
          <%if(i == 0)%>
            <img src="image/enable/blank.png" style="width:1px;height:<%=@rodeo.line_height%>px;vertical-align:middle;">
          <%end%>
          <%if(@farray[i]['delf'])%>
            <%@farray[i]['fobj'].display(true)%>
          <%end%>
        </td>
      <%}%>
      <td class="record_list" width="100%" style="padding:0px">
      </td>
    </tr>
    <%n += 1%>
  <%}%>
  <%while(n < @rodeo.elems.to_i)%>
    <tr>
      <%@farray.each_index {|i|%>
         <td class="record_list" nowrap>
          <%if(i == 0)%>
            <img src="image/enable/blank.png" style="width:1px;height:<%=@rodeo.line_height%>px;vertical-align:middle;">
          <%end%>
          &nbsp;
         </td>
      <%}%>
      <td class="record_list" width="100%" style="padding:0px"></td>
    </tr>
    <%n += 1%>
  <%end%>
</table>

#### view_body

<%self.farray.each {|fld|%>
  <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true)%>
    <tr>
      <th class="record_spec" width="0%" nowrap>
        <%@rodeo.html_tooltip(fld['name'],fld['fobj'].conf['description'])%>
      </th>
      <td class="record_spec" colspan="2" width="100%">
        <%fld['fobj'].display(false)%>
      </td>
    </tr>
  <%end%>
<%}%>

#### edit_body

<%self.farray.each {|fld|%>
  <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true)%>
    <%if(self.insert_mode != true or fld['name'] != "roid")%>
      <tr>
        <%if(fld['fobj'].conf['bgcolor'])%>
          <th class="record_spec" style="background-color:<%=fld['fobj'].conf['bgcolor']%>;" width="0%" nowrap>
        <%else%>
          <th class="record_spec" width="0%" nowrap>
        <%end%>
          <%@rodeo.html_tooltip(fld['name'],fld['fobj'].conf['description'])%>
          <%if(fld['fobj'].conf['accesskey'] != "")%>
            <tt>(<u><%=fld['fobj'].conf['accesskey']%></u>)</tt>
          <%end%>
          <%if(fld['fobj'].conf['valid'])%>
            <font color="red">※</font>
          <%end%>
        </th>
        <td class="record_edit" colspan="2" width="100%">
          <%fld['fobj'].entry()%>
        </td>
      </tr>
    <%end%>
  <%end%>
<%}%>

#### rodeo_record_spec

<%
  update_acct = (@rodeo.admin or (self['roid'].conf['record_update'] & @rodeo.user_record['グループ'].value != []))
  delete_acct = (@rodeo.admin or (self['roid'].conf['record_delete'] & @rodeo.user_record['グループ'].value != []))
  if(update_acct)
    disable = {"accesskey"=>"E"}
  else
    disable = {"disabled"=>"true","accesskey"=>"E"}
  end
%>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
      }
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
    <title><%=@rodeo.db%>/<%=name%>/<%=@roid%></title>
  </head>
  <body class="margin0" style="padding:1px" onclick="return nd();" onload="on_load_func();">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <th class="record_spec" width="100%" nowrap></th>
      <th class="record_spec"><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
      <%@rodeo.html_form(@rodeo.appname,{"autocomplete"=>"off"}) {%>
        <%@rodeo.html_hidden("action","record_edit")%>
        <th class="record_spec"><%@rodeo.html_submit("submit","編集(E)",disable)%></th>
      <%}%>
    </table>
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <tr>
        <th class="record_spec" nowrap>テーブル</th>
        <th class="record_spec" width="100%" nowrap><%=@name%></th>
      </tr>
      <%@rodeo.eruby(__FILE__,"view_body",binding)%>
    </table>
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <th class="record_spec" width="100%" nowrap></th>
      <th class="record_spec"><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
      <%@rodeo.html_form(@rodeo.appname,{"autocomplete"=>"off"}) {%>
        <%@rodeo.html_hidden("action","record_edit")%>
        <th class="record_spec"><%@rodeo.html_submit("submit","編集(E)",disable)%></th>
      <%}%>
    </table>
  </body>
</html>

#### rodeo_record_spec_window

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><%=@rodeo.db%>/<%=name%>/詳細</title>
    <%@rodeo.html_rodeo_javascript()%>
    <style type="text/css" media="screen"><!--
      .noscreen {
        visibility:hidden
      }
    --></style>
    <style type="text/css" media="print"><!--
      .noprint {
        visibility:hidden
      }
    --></style>
    <script type="text/javascript"><!--
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
      }
    --></script>
  </head>
  <body class="margin0" style="padding:1px" onload="on_load_func();" onclick="return nd();">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <tr>
        <th class="record_spec" nowrap>テーブル</th>
        <th class="record_spec" width="100%" nowrap><%=@name%></th>
        <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"window.close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
      </tr>
      <%@rodeo.eruby(__FILE__,"view_body",binding)%>
      <tr>
        <th class="record_spec" width="100%" colspan="2" nowrap></th>
        <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"window.close();","accesskey"=>"X"})%></th>
      <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
      </tr>
    </table>
  </body>
</html>
<%#end%>

#### rodeo_record_spec_done_window

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        window.close();
      }
    </script>
  </head>
  <%if(reload)%>
    <body onload="on_load_func();">
  <%else%>
    <body>
  <%end%>
  </body>
</html>

#### rodeo_record_new

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
        <%
          target = nil
          @farray.each {|fld|
            if(fld['fobj'].name != "roid")
              if(fld['fobj'].exec == false)
                target = fld['fobj'].cgi_name()
                break
              end
            end
          }
        %>
      }
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body class="margin0" style="padding:1px" onload="on_load_func();">
    <%@rodeo.html_form(@rodeo.appname,{"autocomplete"=>"off"}) {%>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.html_hidden("action","do_record_new")%>
        <tr>
          <th class="record_spec" width="100%" nowrap>
            <%self.farray.each {|fld|%>
              <%if(fld['fobj'].error)%>
                <font color="red"><%=fld['fobj'].error%></font>
                <%break%>
              <%end%>
            <%}%>
          </th>
          <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec" nowrap><%@rodeo.html_submit("submit","追加(A)",{"accesskey"=>"A"})%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.eruby(__FILE__,"edit_body",binding)%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%" nowrap><font color="red">※</font>の項目は必須項目です</th>
          <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec"><%@rodeo.html_submit("submit","追加(A)",{"accesskey"=>"A"})%></th>
        </tr>
      </table>
    <%}%>
  </body>
</html>

#### rodeo_record_new_window

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><%=@rodeo.db%>/<%=name%>/追加</title>
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
        <%
          target = nil
          @farray.each {|fld|
            if(fld['fobj'].name != "roid")
              if(fld['fobj'].exec == false)
                target = fld['fobj'].cgi_name()
                break
              end
            end
          }
        %>
      }
    </script>
  </head>
  <body class="margin0" style="padding:1px" onload="on_load_func();">
    <%@rodeo.html_form(@rodeo.appname,{"autocomplete"=>"off"}) {%>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.html_hidden("action","do_record_new_window")%>
        <tr>
          <th class="record_spec" width="100%" nowrap>
            <%self.farray.each {|fld|%>
              <%if(fld['fobj'].error)%>
                <font color="red"><%=fld['fobj'].error%></font>
                <%break%>
              <%end%>
            <%}%>
          </th>
          <th class="record_spec"><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"window.close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec" nowrap><%@rodeo.html_submit("submit","追加(A)",{"accesskey"=>"A"})%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.eruby(__FILE__,"edit_body",binding)%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%" nowrap><font color="red">※</font>の項目は必須項目です</th>
          <th class="record_spec"><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"window.close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec"><%@rodeo.html_submit("submit","追加(A)",{"accesskey"=>"A"})%></th>
        </tr>
      </table>
    <%}%>
  </body>
</html>

#### rodeo_record_new_done

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <%
    reccount = count()
    pages = reccount / @rodeo.elems.to_i
    pages += 1 if((reccount % @rodeo.elems.to_i) != 0)
    now_page = @rodeo.offset / @rodeo.elems.to_i
    last_page = @rodeo.elems.to_i * (pages - 1)
  %>
  <script type="text/javascript">
    function on_load_func() {
      parent.record_new_done(<%=roid%>,<%=last_page%>);
    }
  </script>
  <body onload="on_load_func();"></body>
</html>

#### rodeo_record_new_done_window

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <%
    reccount = count()
    pages = reccount / @rodeo.elems.to_i
    pages += 1 if((reccount % @rodeo.elems.to_i) != 0)
    now_page = @rodeo.offset / @rodeo.elems.to_i
    last_page = @rodeo.elems.to_i * (pages - 1)
  %>
  <script type="text/javascript">
    function on_load_func() {
      window.opener.record_new_done(<%=roid%>,<%=last_page%>);
      window.close();
    }
  </script>
  <body onload="on_load_func();"></body>
</html>

#### rodeo_record_edit

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
        <%
          target = nil
          @farray.each {|fld|
            if(fld['fobj'].name != "roid")
              if(fld['fobj'].exec == false)
                target = fld['fobj'].cgi_name()
                break
              end
            end
          }
        %>
      }
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
        return(true);
      }
    </script>
  </head>
  <body class="margin0" style="padding:1px" onload="on_load_func();">
    <%@rodeo.html_form(@rodeo.appname,{"autocomplete"=>"off"}) {%>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.html_hidden("action","do_record_edit")%>
        <tr>
          <th class="record_spec" width="100%">
            <%self.farray.each {|fld|%>
              <%if(fld['fobj'].error)%>
                <font color="red"><%=fld['fobj'].error%></font>
                <%break%>
              <%end%>
            <%}%>
          </th>
          <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec" width="0%" nowrap><%@rodeo.html_submit("exec","更新(U)",{"accesskey"=>"U"})%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.eruby(__FILE__,"edit_body",binding)%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%" nowrap><font color="red">※</font>の項目は必須項目です</th>
          <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec" width="0%" nowrap><%@rodeo.html_submit("exec","更新(U)",{"accesskey"=>"U"})%></th>
        </tr>
      </table>
    <%}%>
  </body>
</html>

#### rodeo_record_edit_window

<%#if(ENV['HTTP_USER_AGENT'] =~ /iphone/i)%>
<%#@rodeo.eruby(__FILE__,"iphone_rodeo_record_edit",binding)%>
<%#else%>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><%=@rodeo.db%>/<%=name%>/編集</title>
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
//        window.parent.message_show('レコード「<%=@rodeo.roid%>」を更新しました');
        window.parent.record_list_update(false,true,true,false,null);
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
        <%
          target = nil
          @farray.each {|fld|
            if(fld['fobj'].name != "roid")
              if(fld['fobj'].exec == false)
                target = fld['fobj'].cgi_name()
                break
              end
            end
          }
        %>
      }
    </script>
    </script>
  </head>
  <body class="margin0" style="padding:1px" onload="on_load_func();">
    <%@rodeo.html_form(@rodeo.appname,{"autocomplete"=>"off"}) {%>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.html_hidden("action","do_record_edit_window")%>
        <tr>
          <th class="record_spec" width="100%" nowrap>編集 [<%=@rodeo.db%>/<%=name%>/<%=@rodeo.roid%>]</th>
            <%self.farray.each {|fld|%>
              <%if(fld['fobj'].error)%>
                <font color="red"><%=fld['fobj'].error%></font>
                <%break%>
              <%end%>
            <%}%>
          </th>
          <th class="record_spec"><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"window.close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec" width="0%" nowrap><%@rodeo.html_submit("submit","更新(U)",{"accesskey"=>"U"})%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.eruby(__FILE__,"edit_body",binding)%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%" nowrap><font color="red">※</font>の項目は必須項目です</th>
          <th class="record_spec"><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"window.close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec" width="0%" nowrap><%@rodeo.html_submit("submit","更新(U)",{"accesskey"=>"U"})%></th>
        </tr>
      </table>
    <%}%>
  </body>
</html>
<%#end%>

#### rodeo_record_edit_done

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        window.parent.list_shrink(null);
        window.parent.message_show('レコード「<%=@rodeo.roid%>」を更新しました');
        window.parent.record_list_update(false,true,true,false,null);
      }
    </script>
  </head>
  <%if(reload)%>
    <body onload="on_load_func();">
  <%else%>
    <body>
  <%end%>
  </body>
</html>

#### rodeo_record_edit_done_window

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        try {
          window.opener.parent.record_list_update(false,true,true,false,null);
        } catch(e) {}
        try {
          window.opener.record_list_update(false,true,true,false,null);
        } catch(e) {}
        window.close();
      }
    </script>
  </head>
  <%if(reload)%>
    <body onload="on_load_func();">
  <%else%>
    <body>
  <%end%>
  </body>
</html>

#### rodeo_field_config

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        for(i=0; i < on_load_fook.length; i ++) {
          on_load_fook[i]();
        }
      }
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body class="margin0" style="padding:1px"  onload="on_load_func();">
    <%@rodeo.html_form(@rodeo.appname) {%>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@rodeo.html_hidden("action","do_field_config")%>
        <tr>
          <th class="record_spec" width="100%" nowrap>
            [<%=@rodeo['field']%>/<%=instance_eval("#{@fields[@rodeo['field']]['type']}.name()".untaint)%>型]の設定
          </th>
          <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec"><%@rodeo.html_submit("exec","設定(R)",{"accesskey"=>"R"})%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <%@fields[@rodeo['field']]['fobj'].config_entry()%>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%"></th>
          <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec"><%@rodeo.html_submit("exec","設定(R)",{"accesskey"=>"R"})%></th>
        </tr>
      </table>
    <%}%>
  </body>
</html>

#### rodeo_field_config_done

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        window.parent.list_shrink(null);
        window.parent.message_show('フィールド「<%=@rodeo.field%>」を設定しました');
        window.parent.record_list_update(true,true,true,false,null);
      }
    </script>
  </head>
  <body onload="on_load_func();">
  </body>
</html>

#### rodeo_where_config

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_close() {
        parent.list_shrink(null);
        parent.document.getElementById("rodeo_work").src = "blank.html";
      }
    </script>
  </head>
  <body class="margin0" style="padding:1px">
    <table class="record_spec" width="100%" style="margin-bottom:1px">
      <%@rodeo.html_form(@rodeo.appname) {%>
        <%@rodeo.html_hidden("action","do_where_config")%>
        <tr>
          <th class="record_spec" width="100%" nowrap>
            [<%=@rodeo['field']%>/<%=instance_eval("#{@fields[@rodeo['field']]['type']}.name()".untaint)%>型]絞りこみ条件の設定
          </th>
          <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec"><%@rodeo.html_submit("exec","絞り込み(F)",{"accesskey"=>"F"})%></th>
        </tr>
      </table>
      <table class="record_spec" width="100%" style="margin-bottom:1px">
        <tr>
          <%@fields[@rodeo['field']]['fobj'].where_entry()%>
        </tr>
      </table>
      <table class="record_spec" width="100%">
        <tr>
          <th class="record_spec" width="100%"></th>
          <th class="record_spec" nowrap><%@rodeo.html_button("submit","キャンセル(X)",{"onclick"=>"on_close();","accesskey"=>"X"})%></th>
          <th class="record_spec"><%@rodeo.html_button("submit","印刷(P)",{"onclick"=>"window.print();","accesskey"=>"P"})%></th>
          <th class="record_spec"><%@rodeo.html_submit("exec","絞り込み(F)",{"accesskey"=>"F"})%></th>
        </tr>
      <%}%>
    </table>
  </body>
</html>

#### rodeo_where_config_done

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <%if(where)%>
      <%if(@rodeo['where'] != "")%>
        <%@rodeo['where'] << " and #{where}"%>
      <%else%>
        <%@rodeo['where'] = "#{where}"%>
      <%end%>
      <script type="text/javascript">
        function on_load_func() {
          if(parent.rodeo_where == "") {
            parent.rodeo_where = '<%=where.gsub(/\'/,"\\\\'")%>';
            parent.rodeo_old_offset = parent.rodeo_offset;
            parent.rodeo_old_sort = parent.rodeo_sort;
            parent.rodeo_old_sort_dir = parent.rodeo_sort_dir;
          } else {
            parent.rodeo_where = parent.rodeo_where.concat(' and ');
            parent.rodeo_where = parent.rodeo_where.concat('<%=where.gsub(/\'/,"\\\\'")%>');
          }
          parent.rodeo_records = <%=count(@rodeo['where'])%>;
          parent.parent.message_show('絞りこみ条件を追加しました');
          parent.rodeo_offset = 0;
          if(parent.list_shrinked) parent.list_shrink(null);
          parent.field_update();
        }
      </script>
    <%end%>
  </head>
  <%if(where)%>
    <body onload="on_load_func();">
  <%else%>
    <body onload="parent.parent.message_show('絞りこみ条件が不正です');">
  <%end%>
  </body>
</html>

#### rodeo_where_clear

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        parent.rodeo_records = <%=count()%>;
        parent.parent.message_show('絞りこみ条件を解除しました');
      }
    </script>
  </head>
  <body onload="on_load_func();">
  </body>
</html>

#### rodeo_mark_edit

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <%@rodeo.html_rodeo_javascript()%>
    <script type="text/javascript">
      function on_load_func() {
        <%where = %Q("roid_7" = true)%>
        <%fetch(0,count(where),where,nil) {|roid|%>
          record_edit_window(<%=roid%>);
        <%}%>
        window.parent.record_list_update(false,false,true,false,null);
      }
    </script>
  </head>
  <body onload="on_load_func();">
  </body>
</html>

#### iphone_browser

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><%=@rodeo.db%>/<%=name%></title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <link rel="apple-touch-icon" href="lib/iui/iui-logo-touch-icon.png" />
    <meta name="apple-touch-fullscreen" content="YES" />
    <style type="text/css" media="screen">@import "lib/iui/iui.css";</style>
    <script type="application/x-javascript" src="lib/iui/iui.js"></script>
    <%@rodeo.html_rodeo_javascript()%>
    <style type="text/css">
      body > ul > li {
        font-size: 16px;
      }
      body > ul > li > a {
        padding-left: 54px;
        padding-right: 40px;
        min-height: 34px;
      }
      li .roid {
        display: block;
        position: absolute;
        margin: 0;
        left: 6px;
        top: 7px;
        text-align: center;
        font-size: 110%;
        letter-spacing: -0.07em;
        color: #93883F;
        font-weight: bold;
        text-decoration: none;
        width: 36px;
        height: 30px;
        padding: 7px 0 0 0;
        background: url("lib/iui/shade-compact.gif") no-repeat;
      }
    </style>
  </head>
  <body>
    <div class="toolbar">
      <h1 id="pageTitle"><%=name%></h1>
    </div>
    <ul id="tables" selected="true">
      <%
        n = 0
        order = order_string()
        where = where_string()
      %>
      <script type="text/javascript">
        rodeo_records = <%=count(where)%>;
      </script>
      <%fetch(@rodeo.offset,@rodeo.elems.to_i,where,order) {|roid|%>
        <%url = ""%>
        <%url << "#{@rodeo.appname}?"%>
        <%url << "user=#{CGI::escape(@rodeo.user||"")}&"%>
        <%url << "session=#{CGI::escape(@rodeo.session||"")}&"%>
        <%url << "table=#{CGI::escape(@name||"")}&"%>
        <%url << "roid=#{roid}&"%>
        <%url << "action=record_spec_window"%>
        <%target = CGI::escape("#{@rodeo.db}/#{@name}/詳細")%>
        <li>
          <a class="roid" href="<%=url%>" target="<%=target%>"><%=roid%></a>
          <%@farray.each_index {|i|%>
            <%if(i > 0)%>
              <%if(@farray[i]['type'] == "RFstring")%>
                <a href="<%=url%>" target="<%=target%>"><%@farray[i]['fobj'].display(true)%></a>
                <%break%>
              <%end%>
            <%end%>
          <%}%>
        </li>
      <%}%>
    </ul>
  </body>
</html>

#### iphone_rodeo_record_spec

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><%=@rodeo.db%>/<%=name%>/詳細</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <link rel="apple-touch-icon" href="lib/iui/iui-logo-touch-icon.png" />
    <meta name="apple-touch-fullscreen" content="YES" />
    <style type="text/css" media="screen">@import "lib/iui/iui.css";</style>
    <script type="application/x-javascript" src="lib/iui/iui.js"></script>
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <body>
    <div class="toolbar">
      <h1 id="pageTitle">詳細</h1>
      <a class="button leftButton" type="cancel" href="javascript:window.close();">閉じる</a>
      <%url = ""%>
      <%url << "#{@rodeo.appname}?"%>
      <%url << "user=#{CGI::escape(@rodeo.user||"")}&"%>
      <%url << "session=#{CGI::escape(@rodeo.session||"")}&"%>
      <%url << "table=#{CGI::escape(@name||"")}&"%>
      <%url << "roid=#{@roid}&"%>
      <%url << "action=record_edit_window"%>
      <%target = CGI::escape("#{@rodeo.db}/#{@name}/詳細")%>
      <a class="button blueButton" type="submit" href="<%=url%>" target="<%=target%>">編集</a>
    </div>
    <%@rodeo.html_form(@rodeo.appname,{"class"=>"panel","selected"=>"true"}) {%>
      <fieldset>
        <%self.farray.each {|fld|%>
          <%if(fld['name'] != "roid")%>
            <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true)%>
              <div class="row">
                <label><%=fld['name']%></label>
                <span><%fld['fobj'].display(false)%></span>
              </div>
            <%end%>
          <%end%>
        <%}%>
      </fieldset>
    <%}%>
  </body>
</html>

#### iphone_rodeo_record_edit

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title><%=@rodeo.db%>/<%=name%>/編集</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <link rel="apple-touch-icon" href="lib/iui/iui-logo-touch-icon.png" />
    <meta name="apple-touch-fullscreen" content="YES" />
    <style type="text/css" media="screen">@import "lib/iui/iui.css";</style>
    <script type="application/x-javascript" src="lib/iui/iui.js"></script>
    <%@rodeo.html_rodeo_javascript()%>
  </head>
  <body>
    <div class="toolbar">
      <h1 id="pageTitle">詳細</h1>
      <a class="button leftButton" type="cancel" href="javascript:window.close();">キャンセル</a>
      <%url = ""%>
      <%url << "#{@rodeo.appname}?"%>
      <%url << "user=#{CGI::escape(@rodeo.user||"")}&"%>
      <%url << "session=#{CGI::escape(@rodeo.session||"")}&"%>
      <%url << "table=#{CGI::escape(@name||"")}&"%>
      <%url << "roid=#{@roid}&"%>
      <%url << "action=record_edit_window"%>
      <%target = CGI::escape("#{@rodeo.db}/#{@name}/編集")%>
      <a class="button blueButton" type="submit" href="<%=url%>" target="<%=target%>">更新</a>
    </div>
    <%@rodeo.html_form(@rodeo.appname,{"class"=>"panel","selected"=>"true"}) {%>
      <fieldset>
        <%self.farray.each {|fld|%>
          <%if(fld['name'] != "roid")%>
            <%if(fld['fobj'].plugin != true and fld['fobj'].exec != true)%>
              <div class="row">
                <label><%=fld['name']%></label>
                <%fld['fobj'].entry()%>
              </div>
            <%end%>
          <%end%>
        <%}%>
      </fieldset>
    <%}%>
  </body>
</html>

