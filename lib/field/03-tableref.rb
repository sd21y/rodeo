# -*- coding: utf-8 -*-

#
# テーブル参照型
#

class RFtableref < RFInt
  def self.name(); "テーブル参照" end
  def self.type(); RFInt.type() end
  # 初期化
  def initialize(name,table,conf)
    super(name,table,conf)
    @conf['regexp'] = nil    unless(@conf['regexp'])
  end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['regexp'] = @table.rodeo["#{@fid}.regexp"]
  end
  # 絞りこみ
  def where_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"where_entry",binding)
      yield() if(block_given?)
    }
  end
  # 絞りこみの評価
  def eval_where()
    where = nil
    case @table.rodeo['operator']
    when "="  then where = %Q("#{@name}" =  #{@table.rodeo['data']})
    when "!=" then where = %Q("#{@name}" != #{@table.rodeo['data']})
    when "<"  then where = %Q("#{@name}" <  #{@table.rodeo['data']})
    when "<=" then where = %Q("#{@name}" <= #{@table.rodeo['data']})
    when ">"  then where = %Q("#{@name}" >  #{@table.rodeo['data']})
    when ">=" then where = %Q("#{@name}" >= #{@table.rodeo['data']})
    end
    where
  end
  #
  def eval_where_marked(roid)
    where = nil
    @table.select(roid)
    if(@value)
      where = %Q("#{@name}" =  #{to_db()})
    end
    where
  end
  # CGIからの取り込み
  def from_cgi()
    super() {
      @value = @table.rodeo[cgi_name()].to_i
		}
  end
  # 入力
  def entry(opt={})
    opt = {} unless(opt)
    opt['accesskey'] = @conf['accesskey'] if(@conf['accesskey'] != "")
    super() {
      first = nil
      @table.rodeo.sql.exec(%Q(select "tablename" from "pg_tables" where tablename~'^rc_*' order by tablename)) {|res|
        @table.rodeo.html_select("#{cgi_name()}",@value) {
          @table.rodeo.html_item(nil,"<<< なし >>>")
          if(@conf['regexp'] and @conf['regexp'].strip != "")
            res.each {|ent|
              name = ent['tablename'].sub(/^rc_/,"")
              if(name =~ /#{@conf['regexp']}/)
                @table.rodeo.html_item(@table.find_tid(name),name)
              end
            }
          else
            res.each {|ent|
              name = ent['tablename'].sub(/^rc_/,"")
              @table.rodeo.html_item(@table.find_tid(name),name)
            }
          end
        }
      }
    }
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(@value)
        Rodeo.print @table.refer_tid(@value)
      end
    }
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("検索用正規表現")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.regexp",@conf['regexp'],{"style"=>"width:100%"})%></td>
</tr>

#### where_entry

<tr>
  <td class="field_conf">
    <%=@name%>が
    <%@table.rodeo.html_number("data","",{"size"=>@conf['cols']})%>
    <br>
    <%
      @table.rodeo.html_radio("operator","=") {
        @table.rodeo.html_item("=" ,"等しい")
        @table.rodeo.html_item("!=","等しくない")
        @table.rodeo.html_item("<" ,"小さい")
        @table.rodeo.html_item("<=","以下")
        @table.rodeo.html_item(">" ,"大きい")
        @table.rodeo.html_item(">=","以上")
      }
    %>
  </td>
</tr>

