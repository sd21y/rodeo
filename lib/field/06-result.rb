# -*- coding: utf-8 -*-

#
# 設問結果型
#

require "#{File.dirname(__FILE__)}/../email.rb"

class RFresult < RFPlugin
  def self.name(); "設問結果" end
  # 実行用アイコン
  def icon()
    "applications-system.png"
  end
  # 設定
  def config_entry(su=true)
    super(su,[]) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['enq_title'] = @table.rodeo["#{@fid}.enq_title"]
    @conf['enq_time'] = @table.rodeo["#{@fid}.enq_time"].to_i
  end
  # アプリケーション画面の表示
  def action_app_exec()
    super() {
      @table.clear()
#      @table.find("回答者",%Q({#{@table.rodeo.user_record['roid'].value}}))
      @table.rodeo.eruby(__FILE__,"result_start",binding)
      @table.rodeo.output()
    }
  end
  # アプリケーション画面の表示
  def action_result_done()
=begin
    roid = @table.find("回答者",%Q({#{@table.rodeo.user_record['roid'].value}}))
    if(roid)
      @table.eval_cgi()
      @table['回答者'].value = [@table.rodeo.user_record['roid'].value]
      point = 0
      total = 0
      result = @table.rodeo.table_new(@table.name+"問題")
      result.fetch(0,result.count(),nil,%Q("設問ID")) {|roid2|
        master = @table.rodeo.table_new(@table[result['設問ID'].value].conf['master_table'])
        right = result['正解'].value.split(/[, ]/).sort.join(",")
        array = []
        @table[result["設問ID"].value].value.each {|v|
          master.select(v)
          array << master['選択肢'].value
        }
        ans = array.sort.join(",")
        total += result["配点"].value
        point += result["配点"].value if(ans == right)
      }
      @table['得点'].value = point
      @table.update(roid)
    else
=end
      @table.insert_mode = true
      @table.eval_cgi()
      @table['回答者'].value = [@table.rodeo.user_record['roid'].value]
      point = 0
      total = 0
      result = @table.rodeo.table_new(@table.name+"問題")
      result.fetch(0,result.count(),nil,%Q("設問ID")) {|roid2|
        master = @table.rodeo.table_new(@table[result['設問ID'].value].conf['master_table'])
        right = result['正解'].value.split(/[, ]/).sort.join(",")
        array = []
        @table[result["設問ID"].value].value.each {|v|
          master.select(v)
          array << master['選択肢'].value
        }
        ans = array.sort.join(",")
        total += result["配点"].value
        point += result["配点"].value if(ans == right)
      }
      @table['得点'].value = point
      @table.insert()
=begin
    end
=end
    color = "green"
    color = "yellow" if(point < (total / 3 * 2))
    color = "red"    if(point < (total / 3 * 1))
    @table.rodeo.eruby(__FILE__,"result_done",binding)
    @table.rodeo.output()
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("試験タイトル","試験のタイトルを設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.enq_title",@conf['enq_title'],{"style"=>"width:100%"})%></td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("制限時間","試験の制限時間を設定します。")%></th>
  <td class="field_conf"><%@table.rodeo.html_text("#{@fid}.enq_time",@conf['enq_time'],{"size"=>5})%>分</td>
</tr>

#### result_start

<html>
  <head>
    <title><%=@conf['enq_title']%></title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
  </head>
  <body onload="timer();">
    <center><h1><%=@conf['enq_title']%>結果</h1></center>
    <hr>
    <h3>試験の結果は以下の通りです。<br>選択肢の太字の項目が正解です。</h3>
    <hr>
    <%total = 0%>
    <%point = 0%>
    <%@table.rodeo.html_hidden("action","result_done")%>
    <%@table.rodeo.html_hidden("uid",@table.rodeo.user_record['アカウント'].id.value)%>
    <%where = %Q(#{@table.rodeo.user_record['roid'].value} = ANY("回答者"))%>
    <%@table.fetch(0,1,where,"roid desc") {|uroid|%>
      <%exam = @table.rodeo.table_new(@table.name+"問題")%>
      <%exam.fetch(0,exam.count(),nil,%Q("roid")) {|roid|%>
        <%master = @table.rodeo.table_new(@table[exam['設問ID'].value].conf['master_table'])%>
        <%right = exam['正解'].value.split(/[, ]/).sort.join(",")%>
        <%array = []%>
        <%@table[exam["設問ID"].value].value.each {|v|%>
          <%master.select(v)%>
          <%array << master['選択肢'].value%>
        <%}%>
        <%ans = array.sort.join(",")%>
        <%total += exam["配点"].value%>
        <%point += exam["配点"].value if(ans == right)%>
        <div style="border: solid 1px gray; padding: 10px; margin-top: 10px; margin-bottom: 10px;">
          [<%=exam['設問ID'].value%>]&nbsp;
          <%exam['設問内容'].value.each_line {|line|%>
            <%=line%><br>
          <%}%>
          <%if(exam['設問画像'].value)%>
            <br>
            <%exam['設問画像'].display()%>
            <br>
          <%end%>
          <br>
          <%fld = @table[exam['設問ID'].value]%>
          <%master = @table.rodeo.table_new(fld.conf['master_table'])%>
          <%index = "a"%>
          <%master.fetch(0,master.count(fld.conf['where']),fld.conf['where'],%Q("#{master[fld.conf['master_sort']].order()}" asc)) {|roid2|%>
            <%if(exam["選択肢#{index}"].value != nil and exam["選択肢#{index}"].value != "")%>
              <%label = " #{master[fld.conf['master_field']].value}. #{exam["選択肢#{index}"].value}"%>
              <%if(right =~ /#{index}/)%>
                <strong><%=label%></strong><br>
              <%else%>
                <span style="color: gray;"><%=label%></span><br>
              <%end%>
            <%end%>
            <%index.succ!%>
          <%}%>
          <hr>
          <strong>
          <%if(ans != right)%>
            <span style="color: red;">あなたの解答：<%=ans%></span><br>
          <%else%>
            あなたの解答：<%=ans%><br>
          <%end%>
          <!--- 正解：<%=right%><br> -->
          </strong>
        </div>
      <%}%>
    <%}%>
    <hr>
    <%
      color = "green"
      color = "yellow" if(point < (total / 3 * 2))
      color = "red"    if(point < (total / 3 * 1))
    %>
    <h1>あなたの得点は<%=total%>点満点中<span style="color: <%=color%>;"><%=point%>点</span>です。</h1>
    <hr>
  </body>
</html>

#### result_done

<html>
  <head>
    <title><%=@conf['enq_title']%></title>
  </head>
  <body>
    <center><h1><%=@conf['enq_title']%>結果</h1></center>
    <hr>
    <center>
    <h1>試験お疲れ様でした。</h1>
    <h1>あなたの得点は<%=total%>点満点中<span style="color: <%=color%>;"><%=point%>点</span>です。</h1>
    </center>
    <hr>
  </body>
</html>

