# -*- coding: utf-8 -*-

#
# YAMAHA-RTシリーズ設定取得型
#

require 'net/telnet'
require 'timeout'
require 'pty'
require 'open3'

class PTYssh
  #
  def initialize(addr,port)
    timeout(30) {|i|
      PTY.spawn("ssh -p #{port} #{addr}") {|r,w,pid|
        @input_stream = r
        @output_stream = w
        @child_pid = pid
        #PTY.protect_signal {
          yield(self)
        #}
      }
    }
  end
  #
  def waitfor(pat,str = nil)
    res = ""
    done = false
    while(not done)
      timeout(30) {|i|
        c = @input_stream.getc().chr
        yield(c) if(block_given?)
        res << c
        if(res =~ pat)
          cmd(str) if(str)
          done = true
        end
      }
    end
  end
  #
  def cmd(c)
    @output_stream.puts(c)
  end
end

def getconfig_telnet(adr,lpw,apw)
  config = ""
  begin
    su = Iconv.new("UTF8","CP932")
    telnet = Net::Telnet.new("Host"=>adr)
    telnet.waitfor(/Password[: ]*\z/n)
    telnet.cmd("String"=>lpw,"Match"=>/\>[ ]*\z/n)
    telnet.cmd("String"=>"console character ascii","Match"=>/\>[ ]*\z/n)
    telnet.cmd("String"=>"console lines infinity","Match"=>/\>[ ]*\z/n)
    telnet.cmd("String"=>"console columns 200","Match"=>/\>[ ]*\z/n)
    telnet.cmd("String"=>"administrator","Match"=>/Password[: ]*\z/n)
    telnet.cmd("String"=>apw,"Match"=>/\#[ ]*\z/n)
    telnet.cmd("String"=>"show config","Match"=>/\#[ ]*\z/n) {|c| config << c }
    telnet.close
    config.sub!(/^show config.*?#/m,"")
    config.sub!(/^\#\s$/m,"")
    config = "#"+config
    config = su.iconv(config)
  rescue Exception
    config = nil
  end
  config
end

def getconfig_ssh(adr,lpw,apw)
  config = ""
  begin
    PTYssh.new(adr,5024) {|ssh|
      ssh.waitfor(/password:/,"a-shigi") #{|c| print c }
      ssh.waitfor(/>\s/,"console lines infinity") #{|c| print c }
      ssh.waitfor(/>\s/,"administrator") #{|c| print c }
      ssh.waitfor(/Password:/,"0559772799") #{|c| print c }
      ssh.waitfor(/#\s/,"show config") #{|c| print c }
      ssh.waitfor(/#\s/,"exit") #{|c| print c }
      ssh.waitfor(/>\s/,"exit") {|c| config << c }
      ssh.waitfor(/closed\./) #{|c| print c }
    }
  rescue Exception
    config = nil
  end
  config
end

class RFgetrtconfig < RFPlugin
  def self.name(); "RT設定取得" end
  # 設定
  def config_entry(su=true)
    super(su) {
      @table.rodeo.eruby(__FILE__,"config_entry",binding)
      yield() if(block_given?)
    }
  end
  # 設定の評価
  def eval_config()
    super()
    @conf['login_field']  = @table.rodeo["#{@fid}.login_field"]
    @conf['admin_field']  = @table.rodeo["#{@fid}.admin_field"]
    @conf['addr_field']  = @table.rodeo["#{@fid}.addr_field"]
    @conf['conf_field']  = @table.rodeo["#{@fid}.conf_field"]
  end
  # CGIからの取り込み
  def from_cgi()
  end
  # 入力
  def entry()
  end
  # 表示
  def display(list=false)
    super(list) {|list|
      if(list)
        if(@table[@conf['addr_field']].value)
          Rodeo.print %Q(<a href="javascript:app_exec('#{@name}',#{@table.roid})">設定取得</a>)
        end
      end
    }
  end
end

#
# イベントハンドラ
#

class RFgetrtconfig < RFPlugin
  def action_app_exec()
    super() {
      @table.rodeo.eruby(__FILE__,"start",binding)
    }
  end
  def action_do_getconfig()
    adr = @table[@conf['addr_field']].value.to_str
    lpw = @table[@conf['login_field']].passwd.value
    apw = @table[@conf['admin_field']].passwd.value
    config = nil
    Open3.popen3("lib/getrtconfig.rb #{adr} #{lpw} #{apw}") {|stdin,stdout,stderr|
      config = stdout.read()
    }
    if(config)
      @table.select(@table.roid)
      @table[@conf['conf_field']].value = config
      @table.update(@table.roid)
      @table.rodeo.eruby(__FILE__,"done",binding)
      @table.rodeo.output()
    else
      @table.rodeo.eruby(__FILE__,"error",binding)
      @table.rodeo.output()
    end
  end
end

__END__

#### config_entry

<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("ログインアカウント","ルータにログインするときのアカウントフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.login_field",@conf['login_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFaccount")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("管理者アカウント","管理者のアカウントフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.admin_field",@conf['admin_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFaccount")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("アドレスフィールド","ルータにアクセスするIPアドレスのフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.addr_field",@conf['addr_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFinet" or h['type'] == "RFinet6")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>
<tr>
  <th class="field_conf"><%@table.rodeo.html_tooltip("テキストフィールド","ルータの設定情報を格納するフィールドを選択します。")%></th>
  <td class="field_conf">
    <%
      @table.rodeo.html_select("#{@fid}.conf_field",@conf['conf_field']) {
        @table.farray.each {|h|
          if(h['type'] == "RFmemo")
            @table.rodeo.html_item(h['name'],h['name'])
          end
        }
      }
    %>
  </td>
</tr>

#### start

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
  </head>
  <body>
    [[[[ ルータ設定情報の取得 ]]]]<br>
    <hr>
    ルータ「<%=@table[@conf['addr_field']].value.to_str%>」から設定情報を取得し、「<%=@conf['conf_field']%>」を更新します。<br>
    よろしいですか？<br>
    <hr>
    <%@table.rodeo.html_form(@table.rodeo.appname) {%>
      <%@table.rodeo.html_hidden("action","do_getconfig")%>
      <%@table.rodeo.html_submit("submit","実行")%>
    <%}%>
  </body>
</html>

#### done

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
  </head>
  <body>
    [[[[ ルータ設定情報 ]]]]<br>
    <hr><tt>
    <%config.each_line {|line|%>
      <%line.chomp! if(line)%>
      <%line.gsub!(/\r/,"") if(line)%>
      <%line = CGI::escapeHTML(line) if(line)%>
      <%line.gsub!(/\s/,"&nbsp;") if(line)%>
      <%=line%><br>
    <%}%>
    </tt>
    <hr>
  </body>
</html>

#### error

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@table.rodeo.html_rodeo_javascript()%>
  </head>
  <body>
    [[[[ ルータ設定情報の取得エラー ]]]]<br>
    <hr>
    <font color="red">
    ルータの設定情報を取得できませんでした。<br>
    ルータのアドレスもしくはパスワードを確認してください。<br>
    </font>
    <hr>
  </body>
</html>

